# Jahyca

## Project concept

This game is just a fun little side project I sometimes work on to try out some things or just to have a break from my main projects.

It started as a short experiment with the following goals / limitations:
- create a playable game as fast as possible (within 24 hours, then a better version within 3 days), like a little personal game jam :)
- the game needs to be playable comfortably on both desktop and mobile (only clicking / tapping required, UI suitably visible in both cases)
- game art needs to be sourced from a royalty free site (OpenGameArt.org)
- try making an RPG where the player has no levels and cannot get superhuman amount of e.g. hitpoints compared to when starting out (inspired by [Shadiversity](https://www.youtube.com/watch?v=x6UV2gXwFPw))
- use HTML, ES6 style JS and CSS from scratch, UI needs to be plain HTML, animations with CSS (I work a lot normally with canvas / WebGL, so this was new for me)

My main concept was that since in real life, people generally accomplish big things not by becoming lone superhuman heroes, but by relying on others (e.g. you use the infrastructure and accumulated knowledge of other people as well as rely on friends for support and experts for professional needs). To mirror this, the main mechanic in the game is that the hero him/herself doesn't get any stat increases or experience from defeating monsters, but instead needs to invest the loot he/she finds in the NPCs of the town so they can provide the hero with higher level services (training, weapons, potions etc)

## Story & lore

The ages long war has ended between the two most powerful human realms, the Jahyr Kingdom founded by the tough warriors of the harsh deserts, and the ancient Cascadia of the fertile riverlands. The peace has been sealed with a historical marriage, creating the greatest human civilization to date, the empire of Jahyca. In this new age of prosperity and progress, humans slowly start expanding again, repopulating the war ravaged lands, taking them back from the hands of beasts, trolls and worse, as the monstrous creatures have spread all over the realm while the human armies were busy fighting each other. Some even have seen dragons from the Obno mountains right in the riverlands. As an adventurer, help your town rebuild and secure the surrounding areas from monsters and mercenary knights who, now jobless, turned to a life of raiding and crime.

In the world of Jahyca, humans, unlike some other creatures, such as elves, do not possess innate magical abilities. Through the power of alchemy though, it became possible to create substances that can be ingested by a human, and the resulting magical turbulence can be focused with enough training for magical effect. Items can also be enchanted with magical properties and can act as a reservoir of magic for a human.

## Credits & license

I have designed and programmed the game on my own, but most of the art (expect for a few pieces I couldn't find and had to make myself) is taken or adapted from royalty free assets from OpenGameArt.org.
See [img/sources.txt](img/sources.txt) for a complete list of where each of the images is from and what license corresponds to them.

The fonts used in the game are [Oregano](https://fonts.google.com/specimen/Oregano) and [Macondo Swash Caps](https://fonts.google.com/specimen/Macondo+Swash+Caps), available under the [Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).

The other source files (HTML, JS, CSS) are released by me under the [GNU GPL v3 license](http://www.gnu.org/licenses/gpl-3.0-standalone.html).

Copyright 2018-2020 Krisztián Nagy
