/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

type EnemyTypeProbabilities = {
    [P in EnemyTypeName]: number;
}

interface NaturalHealing {
    name: string;
    value: number;
    chance: number;
}

interface RoomType {
    roomName: string;
    roomImage: string;
    pathImage: string;
    light: boolean;
    enemyChance: number;
    enemyTypeProbabilities: EnemyTypeProbabilities;
    loot: LootChances;
    healing: NaturalHealing;
}

type AreaTypeProbabilities = {
    [P in keyof typeof AreaTypes]: number;
}

interface AreaType extends NamedObject {
    image: string;
    roomTypes: readonly RoomType[];
    roomProbabilities: readonly (readonly number[])[];
    reward: LootChances;
    pathDepth: number;
    pathProbabilities: readonly number[];
    nextAreaTypeProbabilities: AreaTypeProbabilities;
}

type AreaTypeMap = NamedObjectMap<AreaType>;

const AreaTypes: AreaTypeMap = {
    FOREST: {
        name: "Forest",
        image: "forest.png",
        roomTypes: [{
            roomName: "path",
            roomImage: "forest.png",
            pathImage: "forest-path.png",
            light: false,
            enemyChance: 0.8,
            enemyTypeProbabilities: {
                SHAMAN: 0.6,
                FOREST_MONSTER: 0.3,
                EARTH_GOLEM: 0.1
            },
            loot: {
                gold: {
                    chance: 0.5,
                    amount: [5, 100]
                },
                weapons: [{
                    type: WeaponTypes.RUSTY_SWORD,
                    chance: 0.05
                }, {
                    type: WeaponTypes.HAND_AXE,
                    chance: 0.1
                }],
                potions: [{
                    type: PotionTypes.SMALL_LIFE,
                    chances: [0.95, 0.05]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.95, 0.05]
                }]
            },
            healing: {
                name: "medicinal herbs",
                value: 10,
                chance: 0.3
            }
        }, {
            roomName: "path",
            roomImage: "forest2.png",
            pathImage: "forest2-path.png",
            light: false,
            enemyChance: 0.8,
            enemyTypeProbabilities: {
                SHAMAN: 0.2,
                FOREST_MONSTER: 0.1,
                EARTH_GOLEM: 0.2,
                ELVEN_RANGER: 0.5
            },
            loot: {
                gold: {
                    chance: 0.65,
                    amount: [5, 150]
                },
                weapons: [{
                    type: WeaponTypes.RUSTY_SWORD,
                    chance: 0.05
                }, {
                    type: WeaponTypes.HAND_AXE,
                    chance: 0.1
                }],
                potions: [{
                    type: PotionTypes.SMALL_LIFE,
                    chances: [0.95, 0.05]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.1]
                }]
            },
            healing: {
                name: "healing fountain",
                value: 15,
                chance: 0.2
            }
        }, {
            roomName: "path",
            roomImage: "forest3.png",
            pathImage: "forest3-path.png",
            light: false,
            enemyChance: 0.8,
            enemyTypeProbabilities: {
                SHAMAN: 0.2,
                FOREST_MONSTER: 0.0,
                EARTH_GOLEM: 0.0,
                ELVEN_RANGER: 0.5,
                ELVEN_MAGE: 0.3
            },
            loot: {
                gold: {
                    chance: 0.75,
                    amount: [5, 175]
                },
                weapons: [{
                    type: WeaponTypes.HAND_AXE,
                    chance: 0.1
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.05
                }],
                potions: [{
                    type: PotionTypes.SMALL_LIFE,
                    chances: [0.95, 0.05]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.85, 0.1, 0.05]
                }]
            },
            healing: {
                name: "healing fountain",
                value: 15,
                chance: 0.2
            }
        }],
        roomProbabilities: [[1, 0, 0], [0.75, 0.25, 0], [0.5, 0.3, 0.2], [0.3, 0.5, 0.2]],
        reward: {
            gold: {
                chance: 1.0,
                amount: [50, 100]
            },
            npcImprovementItem: [{
                npc: "Healer",
                chance: 0.5
            }, {
                npc: "Swordsmith",
                chance: 0.5
            }]
        },
        pathDepth: 3,
        pathProbabilities: [0.6, 0.3, 0.1],
        nextAreaTypeProbabilities: {
            FOREST: 0.0,
            MOUNTAINS: 0.5,
            RIVERLAND: 0.5,
            FIELDS: 0.0,
            DESERT: 0.0,
            SWAMP: 0.0,
            CAVES: 0.0,
            SEASIDE: 0.0,
            CASTLE: 0.0
        }
    },
    MOUNTAINS: {
        name: "Mountains",
        image: "mountains.png",
        roomTypes: [{
            roomName: "ridge",
            roomImage: "mountains.png",
            pathImage: "mountains-path.png",
            light: true,
            enemyChance: 0.7,
            enemyTypeProbabilities: {
                GOLEM: 0.5,
                HELLBEAST: 0.3,
                MOUNTAIN_TROLL: 0.2
            },
            loot: {
                gold: {
                    chance: 0.4,
                    amount: [5, 250]
                },
                weapons: [{
                    type: WeaponTypes.APPRENTICE_STAFF,
                    chance: 0.025
                }, {
                    type: WeaponTypes.ARMING_SWORD,
                    chance: 0.025
                }],
                potions: [{
                    type: PotionTypes.SMALL_LIFE,
                    chances: [0.9, 0.05, 0.05]
                }, {
                    type: PotionTypes.SMALL_MAGIC,
                    chances: [0.9, 0.1]
                }]
            },
            healing: {
                name: "a mountain source",
                value: 10,
                chance: 0.15
            },
        }, {
            roomName: "ridge",
            roomImage: "mountains2.png",
            pathImage: "mountains2-path.png",
            light: true,
            enemyChance: 0.75,
            enemyTypeProbabilities: {
                GOLEM: 0.3,
                HELLBEAST: 0.2,
                MOUNTAIN_TROLL: 0.4,
                DRAGON: 0.1
            },
            loot: {
                gold: {
                    chance: 0.6,
                    amount: [5, 300]
                },
                weapons: [{
                    type: WeaponTypes.APPRENTICE_STAFF,
                    chance: 0.025
                }, {
                    type: WeaponTypes.ARMING_SWORD,
                    chance: 0.025
                }],
                potions: [{
                    type: PotionTypes.SMALL_LIFE,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }, {
                    type: PotionTypes.SMALL_MAGIC,
                    chances: [0.9, 0.05, 0.05]
                }]
            },
            healing: {
                name: "a mountain source",
                value: 10,
                chance: 0.15
            },
        }],
        roomProbabilities: [[1, 0], [0.75, 0.25], [0.5, 0.5]],
        reward: {
            gold: {
                chance: 1.0,
                amount: [100, 250]
            },
            npcImprovementItem: [{
                npc: "Healer",
                chance: 0.5
            }, {
                npc: "Mage",
                chance: 0.5
            }]
        },
        pathDepth: 4,
        pathProbabilities: [0.8, 0.15, 0.05],
        nextAreaTypeProbabilities: {
            FOREST: 0.25,
            MOUNTAINS: 0.0,
            RIVERLAND: 0.25,
            FIELDS: 0.25,
            DESERT: 0.25,
            SWAMP: 0.0,
            CAVES: 0.0,
            SEASIDE: 0.0,
            CASTLE: 0.0
        }
    },
    RIVERLAND: {
        name: "Riverland",
        image: "riverland.png",
        roomTypes: [{
            roomName: "shoreline",
            roomImage: "riverland.png",
            pathImage: "riverland-path.png",
            light: true,
            enemyChance: 0.75,
            enemyTypeProbabilities: {
                INSECT_WARRIOR: 0.6,
                KNIGHT: 0.3,
                RIVER_TROLL: 0.1
            },
            loot: {
                gold: {
                    chance: 0.6,
                    amount: [5, 180]
                },
                weapons: [{
                    type: WeaponTypes.HAND_AXE,
                    chance: 0.05
                }],
                potions: [{
                    type: PotionTypes.SMALL_LIFE,
                    chances: [0.9, 0.05, 0.05]
                }]
            },
            healing: {
                name: "a freshwater stream",
                value: 10,
                chance: 0.20
            },
        }, {
            roomName: "shoreline",
            roomImage: "riverland2.png",
            pathImage: "riverland2-path.png",
            light: true,
            enemyChance: 0.8,
            enemyTypeProbabilities: {
                INSECT_WARRIOR: 0.3,
                KNIGHT: 0.3,
                RIVER_TROLL: 0.3,
                DRAGON: 0.1
            },
            loot: {
                gold: {
                    chance: 0.7,
                    amount: [5, 260]
                },
                weapons: [{
                    type: WeaponTypes.HAND_AXE,
                    chance: 0.05
                }],
                potions: [{
                    type: PotionTypes.SMALL_LIFE,
                    chances: [0.925, 0.025, 0.025, 0.025]
                }, {
                    type: PotionTypes.SMALL_MAGIC,
                    chances: [0.95, 0.05]
                }]
            },
            healing: {
                name: "a freshwater stream",
                value: 10,
                chance: 0.20
            },
        }],
        roomProbabilities: [[1, 0], [0.75, 0.25], [0.5, 0.5]],
        reward: {
            gold: {
                chance: 1.0,
                amount: [100, 200]
            },
            npcImprovementItem: [{
                npc: "Swordsmith",
                chance: 0.5
            }, {
                npc: "Mage",
                chance: 0.5
            }]
        },
        pathDepth: 3,
        pathProbabilities: [0.6, 0.25, 0.1, 0.05],
        nextAreaTypeProbabilities: {
            FOREST: 0.2,
            MOUNTAINS: 0.2,
            RIVERLAND: 0.0,
            FIELDS: 0.3,
            DESERT: 0.2,
            SWAMP: 0.1,
            CAVES: 0.0,
            SEASIDE: 0.0,
            CASTLE: 0.0
        }
    },
    FIELDS: {
        name: "Fields",
        image: "fields.png",
        roomTypes: [{
            roomName: "field",
            roomImage: "fields.png",
            pathImage: "fields-path.png",
            light: false,
            enemyChance: 0.5,
            enemyTypeProbabilities: {
                ORC_WARRIOR: 0.5,
                MINOTAUR: 0.3,
                FLAMELING: 0.2
            },
            loot: {
                gold: {
                    chance: 0.4,
                    amount: [5, 220]
                },
                weapons: [{
                    type: WeaponTypes.BATTLE_AXE,
                    chance: 0.035
                }, {
                    type: WeaponTypes.ARMING_SWORD,
                    chance: 0.035
                }, {
                    type: WeaponTypes.APPRENTICE_STAFF,
                    chance: 0.035
                }],
                potions: [{
                    type: PotionTypes.SMALL_LIFE,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }, {
                    type: PotionTypes.SMALL_MAGIC,
                    chances: [0.925, 0.05, 0.025]
                }]
            },
            healing: {
                name: "ripe crops",
                value: 10,
                chance: 0.10
            },
        }, {
            roomName: "field",
            roomImage: "fields2.png",
            pathImage: "fields2-path.png",
            light: true,
            enemyChance: 0.65,
            enemyTypeProbabilities: {
                ORC_WARRIOR: 0.2,
                MINOTAUR: 0.4,
                FLAMELING: 0.3,
                GHOUL: 0.1
            },
            loot: {
                gold: {
                    chance: 0.5,
                    amount: [5, 280]
                },
                weapons: [{
                    type: WeaponTypes.BATTLE_AXE,
                    chance: 0.035
                }, {
                    type: WeaponTypes.ARMING_SWORD,
                    chance: 0.035
                }, {
                    type: WeaponTypes.APPRENTICE_STAFF,
                    chance: 0.035
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.9, 0.075, 0.025]
                }, {
                    type: PotionTypes.SMALL_MAGIC,
                    chances: [0.925, 0.05, 0.025]
                }]
            },
            healing: {
                name: "ripe crops",
                value: 10,
                chance: 0.10
            },
        }],
        roomProbabilities: [[1, 0], [0.75, 0.25], [0.5, 0.5]],
        reward: {
            gold: {
                chance: 1.0,
                amount: [100, 250]
            },
            npcImprovementItem: [{
                npc: "Swordsmith",
                chance: 0.5
            }, {
                npc: "Alchemist",
                chance: 0.5
            }]
        },
        pathDepth: 2,
        pathProbabilities: [0.3, 0.4, 0.2, 0.1],
        nextAreaTypeProbabilities: {
            FOREST: 0.1,
            MOUNTAINS: 0.1,
            RIVERLAND: 0.1,
            FIELDS: 0.0,
            DESERT: 0.2,
            SWAMP: 0.2,
            CAVES: 0.1,
            SEASIDE: 0.1,
            CASTLE: 0.1
        }
    },
    DESERT: {
        name: "Desert",
        image: "desert.png",
        roomTypes: [{
            roomName: "dune",
            roomImage: "desert.png",
            pathImage: "desert-path.png",
            light: true,
            enemyChance: 0.4,
            enemyTypeProbabilities: {
                FOSSIL: 0.5,
                HELLBEAST: 0.3,
                DRAGONSNAKE: 0.2
            },
            loot: {
                gold: {
                    chance: 0.4,
                    amount: [5, 300]
                },
                weapons: [{
                    type: WeaponTypes.BATTLE_AXE,
                    chance: 0.025
                }, {
                    type: WeaponTypes.ARMING_SWORD,
                    chance: 0.025
                }, {
                    type: WeaponTypes.APPRENTICE_STAFF,
                    chance: 0.05
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.9, 0.075, 0.025]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }]
            },
            healing: {
                name: "eatable bugs",
                value: 5,
                chance: 0.05
            },
        }, {
            roomName: "dune",
            roomImage: "desert2.png",
            pathImage: "desert2-path.png",
            light: true,
            enemyChance: 0.6,
            enemyTypeProbabilities: {
                FOSSIL: 0.2,
                HELLBEAST: 0.3,
                DRAGONSNAKE: 0.2,
                DESERT_TROLL: 0.2
            },
            loot: {
                gold: {
                    chance: 0.5,
                    amount: [5, 350]
                },
                weapons: [{
                    type: WeaponTypes.BATTLE_AXE,
                    chance: 0.025
                }, {
                    type: WeaponTypes.ARMING_SWORD,
                    chance: 0.025
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.035
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }]
            },
            healing: {
                name: "eatable bugs",
                value: 5,
                chance: 0.05
            },
        }],
        roomProbabilities: [[1, 0], [0.75, 0.25], [0.5, 0.5]],
        reward: {
            gold: {
                chance: 1.0,
                amount: [150, 300]
            },
            npcImprovementItem: [{
                npc: "Mage",
                chance: 0.5
            }, {
                npc: "Alchemist",
                chance: 0.5
            }]
        },
        pathDepth: 3,
        pathProbabilities: [0.4, 0.3, 0.2, 0.1],
        nextAreaTypeProbabilities: {
            FOREST: 0.1,
            MOUNTAINS: 0.1,
            RIVERLAND: 0.0,
            FIELDS: 0.2,
            DESERT: 0.0,
            SWAMP: 0.2,
            CAVES: 0.15,
            SEASIDE: 0.15,
            CASTLE: 0.1
        }
    },
    SWAMP: {
        name: "Swamp",
        image: "swamp.png",
        roomTypes: [{
            roomName: "pond",
            roomImage: "swamp.png",
            pathImage: "swamp-path.png",
            light: false,
            enemyChance: 0.8,
            enemyTypeProbabilities: {
                SWAMPLING: 0.6,
                PENTAGORE: 0.3,
                EXILED_KNIGHT: 0.1
            },
            loot: {
                gold: {
                    chance: 0.3,
                    amount: [5, 300]
                },
                weapons: [{
                    type: WeaponTypes.BATTLE_AXE,
                    chance: 0.015
                }, {
                    type: WeaponTypes.ARMING_SWORD,
                    chance: 0.015
                }, {
                    type: WeaponTypes.APPRENTICE_STAFF,
                    chance: 0.1
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }]
            },
            healing: {
                name: "swamp fruits",
                value: 10,
                chance: 0.25
            }
        }, {
            roomName: "pond",
            roomImage: "swamp2.png",
            pathImage: "swamp2-path.png",
            light: false,
            enemyChance: 0.85,
            enemyTypeProbabilities: {
                SWAMPLING: 0.2,
                PENTAGORE: 0.2,
                EXILED_KNIGHT: 0.25,
                SWAMP_BEAST: 0.35
            },
            loot: {
                gold: {
                    chance: 0.3,
                    amount: [5, 300]
                },
                weapons: [{
                    type: WeaponTypes.BATTLE_AXE,
                    chance: 0.02
                }, {
                    type: WeaponTypes.LARGE_AXE,
                    chance: 0.005
                }, {
                    type: WeaponTypes.ARMING_SWORD,
                    chance: 0.02
                }, {
                    type: WeaponTypes.FINE_SWORD,
                    chance: 0.005
                }, {
                    type: WeaponTypes.APPRENTICE_STAFF,
                    chance: 0.125
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.01
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.85, 0.05, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_LIFE,
                    chances: [0.95, 0.05]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.85, 0.05, 0.05, 0.05]
                }]
            },
            healing: {
                name: "healing mushrooms",
                value: 15,
                chance: 0.15
            }
        }, {
            roomName: "pond",
            roomImage: "swamp.png",
            pathImage: "swamp-path.png",
            light: false,
            enemyChance: 0.9,
            enemyTypeProbabilities: {
                SWAMPLING: 0.1,
                PENTAGORE: 0.15,
                EXILED_KNIGHT: 0.25,
                SWAMP_BEAST: 0.25,
                SWAMP_DRAGON: 0.25
            },
            loot: {
                gold: {
                    chance: 0.3,
                    amount: [5, 350]
                },
                weapons: [{
                    type: WeaponTypes.BATTLE_AXE,
                    chance: 0.025
                }, {
                    type: WeaponTypes.LARGE_AXE,
                    chance: 0.01
                }, {
                    type: WeaponTypes.ARMING_SWORD,
                    chance: 0.025
                }, {
                    type: WeaponTypes.FINE_SWORD,
                    chance: 0.01
                }, {
                    type: WeaponTypes.APPRENTICE_STAFF,
                    chance: 0.15
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.015
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.8, 0.1, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_LIFE,
                    chances: [0.925, 0.05, 0.025]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.8, 0.1, 0.05, 0.05]
                }]
            },
            healing: {
                name: "swamp fruits",
                value: 10,
                chance: 0.15
            }
        }],
        roomProbabilities: [[1, 0, 0], [0.75, 0.25, 0], [0.5, 0.3, 0.2], [0.3, 0.5, 0.2]],
        reward: {
            gold: {
                chance: 1.0,
                amount: [150, 350]
            },
            npcImprovementItem: [{
                npc: "Healer",
                chance: 0.5
            }, {
                npc: "Alchemist",
                chance: 0.5
            }]
        },
        pathDepth: 3,
        pathProbabilities: [0.6, 0.3, 0.1],
        nextAreaTypeProbabilities: {
            FOREST: 0.1,
            MOUNTAINS: 0.05,
            RIVERLAND: 0.05,
            FIELDS: 0.15,
            DESERT: 0.1,
            SWAMP: 0.0,
            CAVES: 0.25,
            SEASIDE: 0.2,
            CASTLE: 0.1
        }
    },
    CAVES: {
        name: "Caves",
        image: "caves.png",
        roomTypes: [{
            roomName: "room",
            roomImage: "caves.png",
            pathImage: "caves-path.png",
            light: false,
            enemyChance: 0.75,
            enemyTypeProbabilities: {
                SPIDER: 0.45,
                SKELETON: 0.35,
                CAVE_TROLL: 0.2
            },
            loot: {
                gold: {
                    chance: 0.6,
                    amount: [5, 350]
                },
                weapons: [{
                    type: WeaponTypes.LARGE_AXE,
                    chance: 0.02
                }, {
                    type: WeaponTypes.FINE_SWORD,
                    chance: 0.01
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.02
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.9, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_LIFE,
                    chances: [0.95, 0.05]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_MAGIC,
                    chances: [0.95, 0.05]
                }]
            },
            healing: {
                name: "mineral water",
                value: 15,
                chance: 0.15
            },
        }, {
            roomName: "room",
            roomImage: "caves2.png",
            pathImage: "caves2-path.png",
            light: false,
            enemyChance: 0.8,
            enemyTypeProbabilities: {
                SPIDER: 0.2,
                SKELETON: 0.3,
                CAVE_TROLL: 0.25,
                WEREWOLF: 0.25
            },
            loot: {
                gold: {
                    chance: 0.75,
                    amount: [5, 400]
                },
                weapons: [{
                    type: WeaponTypes.LARGE_AXE,
                    chance: 0.02
                }, {
                    type: WeaponTypes.FINE_SWORD,
                    chance: 0.01
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.02
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.9, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_LIFE,
                    chances: [0.925, 0.05, 0.025]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_MAGIC,
                    chances: [0.925, 0.05, 0.025]
                }]
            },
            healing: {
                name: "mineral water",
                value: 15,
                chance: 0.15
            },
        }],
        roomProbabilities: [[1, 0], [0.75, 0.25], [0.5, 0.5]],
        reward: {
            gold: {
                chance: 1.0,
                amount: [200, 350]
            },
            npcImprovementItem: [{
                npc: "Alchemist",
                chance: 0.5
            }, {
                npc: "Scout",
                chance: 0.5
            }]
        },
        pathDepth: 3,
        pathProbabilities: [0.4, 0.3, 0.2, 0.1],
        nextAreaTypeProbabilities: {
            FOREST: 0.05,
            MOUNTAINS: 0.1,
            RIVERLAND: 0.05,
            FIELDS: 0.1,
            DESERT: 0.1,
            SWAMP: 0.1,
            CAVES: 0.0,
            SEASIDE: 0.3,
            CASTLE: 0.2
        }
    },
    SEASIDE: {
        name: "Seaside",
        image: "seaside.png",
        roomTypes: [{
            roomName: "shoreline",
            roomImage: "seaside.png",
            pathImage: "seaside-path.png",
            light: true,
            enemyChance: 0.75,
            enemyTypeProbabilities: {
                ARMORED_CRAB: 0.4,
                VIKING: 0.35,
                SEA_DRAGON: 0.25
            },
            loot: {
                gold: {
                    chance: 0.65,
                    amount: [5, 400]
                },
                weapons: [{
                    type: WeaponTypes.VIKING_AXE,
                    chance: 0.02
                }, {
                    type: WeaponTypes.FINE_SWORD,
                    chance: 0.02
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.02
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.8, 0.1, 0.1]
                }, {
                    type: PotionTypes.LARGE_LIFE,
                    chances: [0.925, 0.075]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_MAGIC,
                    chances: [0.95, 0.05]
                }]
            },
            healing: {
                name: "seagull eggs",
                value: 10,
                chance: 0.2
            },
        }, {
            roomName: "harbor",
            roomImage: "seaside2.png",
            pathImage: "seaside2-path.png",
            light: true,
            enemyChance: 0.8,
            enemyTypeProbabilities: {
                ARMORED_CRAB: 0.3,
                VIKING: 0.25,
                SEA_DRAGON: 0.3,
                VIKING_BERSERKER: 0.15
            },
            loot: {
                gold: {
                    chance: 0.75,
                    amount: [5, 450]
                },
                weapons: [{
                    type: WeaponTypes.VIKING_AXE,
                    chance: 0.02
                }, {
                    type: WeaponTypes.FINE_SWORD,
                    chance: 0.02
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.02
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.8, 0.1, 0.1]
                }, {
                    type: PotionTypes.LARGE_LIFE,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_MAGIC,
                    chances: [0.95, 0.05]
                }]
            },
            healing: {
                name: "seagull eggs",
                value: 10,
                chance: 0.2
            },
        }, {
            roomName: "shoreline",
            roomImage: "seaside.png",
            pathImage: "seaside-path.png",
            light: true,
            enemyChance: 0.85,
            enemyTypeProbabilities: {
                ARMORED_CRAB: 0.1,
                VIKING: 0.1,
                SEA_DRAGON: 0.3,
                VIKING_BERSERKER: 0.25,
                MUTANT_SQUID: 0.25
            },
            loot: {
                gold: {
                    chance: 0.75,
                    amount: [5, 500]
                },
                weapons: [{
                    type: WeaponTypes.VIKING_AXE,
                    chance: 0.02
                }, {
                    type: WeaponTypes.FINE_SWORD,
                    chance: 0.02
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.02
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.8, 0.1, 0.1]
                }, {
                    type: PotionTypes.LARGE_LIFE,
                    chances: [0.85, 0.1, 0.025, 0.025]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_MAGIC,
                    chances: [0.95, 0.05]
                }]
            },
            healing: {
                name: "seagull eggs",
                value: 10,
                chance: 0.2
            },
        }],
        roomProbabilities: [[1, 0, 0], [0.75, 0.25, 0], [0.5, 0.3, 0.2], [0.3, 0.5, 0.2], [0.15, 0.5, 0.35]],
        reward: {
            gold: {
                chance: 1.0,
                amount: [250, 450]
            },
            npcImprovementItem: [{
                npc: "Scout",
                chance: 0.5
            }, {
                npc: "Trainer",
                chance: 0.5
            }]
        },
        pathDepth: 4,
        pathProbabilities: [0.7, 0.2, 0.1],
        nextAreaTypeProbabilities: {
            FOREST: 0.1,
            MOUNTAINS: 0.1,
            RIVERLAND: 0.05,
            FIELDS: 0.1,
            DESERT: 0.1,
            SWAMP: 0.2,
            CAVES: 0.1,
            SEASIDE: 0.0,
            CASTLE: 0.25
        }
    },
    CASTLE: {
        name: "Castle",
        image: "castle.png",
        roomTypes: [{
            roomName: "room",
            roomImage: "castle.png",
            pathImage: "castle-path.png",
            light: false,
            enemyChance: 0.8,
            enemyTypeProbabilities: {
                SKELETON: 0.5,
                DARK_KNIGHT: 0.4,
                DARK_SORCERER: 0.1
            },
            loot: {
                gold: {
                    chance: 0.75,
                    amount: [5, 500]
                },
                weapons: [{
                    type: WeaponTypes.LARGE_AXE,
                    chance: 0.02
                }, {
                    type: WeaponTypes.FINE_SWORD,
                    chance: 0.01
                }, {
                    type: WeaponTypes.MASTERPIECE_SWORD,
                    chance: 0.005
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.02
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.95, 0.025, 0.025]
                }, {
                    type: PotionTypes.LARGE_LIFE,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_MAGIC,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }]
            },
            healing: {
                name: "holy water",
                value: 20,
                chance: 0.1
            },
        }, {
            roomName: "room",
            roomImage: "castle.png",
            pathImage: "castle-path.png",
            light: false,
            enemyChance: 0.9,
            enemyTypeProbabilities: {
                SKELETON: 0.4,
                DARK_KNIGHT: 0.4,
                DARK_SORCERER: 0.1,
                TEMPLAR_KNIGHT: 0.1
            },
            loot: {
                gold: {
                    chance: 0.75,
                    amount: [5, 600]
                },
                weapons: [{
                    type: WeaponTypes.LARGE_AXE,
                    chance: 0.02
                }, {
                    type: WeaponTypes.FINE_SWORD,
                    chance: 0.01
                }, {
                    type: WeaponTypes.MASTERPIECE_SWORD,
                    chance: 0.01
                }, {
                    type: WeaponTypes.MAGE_STAFF,
                    chance: 0.02
                }],
                potions: [{
                    type: PotionTypes.MEDIUM_LIFE,
                    chances: [0.95, 0.025, 0.025]
                }, {
                    type: PotionTypes.LARGE_LIFE,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }, {
                    type: PotionTypes.MEDIUM_MAGIC,
                    chances: [0.9, 0.05, 0.05]
                }, {
                    type: PotionTypes.LARGE_MAGIC,
                    chances: [0.9, 0.05, 0.025, 0.025]
                }]
            },
            healing: {
                name: "holy water",
                value: 20,
                chance: 0.1
            },
        }],
        roomProbabilities: [[1, 0], [0.75, 0.25], [0.5, 0.5]],
        reward: {
            gold: {
                chance: 1.0,
                amount: [250, 500]
            },
            npcImprovementItem: [{
                npc: "Scout",
                chance: 0.5
            }, {
                npc: "Trainer",
                chance: 0.5
            }]
        },
        pathDepth: 3,
        pathProbabilities: [0.35, 0.35, 0.2, 0.1],
        nextAreaTypeProbabilities: {
            FOREST: 0.1,
            MOUNTAINS: 0.1,
            RIVERLAND: 0.2,
            FIELDS: 0.1,
            DESERT: 0.05,
            CAVES: 0.15,
            SEASIDE: 0.15,
            SWAMP: 0.15,
            CASTLE: 0.0
        }
    }
};