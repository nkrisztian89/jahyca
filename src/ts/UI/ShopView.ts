/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface ImprovementItemAppliedEventParams {
    item: NPCImprovementItem;
    npc: NPC;
    improvement: number;
}

interface QuestItemGivenEventParams {
    item: QuestItem;
    npc: NPC;
}

interface ShopViewSettings {
    showAvailableOnly: boolean;
    showAffordableOnly: boolean;
}

class ShopView extends Panel {

    private _player: Character;
    private _npc: NPC | null;
    private _titlePanel: Panel;
    private _titleLabel: Label;
    private _filterButton: ImageView;
    private _townButton: IconButton;
    private _upgradeButton: IconButton;
    private _servicesPanel: Panel;
    private _emptyLabel: Label;
    private _serviceViews: ServiceView[];
    private _playerView: CharacterView;
    private _settings: ShopViewSettings;
    private _first: boolean;

    constructor(game: Game, player: Character) {

        super({
            className: "shop",
            background: 'url("img/shop.png") center center / cover'
        });

        this.log("created RoomView instance", 2);

        this._player = player;
        this._npc = null;
        this._first = true;

        this._settings = {
            showAvailableOnly: false,
            showAffordableOnly: false
        };

        // title

        this._titlePanel = new Panel({
            background: "rgba(255,255,255,0.5)"
        });
        this.add(this._titlePanel);

        this._titleLabel = new Label({
            className: "title",
            text: "Shop",
            fontSize: "4vh"
        });
        this._titlePanel.add(this._titleLabel);
        
        this._filterButton = new ImageView({
            className: "menu-button filter",
            source: "img/eye.png",
            onclick: () => {
                game.showPopup("Which services to show?", "filter-menu", [{
                    caption: "Show all",
                    action: () => {
                        this._settings.showAvailableOnly = false;
                        this._settings.showAffordableOnly = false;
                        this.render();
                    }
                }, {
                    caption: "Show available",
                    action: () => {
                        this._settings.showAvailableOnly = true;
                        this._settings.showAffordableOnly = false;
                        this.render();
                    }
                }, {
                    caption: "Show affordable",
                    action: () => {
                        this._settings.showAvailableOnly = true;
                        this._settings.showAffordableOnly = true;
                        this.render();
                    }
                }]);
            }
        });
        this._titlePanel.add(this._filterButton);

        this._townButton = new IconButton({
            caption: "Back to Town",
            image: "img/world/town.png",
            className: "travelButton",
            action: () => {
                this.hidden = true;
                game.renderTown();
            }
        });
        this.add(this._townButton);

        // upgrade

        this._upgradeButton = new IconButton({
            caption: "Upgrade",
            image: "img/upgrade.png",
            className: "upgradeButton",
            action: () => {
                this._player.upgradeNPC();
                this.render();
            }
        });
        this.add(this._upgradeButton);

        this.addLineBreak();

        // services

        this._servicesPanel = new Panel({
            className: "services"
        });
        this.add(this._servicesPanel);
        
        this._emptyLabel = new Label({
            className: "empty-label",
            text: "No services match the selected criteria."
        });
        this._servicesPanel.add(this._emptyLabel);
        
        this._serviceViews = [];

        this.addLineBreak();

        // player

        this._playerView = game.playerView;

        this.handleImprovementItemApplied = this.handleImprovementItemApplied.bind(this);
        this.handleQuestItemGiven = this.handleQuestItemGiven.bind(this);
        this.handleServicesUnlocked = this.handleServicesUnlocked.bind(this);
        this._handleWeaponSwapped = this._handleWeaponSwapped.bind(this);
    }

    public save(): ShopViewSettings {
        return this._settings;
    }

    public load(settings: ShopViewSettings): void {
        this._settings = settings || {
            showAvailableOnly: false,
            showAffordableOnly: false
        };
    }

    public handleImprovementItemApplied(params?: ImprovementItemAppliedEventParams): void {
        if (!params) {
            throw new Error("Can't apply improvement item - missing parameters!");
        }
        game.showPopup(
            "You have given the " + params.item.name + " to the " + params.npc.name +
            ((params.improvement > 0) ? ", services are improved by " + Utils.percent(params.improvement) + "!" : ", but services cannot be further improved."),
            "info",
            [{
                caption: "OK",
                action: () => {
                    this.render();
                }
            }]);

    }

    public handleQuestItemGiven(params?: QuestItemGivenEventParams): void {
        if (!params) {
            throw new Error("Can't give quest item - missing parameters!");
        }
        game.showPopup(
            "You have given the " + params.item.name + " to the " + params.npc.name + "!",
            "info quest",
            [{
                caption: "OK",
                action: () => {
                    this.render();
                }
            }]);

    }

    public handleServicesUnlocked(): void {
        game.showPopup(
            "You have unlocked new services!",
            "info",
            [{
                caption: "OK",
                action: () => {
                    this.render();
                }
            }]);
    }
    
    private _handleWeaponSwapped(): void {
        if (!this.hidden) {
            this.render();
        }
    }

    public render(npc?: NPC | null): void {
        this.log("rendering ShopView", 3);
        
        if ((this._player !== game.player) || this._first) {
            if (this._player) {
                this._player.removeEventHandler(CharacterEvent.WEAPONS_SWAPPED, this._handleWeaponSwapped);
            }
            this._player = game.player;
            this._player.addEventHandler(CharacterEvent.WEAPONS_SWAPPED, this._handleWeaponSwapped);
        }

        if (this._player.dead) {
            this.hidden = true;
            return;
        }

        npc = npc || this._player.npc;
        if (!npc) {
            throw new Error("Cannot render ShopView without an NPC!");
        }
        if (npc !== this._npc) {
            if (this._npc) {
                this._npc.removeEventHandler(NPCEvent.IMPROVEMENT_ITEM_APPLIED, this.handleImprovementItemApplied);
                this._npc.removeEventHandler(NPCEvent.QUEST_ITEM_GIVEN, this.handleQuestItemGiven);
                this._npc.removeEventHandler(NPCEvent.SERVICES_UNLOCKED, this.handleServicesUnlocked);
            }
            npc.addEventHandler(NPCEvent.IMPROVEMENT_ITEM_APPLIED, this.handleImprovementItemApplied);
            npc.addEventHandler(NPCEvent.QUEST_ITEM_GIVEN, this.handleQuestItemGiven);
            npc.addEventHandler(NPCEvent.SERVICES_UNLOCKED, this.handleServicesUnlocked);
            this._npc = npc;
        }
        let i: number;

        // title
        this._titleLabel.text = "At the " + npc.name;

        // upgrade button
        if (npc.canUpgrade()) {
            this._upgradeButton.caption = "Upgrade " + npc.name;
            this._upgradeButton.setCost(npc.upgradeCost, this._player.gold);
            this._upgradeButton.disabled = this._player.gold < npc.upgradeCost;
            this._upgradeButton.hidden = false;
        } else {
            this._upgradeButton.hidden = true;
        }
        
        let empty = true;

        // navigation
        const services = npc.services;
        for (i = 0; i < services.length; i++) {
            if (this._serviceViews.length <= i) {
                this._serviceViews.push(new ServiceView({
                    action: function (this: ShopView, index: number) {
                        if (this._player.npc) {
                            const service = this._player.npc.services[index];
                            if (!service) {
                                throw new Error("Can't buy service: NPC or service missing!");
                            }
                            this._player.buyService(service, this._player.npc, () => {
                                this.render();
                            });
                        }
                    }.bind(this, i)
                }));
                this._servicesPanel.add(this._serviceViews[i]);
            }
            this._serviceViews[i].hidden =
                (this._settings.showAvailableOnly && !this._player.canUseService(services[i])) ||
                (this._settings.showAffordableOnly && !this._player.canBuyService(services[i], npc));
            if (!this._serviceViews[i].hidden) {
                empty = false;
            }
            this._serviceViews[i].disabled = !this._player.canBuyService(services[i], npc);
            this._serviceViews[i].render(services[i], npc, this._player);
        }
        while (i < this._serviceViews.length) {
            this._serviceViews[i].hidden = true;
            i++;
        }
        
        this._emptyLabel.hidden = !empty;

        // player
        this.add(this._playerView);
        this._playerView.render(this._player, this);
        
        this._first = false;
    }
}