/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface Tips {
    fast?: boolean;
    tough?: boolean;
}

class RoomView extends Panel {

    private _tipsShown: Tips;
    private _newRoom: boolean;
    private _lastRoom: Room | null;
    private _titlePanel: Panel;
    private _titleLabel: Label;
    private _backButton: IconButton;
    private _worldButton: IconButton;
    private _pathsPanel: Panel;
    private _pathViews: PathView[];
    private _roomPanel: Panel;
    private _roomNotePanel: Panel;
    private _roomNoteLabel: Label;
    private _enemyView: EnemyView;
    private _chestView: ChestView;
    private _playerView: CharacterView;
    private _player: Character;
    private _first: boolean;

    constructor(game: Game) {

        super({
            className: "room"
        });

        this.log("created RoomView instance", 2);

        this._player = game.player;
        this._first = true;

        this._tipsShown = {};

        this._newRoom = false;
        this._lastRoom = null;

        // title

        this._titlePanel = new Panel({
            className: "title",
            background: "rgba(255,255,255,0.5)"
        });
        this.add(this._titlePanel);

        this._titleLabel = new Label({
            className: "title",
            text: "Room",
            fontSize: "4vh"
        });
        this._titlePanel.add(this._titleLabel);

        // navigation

        this._backButton = new IconButton({
            className: "travelButton",
            caption: "Go back",
            image: "img/return-arrow.png",
            action: () => {
                this._newRoom = false;
                this._player.goToParentRoom();
                this.render(false, true);
            }
        });
        this.add(this._backButton);

        this._worldButton = new IconButton({
            className: "travelButton",
            caption: "Go back to world",
            image: "img/map.png",
            action: () => {
                this._player.exitArea();
                this._lastRoom = null;
                game.curtain(() => {
                    this.hidden = true;
                    game.renderWorld();
                });
            }
        });
        this.add(this._worldButton);

        this._pathsPanel = new Panel();
        this.add(this._pathsPanel);
        this._pathViews = [];

        // things in the room

        this._roomPanel = new Panel();
        this.add(this._roomPanel);

        this._roomNotePanel = new Panel({
            className: "room-note"
        });
        this._roomPanel.add(this._roomNotePanel);

        this._roomNoteLabel = new Label({
            text: "This room is empty."
        });
        this._roomNotePanel.add(this._roomNoteLabel);
        this._roomNotePanel.hidden = true;

        this._roomPanel.addLineBreak();

        this._enemyView = new EnemyView((event: MouseEvent, skillIndex: number = -1) => {
            if (this._player.enemy?.dead) {
                return;
            }
            this._player.attack((skillIndex >= 0) ? this._player.usableSkills[skillIndex] : null);
            this._enemyView.playAttackAnimation();
            if (!this._player.dead) {
                if (this._player.enemy?.dead) {
                    const loot = this._player.enemy?.getLoot();
                    if (loot) {
                        game.showPopup(game.getLootMessage(loot), "info bag");
                        game.takeLoot(loot, () => {
                            this.render()
                        });
                    }
                    if (game.unlockPortrait(Utils.removeExtension(this._player.enemy?.typeImage))) {
                        game.showPopup(`You have unlocked a new portrait for your leaderboard heroes!<br>When in town, go to <img class="inline" src="img/cog.png"> > <span class="button">Leaderboard</span> > <span class="button">My heroes</span> to equip.`, "info");
                    }
                }
                this.render();
                const area = this._player.area;
                if (area && area.enemyCount <= 0) {
                    game.showPopup("You have defeated all enemies in this area!" +
                        ((area.pathCount > 1) ? "<br>" + area.pathCount + " new paths have opened on the world map." :
                            ((area.pathCount > 0) ? "<br>A new path has opened on the world map." : "")), "info map");
                    if (area.reward) {
                        let message = "You have found a hidden chest, and in it...<br>" + game.getLootMessage(area.reward);
                        game.showPopup(message, "info bag");
                        game.takeLoot(area.reward, () => {
                            area.clearReward();
                            this.render();
                        });
                    }
                }
            } else {
                this.render();
            }
            event.stopPropagation();
        });
        this._roomPanel.add(this._enemyView);

        this._chestView = new ChestView(() => {
            this._player.openTreasure(() => {
                this.render();
            });
        });
        this._roomPanel.add(this._chestView);

        // player

        this._playerView = game.playerView;
        
        this._handleWeaponSwapped = this._handleWeaponSwapped.bind(this);
    }
    
    private _handleWeaponSwapped(): void {
        if (!this.hidden) {
            this.render();
        }
    }

    public render(newArea: boolean = false, zoomOut: boolean = false) {
        if (newArea) {
            this._newRoom = true;
        }
        
        if ((this._player !== game.player) || this._first) {
            if (this._player) {
                this._player.removeEventHandler(CharacterEvent.WEAPONS_SWAPPED, this._handleWeaponSwapped);
            }
            this._player = game.player;
            this._player.addEventHandler(CharacterEvent.WEAPONS_SWAPPED, this._handleWeaponSwapped);
        }

        if (this._player.dead) {
            if (!this.hidden) {
                const area = this._player.area;
                if (!area) {
                    throw new Error("Area is not set!");
                }
                game.showPopup(
                    "You have been gravely wounded!<br>You have to throw everything away to escape and get back to town!" +
                    ((this._player.gold > this._player.safeGold) ? "<br>You lose " + (this._player.gold - this._player.safeGold) + " gold!" : "") +
                    (this._player.weapon ? "<br>You lose " + this._player.weapon.typeName + "!" : "") +
                    (this._player.backupWeapon ? "<br>You lose " + this._player.backupWeapon.typeName + "!" : "") +
                    (this._player.getPotionAmount() ? "<br>You lose " + this._player.getPotionAmount() + " potions!" : "") +
                    "<br>You will not be able to find the same paths in the " + area.typeName + " again.",
                    "info defeat",
                    [{
                        caption: "Get away",
                        action: () => {
                            this._player.revive();
                            if (!area.hasQuest()) {
                                area.clearModifiers();
                            }
                            area.generateRooms(area.hasQuest() ? area.level : 0);
                            game.renderTown();
                        }
                    }]);
            }
            this.hidden = true;
            return;
        }

        this.log("rendering RoomView", 3);

        const room = this._player.room;

        if (!room) {
            throw new Error("Cannot render RoomView without a room!");
        }

        const finishRender = () => {
            const background = 'url("img/rooms/' + room.roomImage + '") center center / cover';
            if (background !== this.background) {
                this.background = background;
            }
            if (zoomOut) {
                this.playAnimation("zoom-out", 500);
            }

            if (room.enemy) {
                if (room.enemy.fast && !this._tipsShown.fast) {
                    this._tipsShown.fast = true;
                    game.showPopup("This enemy is " + Utils.span("enemyProperty", "fast") + "!<br>" +
                        "Once you have attacked, you will not be able to move away from fights with " + Utils.span("enemyProperty", "fast") + " enemies.",
                        "info battle");
                }
                if (room.enemy.toughness && !this._tipsShown.tough) {
                    this._tipsShown.tough = true;
                    game.showPopup("This enemy is " + Utils.span("enemyProperty", "tough") + "!<br>" +
                        "Your physical damage done to this enemy will be reduced by its " + Utils.span("enemyProperty", "toughness") + " (in this case " + room.enemy.toughness + ")",
                        "info battle");
                }
            }

            if (!room.area) {
                throw new Error("Cannot render RoomView - area of room not set!");
            }

            // title
            this._titleLabel.text = (this._player.fighting ? "A battle in" : (this._newRoom ? "Exploring" : "Wandering in")) + " the " + room.area.typeName;
            this._titlePanel.setClass("battle-title", this._player.fighting);

            // navigation
            let i;
            for (i = 0; i < room.pathCount; i++) {
                if (this._pathViews.length <= i) {
                    this._pathViews.push(new PathView({
                        action: function (this: RoomView, index: number) {
                            if (!this._player) {
                                throw new Error("Can't enter room without a player being set!");
                            }
                            if (!this._player.room) {
                                throw new Error("Can't enter room - room not set!");
                            }
                            this._newRoom = !this._player.room.getPath(index).visited;
                            this._player.enterRoom(this._player.room.getPath(index));
                            this.playAnimation("zoom-in", 500);
                            this.render();
                        }.bind(this, i)
                    }, this._player));
                    this._pathsPanel.add(this._pathViews[i]);
                }
                this._pathViews[i].hidden = false;
                this._pathViews[i].disabled = room.hasEnemy;
                this._pathViews[i].render(room.getPath(i));
            }
            while (i < this._pathViews.length) {
                this._pathViews[i].hidden = true;
                i++;
            }
            this._backButton.disabled = !!room.enemy && room.enemy.fast && this._player.fighting;
            this._backButton.light = room.light;
            this._backButton.imageSource = "img/return-arrow" + (room.light ? "-dark.png" : ".png");
            this._backButton.hidden = !room.parent;
            this._worldButton.disabled = !!room.enemy && room.enemy.fast && this._player.fighting;
            this._worldButton.light = room.light;
            this._worldButton.hidden = !!room.parent;

            // things in the room

            this._roomNoteLabel.text = "This " + room.roomName + " is empty.";
            this._roomNotePanel.hidden = !!(room.enemy || room.chest);

            // enemy
            if (room.enemy) {
                this._enemyView.setClass("side", !!room.chest);
                this._enemyView.render(room.enemy, this._player.weapon, this._player.usableSkills);
            } else {
                this._enemyView.hidden = true;
            }

            // treasure chest
            if (room.chest) {
                this._chestView.setClass("side", !!room.enemy);
                this._chestView.render(room.chest);
                this._chestView.disabled = room.hasEnemy || !room.hasTreasure;
            } else {
                this._chestView.hidden = true;
            }

            // player
            this.add(this._playerView);
            this._playerView.render(this._player, this);

            this._lastRoom = room;

            if (room.healing && !room.hasEnemy && !this._player.dead) {
                if (this._player.hp < this._player.maxHP) {
                    const healing = this._player.naturalHeal(room.healing.value);
                    game.showPopup("You have found " + room.healing.name + " and recovered " + healing + " life.", "info heal", [{
                        caption: "OK",
                        action: () => {
                            this.render();
                        }
                    }]);
                }
                room.clearHealing();
            }
            
            this._first = false;
        };

        // background
        if ((room !== this._lastRoom) && !game.curtainInProgress) {
            game.curtain(finishRender);
        } else {
            finishRender();
        }
    }
}