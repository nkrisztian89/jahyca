/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

class PathView extends OptionView {
    
    private _player: Character;
    private _enemyImage: ImageView;
    private _chestImage: ImageView;
    private _unknownImage: ImageView;
    private _unknownEnemyImage: ImageView;
    
    constructor(params: OptionViewParams, player: Character) {

        params.className = "path light " + (params.className || "");
        params.textless = true;

        super(params);
        
        this.log("created PathView instance", 2);

        this._player = player;

        this._enemyImage = new ImageView({
            className: "enemy"
        });
        this._enemyImage.hidden = true;
        this.add(this._enemyImage);

        this._chestImage = new ImageView({
            className: "chest"
        });
        this._chestImage.hidden = true;
        this.add(this._chestImage);

        this._unknownImage = new ImageView({
            className: "unknown",
            source: "img/rooms/unknown.png"
        });
        this.add(this._unknownImage);

        this._unknownEnemyImage = new ImageView({
            className: "unknown",
            source: "img/rooms/unknownenemy.png"
        });
        this.add(this._unknownEnemyImage);
    }

    render(path: Room) {
        this.log("rendering PathView", 3);

        this.image = "img/rooms/" + path.pathImage;

        if (path.visited && path.enemy) {
            this._enemyImage.source = "img/enemies/" + path.enemy.typeImage;
            this._enemyImage.setClass("side", !!path.chest && (!path.chest.empty || !path.chest.goldOnly));
            this._enemyImage.hidden = false;
        } else {
            this._enemyImage.hidden = true;
        }

        if (path.visited && path.chest && (!path.chest.empty || !path.chest.goldOnly)) {
            this._chestImage.source = "img/" + (path.chest.goldOnly ? "gold-pile.png" : (path.chest.empty ? "chest_open.png" : "chest_closed.png"));
            this._chestImage.opacity = (path.chest.empty ? 0.5 : 1.0);
            this._chestImage.setClass("side", !!path.enemy);
            this._chestImage.hidden = false;
        } else {
            this._chestImage.hidden = true;
        }

        if (path.visited) {
            this._unknownImage.hidden = true;
            this._unknownEnemyImage.hidden = true;
        } else {
            if (path.enemy && (this._player.tracking > 0)) {
                this._unknownImage.hidden = true;
                this._unknownEnemyImage.hidden = false;
            } else {
                this._unknownImage.hidden = false;
                this._unknownEnemyImage.hidden = true;
            }
        }

        this.setClass("traversed", path.traversed);
        this.setClass("visited", path.visited && !path.traversed);
    }
}