/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface WorldViewSettings {
    hideCompleted: boolean;
    sort: boolean;
}

class WorldView extends Panel {

    private _game: Game;
    private _roomView: RoomView;
    private _settings: WorldViewSettings;
    private _titlePanel: Panel;
    private _titleLabel: Label;
    private _menuPanel: Panel;
    private _hideCompleteButton: IconButton;
    private _sortButton: IconButton;
    private _townButton: IconButton;
    private _areasPanel: Panel;
    private _areas: Area[];
    private _areaViews: AreaView[];

    constructor(game: Game) {

        super({
            className: "world",
            background: 'url("img/world.png") center center / cover'
        });

        this.log("created WorldView instance", 2);

        this._game = game;

        this._roomView = new RoomView(game);
        this._roomView.hidden = true;
        this._roomView.addToPage();

        this._settings = {
            hideCompleted: false,
            sort: false
        };

        // title

        this._titlePanel = new Panel({
            background: "rgba(255,255,255,0.5)"
        });
        this.add(this._titlePanel);

        this._titleLabel = new Label({
            className: "title",
            text: "World map",
            fontSize: "4vh"
        });
        this._titlePanel.add(this._titleLabel);

        this._menuPanel = new Panel({
            className: "topMenu"
        });
        this.add(this._menuPanel);

        this._hideCompleteButton = new IconButton({
            caption: "Hide completed",
            image: "img/hide-completed.png",
            className: "settingButton light",
            action: () => {
                this._settings.hideCompleted = !this._settings.hideCompleted;
                this.render();
            }
        });
        this._menuPanel.add(this._hideCompleteButton);

        this._sortButton = new IconButton({
            caption: "Sort areas",
            image: "img/sort.png",
            className: "settingButton light",
            action: () => {
                this._settings.sort = !this._settings.sort;
                this.render();
            }
        });
        this._menuPanel.add(this._sortButton);

        this._townButton = new IconButton({
            caption: "Back to Town",
            image: "img/world/town.png",
            className: "travelButton light",
            action: () => {
                game.autoSave();
                game.curtain(() => {
                    this.hidden = true;
                    game.renderTown();
                });
            }
        });
        this.add(this._townButton);

        this.addLineBreak();

        this._areasPanel = new Panel({
            className: "areas"
        });
        this.add(this._areasPanel);

        this._areas = [];
        this._areaViews = [];

        this.hidden = true;
    }

    public save(): WorldViewSettings {
        return this._settings;
    }

    public load(settings: WorldViewSettings): void {
        this._settings = settings || {
            hideCompleted: false,
            sort: false
        };
    }

    public render(): void {
        if (this._game.player.dead) {
            this.hidden = true;
            return;
        }

        this.log("rendering WorldView", 3);

        this._areas = [];
        this._game.forAreas(a => this._areas.push(a));

        this._hideCompleteButton.caption = this._settings.hideCompleted ? "Show completed" : "Hide completed";
        this._hideCompleteButton.hidden = this._areas.length < 2;
        this._sortButton.caption = this._settings.sort ? "Unsort areas" : "Sort areas";
        this._sortButton.hidden = this._areas.length < 3;


        if (this._settings.sort) {
            this._areas.sort((a, b) => a.getSortValue() - b.getSortValue());
        }

        let i;

        // navigation
        for (i = 0; i < this._areas.length; i++) {
            if (this._areaViews.length <= i) {
                this._areaViews.push(new AreaView({
                    action: function (this: WorldView, index: number) {
                        this._game.curtain(() => {
                            const newArea = !this._areas[index].visited;
                            this._game.player.enterArea(this._areas[index]);
                            this.hidden = true;
                            this._roomView.render(newArea);
                            this._roomView.hidden = false;
                        });
                    }.bind(this, i)
                }));
                this._areasPanel.add(this._areaViews[i]);
            }
            this._areaViews[i].hidden = this._settings.hideCompleted && !this._areas[i].hasEnemies();
            this._areaViews[i].render(this._areas[i]);
        }
        while (i < this._areaViews.length) {
            this._areaViews[i].hidden = true;
            i++;
        }
    }
}