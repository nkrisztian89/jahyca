/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface TownViewSettings {
    shop: ShopViewSettings;
}

class TownView extends Panel {

    private _shopView: ShopView;
    private _player: Character;
    private _town: Town;
    private _titlePanel: Panel;
    private _titleLabel: Label;
    private _worldButton: IconButton;
    private _menuButton: ImageView;
    private _saveButton: ImageView;
    private _loadButton: ImageView;
    private _npcsPanel: Panel;
    private _npcViews: NPCView[];

    constructor(game: Game, town: Town, player: Character) {

        super({
            className: "town",
            background: 'url("img/town.png") center center / cover'
        });
        
        this.log("created TownView instance", 2);

        this._shopView = new ShopView(game, player);
        this._shopView.hidden = true;
        this._shopView.addToPage();

        this._player = player;
        this._town = town;

        // title

        this._titlePanel = new Panel({
            background: "rgba(255,255,255,0.5)"
        });
        this.add(this._titlePanel);

        this._titleLabel = new Label({
            className: "title",
            text: "Town",
            fontSize: "4vh"
        });
        this._titlePanel.hidden = true;
        this._titlePanel.add(this._titleLabel);

        this._worldButton = new IconButton({
            caption: "To World Map",
            className: "travelButton",
            image: "img/map.png",
            action: () => {
                game.autoSave();
                game.curtain(() => {
                    this._player.leaveTown();
                    this.hidden = true;
                    game.renderWorld();
                });
            }
        });
        this._worldButton.hidden = true;
        this.add(this._worldButton);

        this.addLineBreak();
        
        this._menuButton = new ImageView({
            className: "menu-button menu",
            source: "img/cog.png",
            onclick: () => {
                game.showMainMenu(true, true, () => {
                    this._loadButton.hidden = !game.canLoad();
                });
            }
        });
        this._titlePanel.add(this._menuButton);

        this._saveButton = new ImageView({
            className: "menu-button save",
            source: "img/save.png",
            onclick: () => {
                game.showSaveMenu(() => {
                    this._loadButton.hidden = !game.canLoad();
                });
            }
        });
        this._titlePanel.add(this._saveButton);

        this._loadButton = new ImageView({
            className: "menu-button load",
            source: "img/load.png",
            onclick: () => {
                game.showLoadMenu(true);
            }
        });
        this._titlePanel.add(this._loadButton);
        this._loadButton.hidden = !game.canLoad();
        
        this._npcsPanel = new Panel({
            className: "npcs"
        });
        this.add(this._npcsPanel);

        this._npcViews = [];
    }
    
    public save(): TownViewSettings {
        return {
            shop: this._shopView.save()
        };
    }
    
    public load(settings: TownViewSettings): void {
        this._shopView.load(settings?.shop);
    }

    public render(): void {
        this._player = game.player;

        if (this._player.dead) {
            this.hidden = true;
            return;
        }

        this.log("rendering TownView", 3);

        this._titlePanel.hidden = false;
        this._worldButton.hidden = false;
        
        this._loadButton.hidden = !game.canLoad();

        let i;

        // navigation
        for (i = 0; i < this._town.npcs.length; i++) {
            if (this._npcViews.length <= i) {
                this._npcViews.push(new NPCView({
                    action: function (this: TownView, index: number) {
                        this._shopView.render(this._town.npcs[index]);
                        this._player.visitNPC(this._town.npcs[index]);
                        this.hidden = true;
                        this._shopView.hidden = false;
                    }.bind(this, i)
                }));
                this._npcsPanel.add(this._npcViews[i]);
            }
            this._npcViews[i].hidden = false;
            this._npcViews[i].render(this._town.npcs[i]);
        }
        while (i < this._npcViews.length) {
            this._npcViews[i].hidden = true;
            i++;
        }
    }
    
    public renderShop(): void {
        this._shopView.render();
    }
}