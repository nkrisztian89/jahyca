/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

class ChestView extends Panel {

    private _imageContainer: Panel;
    private _goldImage: ImageView;
    private _sparkleImage: ImageView;
    private _image: ImageView;
    private _openImage: ImageView;
    private _textPanel: Panel;
    private _label: Label;
    private _takeAnimation: number;
    private _taken: boolean;

    constructor(onclick: MouseEventHandler) {

        super({
            className: "chest",
            onclick: onclick
        });

        this.log("created ChestView instance", 2);

        this._taken = false;
        this._takeAnimation = -1;

        this._imageContainer = new Panel({
            className: "chest-container"
        });
        this.add(this._imageContainer);

        this._goldImage = new ImageView({
            source: "img/gold-pile.png",
            className: "gold"
        });
        this._goldImage.hidden = true;
        this._imageContainer.add(this._goldImage);

        this._sparkleImage = new ImageView({
            source: "img/sparkle.png",
            className: "sparkle"
        });
        this._sparkleImage.hidden = true;
        this._imageContainer.add(this._sparkleImage);

        this._image = new ImageView({
            source: "img/chest_closed.png"
        });
        this._image.hidden = true;
        this._imageContainer.add(this._image);

        this._openImage = new ImageView({
            source: "img/chest_open.png"
        });
        this._openImage.hidden = true;
        this._imageContainer.add(this._openImage);

        this.addLineBreak();

        this._textPanel = new Panel({
            className: "chest-text"
        });
        this.add(this._textPanel);

        this._label = new Label({
            text: "Treasure chest"
        });
        this._textPanel.add(this._label);
    }

    render(chest: Chest) {
        this.log("rendering ChestView", 3);

        if (chest.empty) {
            this._goldImage.hidden = true;
            this._sparkleImage.hidden = true;
            this._image.hidden = true;
            this._openImage.hidden = chest.goldOnly;
            this._label.text = chest.goldOnly ? "Treasure pile<br>Taken" : "Treasure chest<br>Empty";
            this.opacity = 0.5;
        } else if (chest.goldOnly) {
            this._goldImage.hidden = false;
            this._sparkleImage.hidden = false;
            this._image.hidden = true;
            this._openImage.hidden = true;
            this._label.text = "Treasure pile<br>Gold: " + chest.gold;
            this.opacity = 1.0;
        } else {
            this._goldImage.hidden = true;
            this._sparkleImage.hidden = true;
            this._image.hidden = false;
            this._openImage.hidden = true;
            this._label.text = "Treasure chest<br>Closed";
            this.opacity = 1.0;
        }
        
        if (!chest.empty || !chest.goldOnly) {
            this._taken = false;
            this.hidden = false;
        }

        if (chest.empty && chest.goldOnly && !this._taken) {
            this.hidden = false;
            this._goldImage.hidden = false;
            this._takeAnimation = setTimeout(() => {
                this.setClass("taken", true);
                this._takeAnimation = setTimeout(() => {
                    this.hidden = true;
                    this._takeAnimation = -1;
                }, 300);
            }, 0);
            this._taken = true;
        } else {
            this.setClass("taken", false);
            if (this._takeAnimation !== -1) {
                clearTimeout(this._takeAnimation);
                this._takeAnimation = -1;
            }
        }

        this.setClass("open", chest.empty && !chest.goldOnly);
    }
}