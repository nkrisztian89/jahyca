/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

class ServiceView extends OptionView {
    
    private _costLabel: MoneyLabel;
    
    constructor(params: OptionViewParams) {

        params.className = "service " + (params.className || "");

        super(params);
        
        this.log("created ServiceView instance", 2);

        this._costLabel = new MoneyLabel();
        this.add(this._costLabel);
    }

    private static _format(service: NPCService, value: string, noImprovement: boolean = false): string {
        return Utils.format(((!noImprovement && ((service.qualityImprovement || 0) > 0)) ? "item-value-with-improved-quality" : ""), value.toString());
    }

    private static _quality(service: NPCService): string {
        return ((service.qualityImprovement !== undefined) && (service.qualityImprovement > 0)) ? (" (" + Utils.format("item-quality", "+" + Utils.percent(service.qualityImprovement)) + ")") : "";
    }

    private _getServiceName(service: NPCService): string {
        if ("effect" in service) {
            return service.name;
        }
        if ("items" in service) {
            if (service.items.weapon) {
                return service.items.weapon.name;
            }
            if (service.items.potion) {
                return service.items.potion.name;
            }
        }
        if ("discover" in service) {
            return service.name;
        }
        if ("skill" in service) {
            return service.skill.name;
        }
        return "unknown service";
    }

    private _getEffectDescription(effect: Effect, service: NPCService, noImprovement: boolean = false): string {
        let result = "";
        if (isPassiveSkillEffect(effect)) {
            result += "passive skill<br>";
        }
        if (("chance" in effect) && (effect.chance < 1)) {
            result += Utils.percent(effect.chance) + " chance to ";
        }
        switch (effect.type) {
            case EffectType.HEAL:
                result += "heal " + ServiceView._format(service, effect.value.toString(), noImprovement) + " points";
                break;
            case EffectType.INCREASE_LIFE:
                result += "add " + ServiceView._format(service, effect.value.toString(), noImprovement) + " life";
                break;
            case EffectType.INCREASE_DAMAGE:
                result += "add " + ServiceView._format(service, Utils.formatDamageRange(effect.value), noImprovement);
                break;
            case EffectType.INCREASE_BLESSING_DURATION:
                result += "blessings last " + ServiceView._format(service, effect.value.toString(), noImprovement) + " more battle" + ((effect.value > 1) ? "s" : "");
                break;
            case EffectType.WEAPON_QUALITY:
                result += "increase " + (effect.category || "weapon") + " quality to " + ServiceView._format(service, Utils.percent(effect.value), noImprovement);
                break;
            case EffectType.WEAPON_ENCHANT:
                result += "add " + ServiceView._format(service, Utils.formatRange("magic-damage", effect.value), noImprovement) + " to " + (effect.category || "weapon");
                break;
            case EffectType.WEAPON_DISENCHANT:
                result += "remove weapon enchantment";
                noImprovement = true;
                break;
            case EffectType.WEAPON_CATEGORY_DAMAGE:
                result += ServiceView._format(service, Utils.formatDamagePercent(effect.value), noImprovement) + " " + Utils.withWeapon(effect.category);
                break;
            case EffectType.MULTIPLY_DAMAGE:
                result += ServiceView._format(service, Utils.formatDamagePercent(effect.value), noImprovement);
                break;
            case EffectType.REDUCE_DAMAGE:
                result += "cripple (" + Utils.formatDamagePercent(effect.value, false, true) + ")";
                noImprovement = true;
                break;
            case EffectType.STUN:
                result += "stun";
                noImprovement = true;
                break;
            case EffectType.LEECH_LIFE:
                result += "leech " + Utils.formatDamagePercent(effect.value) + " as life";
                noImprovement = true;
                break;
            case EffectType.INCREASE_MAX_BLESSING_DURATION:
                result += "+" + ServiceView._format(service, effect.value.toString(), noImprovement) + " maximum blessing duration";
                break;
            case EffectType.INCREASE_MAX_POTION_PER_TYPE:
                result += "can carry +" + effect.value + " potion" + ((effect.value > 1) ? "s" : "") + " per type";
                noImprovement = true;
                break;
            case EffectType.INCREASE_NATURAL_HEAL:
                result += "+" + Math.round(effect.value * 100) + "% natural healing";
                noImprovement = true;
                break;
            case EffectType.TRACKING:
                result += "detect enemy presence ahead";
                noImprovement = true;
                break;
            default:
                Utils.assertUnreachable(effect);
        }
        if ("rounds" in effect) {
            if (Array.isArray(effect.rounds)) {
                result += " for " + effect.rounds[0] + " - " + effect.rounds[1] + " rounds";
            } else {
                result += " for " + effect.rounds + " round" + ((effect.rounds > 1) ? "s" : "");
            }
        }
        if ("battles" in effect) {
            result += " for " + effect.battles + " battle" + ((effect.battles > 1) ? "s" : "");
        }
        if (service && !noImprovement) {
            result += ServiceView._quality(service);
        }
        return result;
    }

    private _getServiceDescription(service: NPCService): string {
        if ("effect" in service) {
            return this._getEffectDescription(service.effect, service);
        }
        if ("items" in service) {
            if (service.items.weaponInstance) {
                return service.items.weaponInstance.description;
            }
            if (service.items.potion) {
                return this._getEffectDescription(service.items.potion.effect, service);
            }
            return "-";
        }
        if ("discover" in service) {
            return "discover new " + service.discover.areas.map(area => area.name).join("/") + "<br>" +
                    (service.discover.modifiers ?
                            (service.discover.modifiers.treasure ? ("with " + ServiceView._format(service, Utils.percent(service.discover.modifiers.treasure)) + " treasure" + ServiceView._quality(service)) : "") :
                            "with regular properties");
        }
        if (service.skill) {
            let result = (service.skill.weapon ? service.skill.weapon + " " : "") + "skill";
            if (service.skill.damage) {
                result += ", " + Utils.formatDamagePercent(service.skill.damage, false);
            }
            if (service.skill.targetEffect) {
                result += "<br>" + this._getEffectDescription(service.skill.targetEffect, service, true);
            }
            if (service.skill.selfEffect) {
                result += "<br>" + this._getEffectDescription(service.skill.selfEffect, service, true);
            }
            if (service.skill.afterEffect) {
                result += "<br>after: " + this._getEffectDescription(service.skill.afterEffect, service, true);
            }
            return result;
        }
        return "unkown service";
    }

    public render(service: NPCService, npc: NPC, player: Character): void {
        this.log("rendering ServiceView", 3);

        this.title = this._getServiceName(service);

        this.description = Utils.format("skill-description", this._getServiceDescription(service));

        this._costLabel.update(npc.hasFreeServices() ? 0 : service.cost, player.gold);

        this.image = "img/services/" + service.image;

        this.setClass("item", "items" in service);
        this.setClass("skill", "skill" in service);
    }
}