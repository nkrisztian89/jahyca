/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

class PotionView extends Panel {
    
    private _potion: Potions | null;
    private _amount: number;
    
    private _image: ImageView;
    private _label: Label;
    
    constructor(onclick: MouseEventHandler) {
        console.log("created PotionView instance");
        
        super({
            className: "potion",
            onclick: onclick
        });
        
        this._potion = null;
        this._amount = 0;
        
        this._image = new ImageView();
        this.add(this._image);
        
        this._label = new Label({
            className: "amount",
            text: "3"
        });
        this.add(this._label);
    }

    render(potion: Potions) {
        //console.log("rendering PotionView");
        
        this._image.source = "img/potions/" + potion.type.image;
        
        this._label.text = "× " + potion.amount;
        
        if ((potion !== this._potion) || (potion.amount !== this._amount)) {
            this._label.playAnimation("changed", 600);
        }
        
        this._potion = potion;
        this._amount = potion.amount;
    }
}