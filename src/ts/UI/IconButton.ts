/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface IconButtonParams extends PanelParams {
    image: string;
    caption: string;
    action: MouseEventHandler;
}

class IconButton extends Panel {
    
    private _image: ImageView;
    private _label: Label;
    private _costLabel: MoneyLabel;

    constructor(params: IconButtonParams) {

        params.className = "iconButton " + params.className;
        params.onclick = params.action;

        super(params);
        
        this.log("created IconButton instance", 2);

        this._image = new ImageView({
            source: params.image
        });
        this.add(this._image);

        this._label = new Label({
            className: "caption",
            text: params.caption
        });
        this.add(this._label);

        this._costLabel = new MoneyLabel();
        this._costLabel.hidden = true;
        this.add(this._costLabel);
    }

    set caption(value: string) {
        this._label.text = value;
    }

    set light(value: boolean) {
        this.setClass("light", value);
    }

    set imageSource(value: string) {
        this._image.source = value;
    }

    setCost(value: number, compare?: number) {
        if (value !== undefined) {
            this._costLabel.update(value, compare);
            this._costLabel.hidden = false;
        } else {
            this._costLabel.hidden = true;
        }
    }
}