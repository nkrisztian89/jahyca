/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

type IndexedMouseEventHandler = (event: MouseEvent, index?: number) => void;

const PUNCH_ATTACK_ANIMATIONS = ["expand0", "expand1", "expand2"];

class EnemyView extends Panel {

    private _onclick: IndexedMouseEventHandler;
    private _dead: boolean;
    private _dieAnimation: number;
    private _imageContainer: Panel;
    private _image: ImageView;
    private _attackEffectImage: ImageView;
    private _statusImage: ImageView;
    private _status: string;
    private _attackAnimations: string[];
    private _textPanel: Panel;
    private _label: Label;
    private _hpBar: HPBar;
    private _skillsPanel: Panel;
    private _skillViews: SkillView[];


    constructor(onclick: IndexedMouseEventHandler) {

        super({
            className: "enemy",
            onclick: onclick
        });

        this.log("created EnemyView instance", 2);

        this._onclick = onclick;

        this._dead = false;
        this._dieAnimation = -1;

        this._imageContainer = new Panel({
            className: "enemy-container"
        });
        this.add(this._imageContainer);

        this._image = new ImageView({
            className: "enemy"
        });
        this._imageContainer.add(this._image);

        this._attackEffectImage = new ImageView({
            className: "attack"
        });
        this.add(this._attackEffectImage);

        this._statusImage = new ImageView({
            className: "status"
        });
        this.add(this._statusImage);

        this._status = "";

        this._attackAnimations = [];

        this.addLineBreak();

        this._textPanel = new Panel({
            className: "enemy-text"
        });
        this.add(this._textPanel);

        this._label = new Label({
            text: "Enemy"
        });
        this._textPanel.add(this._label);

        this._hpBar = new HPBar({
            width: "20vw",
            height: "2.0vh"
        });
        this._textPanel.add(this._hpBar);

        this._skillsPanel = new Panel({
            className: "skills"
        });
        this.add(this._skillsPanel);

        this._skillViews = [];
    }

    public playAttackAnimation(): void {
        this.playAnimation("shake", 250);
        this._attackEffectImage.playAnimation(Utils.chooseRandom(this._attackAnimations), 250);
    }

    public render(enemy: Enemy, weapon: Weapon | null, skills: SkillType[]): void {
        this.log("rendering EnemyView", 3);

        this._image.source = "img/enemies/" + enemy.typeImage;

        this._image.setGlowClass(enemy.glowColor);

        const properties: string[] = [];
        if (enemy.fast) {
            properties.push(Utils.span("enemyProperty", "fast"));
        }
        if (enemy.toughness) {
            properties.push(Utils.span("enemyProperty", "tough: " + enemy.toughness));
        }
        
        const bossImage = enemy.boss ? `<img class="boss" src="img/boss.png">` : "";

        this._label.text = `<span class="enemy-name${enemy.boss ? " boss": ""}">${bossImage}<span>${enemy.name}</span>${bossImage}</span><br>` +
            "damage: " + Utils.formatDamageRange(enemy.damage, true) +
            ((properties.length > 0) ? "<br>" + properties.join(", ") : "");

        this._hpBar.width = Math.min(Math.max(10, 10 + (enemy.maxHP - 10) / 90 * 15), 25) + "vw";
        this._hpBar.height = Math.min(Math.max(2, 2 + 0.1 * (enemy.toughness || 0)), 2.2) + "vh";
        this._hpBar.render(enemy.hp, enemy.maxHP);

        if (weapon) {
            this._attackEffectImage.source = "img/fight-effects/" + weapon.attackEffect.image;
            this._attackAnimations = weapon.attackEffect.animations;
        } else {
            this._attackEffectImage.source = "img/fight-effects/punch.png";
            this._attackAnimations = PUNCH_ATTACK_ANIMATIONS;
        }

        if (enemy.dead) {
            if (!this._dead) {
                this._dieAnimation = setTimeout(() => {
                    this.element.classList.add("dead");
                    this._dieAnimation = setTimeout(() => {
                        this.hidden = true;
                        this._dieAnimation = -1;
                    }, 700);
                }, 200);
            }
            this._dead = true;
            this.disabled = true;
            this._skillsPanel.hidden = true;
            this._statusImage.hidden = true;
            this._status = "";
        } else {
            if (this._dead) {
                this.element.classList.remove("dead");
            }
            if (this._dieAnimation !== -1) {
                clearTimeout(this._dieAnimation);
                this._dieAnimation = -1;
            }
            this._dead = false;
            this.hidden = false;
            this.disabled = false;

            // skills panel
            Utils.updateViews(this._skillsPanel, this._skillViews, skills, (index) => new SkillView((event) => {
                this._onclick(event, index);
            }), (view, skill) => {
                view.render(skill);
            });
            this._skillsPanel.hidden = false;
            if (enemy.isStunned()) {
                this._statusImage.source = "img/fight-effects/stun.png";
                this._statusImage.opacity = (enemy.stunned > 1) ? 1.0 : 0.7;
                this._statusImage.hidden = false;
                if (this._status !== "stun") {
                    this._statusImage.playAnimation("new", 350);
                }
                this._status = "stun";
            } else {
                this._statusImage.hidden = true;
                this._status = "";
            }
        }
    }
}