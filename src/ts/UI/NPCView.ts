/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

class NPCView extends OptionView {
    
    private _questImage: ImageView;
    private _hasUpdateImage: ImageView;
    
    constructor(params: OptionViewParams) {

        params.className = "npc " + (params.className || "");
        
        super(params);
        
        this.log("created NPCView instance", 2);
        
        this._questImage = new ImageView({
            className: "npc-quest",
            source: "img/quest.png"
        });
        this._questImage.hidden = true;
        this.add(this._questImage);
        
        this._hasUpdateImage = new ImageView({
            className: "npc-quest npc-update",
            source: "img/exclamation-mark.png"
        });
        this._hasUpdateImage.hidden = true;
        this.add(this._hasUpdateImage);
    }

    render(npc: NPC) {
        this.log("rendering NPCView", 3);
        
        this.title = npc.name;
        this.description = "level " + npc.level;
        
        this.image = "img/npc/" + npc.image;
        
        this._questImage.hidden = !npc.hasOpenQuests();
        this._hasUpdateImage.hidden = !npc.hasQuestUpdates();
    }
}