/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

class BlessingView extends Panel {

    private _blessing: BlessingEffect | null;
    private _duration: number;
    private _image: ImageView;
    private _label: Label;

    constructor() {
        console.log("created BlessingView instance");

        super({
            className: "blessing"
        });

        this._blessing = null;
        this._duration = 0;

        this._image = new ImageView();
        this.add(this._image);

        this._label = new Label({
            className: "battle",
            text: "3"
        });
        this.add(this._label);
    }

    render(blessing: Effect & BlessingEffect) {
        //console.log("rendering BlessingView");

        this._image.source = "img/effects/" + blessing.type + ".png";

        this._label.text = blessing.battles.toString();

        if ((blessing !== this._blessing) || (blessing.battles !== this._duration)) {
            this._label.playAnimation("changed", 600);
        }

        this._blessing = blessing;
        this._duration = blessing.battles;
    }
}