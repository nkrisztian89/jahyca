/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
class WeaponView extends Panel {
    
    private _imageContainer: Panel;
    private _image: ImageView;
    private _imageOverlay: Panel;
    private _label: Label;
    private _weapon: Weapon | null;
    
    constructor(params?: PanelParams) {
        
        params = params || {};
        params.className = params.className ? "weapon " + params.className : "weapon";

        super(params);
        
        this.log("created WeaponView instance", 2);

        this._imageContainer = new Panel({
            className: "weapon-image"
        });
        this.add(this._imageContainer);

        this._image = new ImageView({
            className: "weapon"
        });
        this._imageContainer.add(this._image);

        this._imageOverlay = new Panel({
            className: "overlay"
        });
        this._imageOverlay.hidden = true;
        this._imageContainer.add(this._imageOverlay);

        this._label = new Label({
            text: "",
            className: "weapon"
        });
        this.add(this._label);
        
        this._weapon = null;
    }

    render(weapon: Weapon | null, highlightChange: boolean = false) {
        this.log("rendering WeaponView", 3);

        if (weapon) {
            if (weapon.hasMagicDamage) {
                this._imageOverlay.setClass("magic-item", true);
                this._imageOverlay.hidden = false;
            } else {
                this._imageOverlay.hidden = true;
            }
            this._image.source = "img/weapons/" + weapon.image;
            this._imageContainer.hidden = false;
            this._label.text = "<span " + (weapon.hasMagicDamage ? 'class="magic-item"' : "") + ">" + weapon.typeName + "</span><br>" + weapon.longDescription + "";
        } else {
            this._imageContainer.hidden = true;
            this._label.text = "no weapon";
        }
        
        if ((this._weapon !== weapon) && highlightChange) {
            this.playAnimation("changed", 600);
        }
        
        this._weapon = weapon;
    }
}