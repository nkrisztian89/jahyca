/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

class AreaView extends OptionView {
    
    private _treasureImage: ImageView;
    private _questImage: ImageView;
    private _bossImage: ImageView;
    
    constructor(params: OptionViewParams) {
        
        params.className = "area light " + (params.className || "");
        
        super(params);
        
        this.log("created AreaView instance", 2);
        
        this._treasureImage = new ImageView({
            className: "area-treasure",
            source: "img/world/treasure.png"
        });
        this._treasureImage.hidden = true;
        this.add(this._treasureImage);
        
        this._questImage = new ImageView({
            className: "area-quest",
            source: "img/quest.png"
        });
        this._questImage.hidden = true;
        this.add(this._questImage);
        
        this._bossImage = new ImageView({
            className: "area-boss",
            source: "img/boss.png"
        });
        this._bossImage.hidden = true;
        this.add(this._bossImage);
    }

    public render(path: Area) {
        this.log("rendering AreaView", 3);
        
        this.title = path.typeName;
        this.description = path.description;
        this.image = "img/world/" + path.typeImage;
        
        this._treasureImage.hidden = !path.hasExtraTreasure();
        this._questImage.hidden = !path.hasQuest();
        this._bossImage.hidden = !path.hasBoss();
        
        const enemies = path.hasEnemies();
        const visited = path.visited;
        this.setClass("traversed", !enemies);
        this.setClass("visited", enemies && visited);
    }
}