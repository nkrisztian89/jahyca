/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface LabelParams extends ViewParams {
    text: string;
    inline?: boolean;
}

class Label extends GeneralView {
    constructor(params: LabelParams) {
        
        super("Label");
        
        this.log("created Label instance", 2);
        
        this.setParams(params);
        this.setClass("label", true);
        
        this.element.innerHTML = params.text;
        
        if (params.inline) {
            this.element.style.display = "inline-block";
        }
    }
    
    public set text(value: string) {
        this.element.innerHTML = value;
    }
    
    public set color(value: string) {
        this.element.style.color = value;
    }

}