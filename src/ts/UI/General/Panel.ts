/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface PanelParams extends ViewParams {
    onclick?: MouseEventHandler;
    visible?: boolean;
}

class Panel extends GeneralView {
    
    private _disabled: boolean;
    
    constructor(params?: PanelParams) {
        
        super("Panel");
        
        this.log("created Panel instance", 2);
        params = params || {};
        
        this._disabled = false;
        
        this.setParams(params);
        this.setClass("panel", true);
        
        if (params.onclick) {
            const onclick = params.onclick;
            this.element.onclick = (event)=> {
                if (!this._disabled) {
                    onclick(event);
                }
            }
        }
        if ((params.visible === false) || !!params.hidden) {
            this.hidden = true;
        }
    }
    
    public set className(value: string) {
        super.className = value;
        this.setClass("panel", true);
    }
    
    public set disabled(value: boolean) {
        this._disabled = value;
        this.setClass("disabled", value);
    }
}