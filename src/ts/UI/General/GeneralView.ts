/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
const GLOW_COLORS = ["white", "yellow", "orange", "red", "lime", "green", "skyblue", "darkorchid"] as const;

type GlowColor = 'none'|typeof GLOW_COLORS[number];

const GLOW_CLASSES = GLOW_COLORS.map(color => `glow-${color}`);
 
type MouseEventHandler = (event: MouseEvent) => void;

interface ViewParams {
    className?: string;
    hidden?: boolean;
    opacity?: number;
    width?: string;
    height?: string;
    minWidth?: string;
    minHeight?: string;
    maxWidth?: string;
    maxHeight?: string;
    float?: string;
    font?: string;
    fontFamily?: string;
    fontSize?: string;
    fontWeight?: string;
    color?: string;
    background?: string;
    border?: string;
    outline?: string;
    margin?: string;
    filter?: string;
    animation?: string;
}

class GeneralView extends GeneralObject {

    protected element: HTMLElement;
    private _originalDisplayMode: string;
    private _animationID: number;
    private _animationClassName: string;

    constructor(id: string) {
        super(id);

        this.element = document.createElement("div");
        this._originalDisplayMode = "block";
        this._animationID = 0;
        this._animationClassName = "";
    }
    
    public getObjectType(): string {
        return "View";
    }
    
    public addToPage(): void {
        document.body.appendChild(this.element);
    }

    public set className(value: string) {
        this.element.className = value;
    }

    public get hidden(): boolean {
        return this.element.style.display === "none";
    }
    public set hidden(value: boolean) {
        if (!!value !== this.hidden) {
            if (value) {
                this._originalDisplayMode = this.element.style.display;
                this.element.style.display = "none";
            } else {
                this.element.style.display = this._originalDisplayMode;
            }
        }
    }

    public set opacity(value: number) {
        this.element.style.opacity = value.toString();
    }

    public set width(value: string) {
        this.element.style.width = value;
    }
    public set height(value: string) {
        this.element.style.height = value;
    }
    public set minWidth(value: string) {
        this.element.style.minWidth = value;
    }
    public set minHeight(value: string) {
        this.element.style.minHeight = value;
    }
    public set maxWidth(value: string) {
        this.element.style.maxWidth = value;
    }
    public set maxHeight(value: string) {
        this.element.style.maxHeight = value;
    }
    public set float(value: string) {
        this.element.style.float = value;
    }

    public set font(value: string) {
        this.element.style.font = value;
    }
    public set fontFamily(value: string) {
        this.element.style.fontFamily = value;
    }
    public set fontSize(value: string) {
        this.element.style.fontSize = value;
    }
    public set fontWeight(value: string) {
        this.element.style.fontWeight = value;
    }
    public set color(value: string) {
        this.element.style.color = value;
    }
    public get background(): string {
        return this.element.style.background;
    }
    public set background(value: string) {
        this.element.style.background = value;
    }
    public set border(value: string) {
        this.element.style.border = value;
    }
    public set outline(value: string) {
        this.element.style.outline = value;
    }
    public set margin(value: string) {
        this.element.style.margin = value;
    }
    
    public set filter(value: string) {
        this.element.style.filter = value;
    }
    public set animation(value: string) {
        this.element.style.animation = value;
    }
    
    public get html(): string {
        return this.element.outerHTML;
    }

    public setParams(value: ViewParams): void {
        if (value.className !== undefined) {
            this.className = value.className;
        }
        if (value.hidden !== undefined) {
            this.hidden = value.hidden;
        }
        if (value.opacity !== undefined) {
            this.opacity = value.opacity;
        }
        if (value.width !== undefined) {
            this.width = value.width;
        }
        if (value.height !== undefined) {
            this.height = value.height;
        }
        if (value.minWidth !== undefined) {
            this.minWidth = value.minWidth;
        }
        if (value.minHeight !== undefined) {
            this.minHeight = value.minHeight;
        }
        if (value.maxWidth !== undefined) {
            this.maxWidth = value.maxWidth;
        }
        if (value.maxHeight !== undefined) {
            this.maxHeight = value.maxHeight;
        }
        if (value.float !== undefined) {
            this.float = value.float;
        }
        if (value.font !== undefined) {
            this.font = value.font;
        }
        if (value.fontFamily !== undefined) {
            this.fontFamily = value.fontFamily;
        }
        if (value.fontSize !== undefined) {
            this.fontSize = value.fontSize;
        }
        if (value.fontWeight !== undefined) {
            this.fontWeight = value.fontWeight;
        }
        if (value.color !== undefined) {
            this.color = value.color;
        }
        if (value.background !== undefined) {
            this.background = value.background;
        }
        if (value.border !== undefined) {
            this.border = value.border;
        }
        if (value.outline !== undefined) {
            this.outline = value.outline;
        }
        if (value.margin !== undefined) {
            this.margin = value.margin;
        }
        if (value.filter !== undefined) {
            this.filter = value.filter;
        }
        if (value.animation !== undefined) {
            this.animation = value.animation;
        }
    }

    public playAnimation(className: string, duration: number, callback?: () => void): void {
        if (this._animationClassName !== "") {
            if (this._animationID >= 0) {
                clearTimeout(this._animationID);
            }
            this.element.classList.remove(this._animationClassName);
            this._animationClassName = "";
            void this.element.offsetWidth; // trigger reflow so that removing the class if the animation was in progress takes effect before readding it
        }
        this.element.classList.add(className);
        this._animationID = setTimeout(() => {
            this.element.classList.remove(className);
            this._animationID = -1;
            if (callback) {
                callback();
            }
        }, duration);
        this._animationClassName = className;
    }

    public add(child: GeneralView): void {
        if (child.element.parentNode !== this.element) {
            this.element.appendChild(child.element);
        }
    }
    
    public addElement(child: HTMLElement): void {
        this.element.appendChild(child);
    }

    public addLineBreak(): void {
        this.element.appendChild(document.createElement("br"));
    }

    public setClass(className: string, value: boolean): void {
        if (value) {
            this.element.classList.add(className);
        } else {
            this.element.classList.remove(className);
        }
    }
    
    public toggleClass(className: string): void {
        this.element.classList.toggle(className);
    }
    
    public setClassFromList(className: string, list: string[]): void {
        list.forEach(element => this.setClass(element, className === element));
    }
    
    public setGlowClass(color: GlowColor): void {
        this.setClassFromList(`glow-${color}`, GLOW_CLASSES);
    }
    
    public blur(): void {
        this.element.blur();
    }
    
    public clear(): void {
        this.element.innerHTML = "";
    }
}
