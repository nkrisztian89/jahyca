/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
 interface CaptionedButtonParams {
     caption: string;
 }
 
 interface CustomContentButtonParams {
     content: string;
 }
 
interface BaseButtonParams extends ViewParams {
    action: MouseEventHandler;
}

type ButtonParams = BaseButtonParams & (CaptionedButtonParams | CustomContentButtonParams);

class Button extends GeneralView {
    
    protected element: HTMLButtonElement;
    
    constructor(params: ButtonParams) {
        
        super("Button");
        
        this.log("created Button instance", 2);
        
        this.element = document.createElement("button");
        this.setParams(params);
        if (("caption" in params) && params.caption) {
            this.element.textContent = params.caption;
        } else if (("content" in params) && params.content) {
            this.element.innerHTML = params.content;
        }
        this.element.onclick = params.action;
        
    }
    
    public get disabled(): boolean {
        return this.element.disabled;
    }
    public set disabled(value: boolean) {
        this.element.disabled = value;
    }
    
    public set caption(value: string) {
        this.element.textContent = value;
    }
    
    public set content(value: string) {
        this.element.innerHTML = value;
    }
}