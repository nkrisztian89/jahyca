/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface ImageViewParams extends ViewParams {
    source?: string;
    onclick?: MouseEventHandler;
}

class ImageView extends GeneralView {
    
    protected element: HTMLImageElement;
    
    constructor(params?: ImageViewParams) {
        
        super("ImageView");
        
        this.log("created ImageView instance", 2);
        params = params || {};
        
        this.element = new Image();
        this.setParams(params);
        if (params.source) {
            this.element.src = params.source;
        }
        if (params.onclick) {
            this.element.onclick = params.onclick;
        }
    }
    
    public set source(value: string) {
        this.element.src = value;
    }
}