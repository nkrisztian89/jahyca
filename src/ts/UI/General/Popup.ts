/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

type PopupInputCallback = (value: string) => void | boolean;

interface PopupParams {
    text: string;
    className?: string;
    optionsClassName?: string;
    border?: string;
    outline?: string;
    background?: string;
    input?: PopupInput;
    options?: PopupOption[];
}

interface PopupInput {
    value?: string;
    callback?: PopupInputCallback;
}

interface CaptionedPopupOption {
    caption: string;
}

interface CustomContentPopupOption {
    content: string;
}

interface BasePopupOption {
    className?: string;
    action: PopupInputCallback;
    newLineAfter?: boolean;
}

type PopupOption = BasePopupOption & (CaptionedPopupOption | CustomContentPopupOption);

class Popup {

    private _queue: PopupParams[];
    private _wrapper: Panel;
    private _panel: Panel;
    private _label: Label;
    private _button: Button;
    private _inputContainer: Panel;
    private _input: HTMLInputElement;
    private _inputCallback: PopupInputCallback | null;
    private _optionActions: PopupInputCallback[];
    private _optionButtons: Button[];
    private _optionsPanel: Panel;

    constructor(params: PopupParams) {
        console.log("created Popup instance");
        params = params || {};

        this._queue = [];

        this._wrapper = new Panel({
            className: "popup-wrapper"
        });

        params.className = "popup" + (params.className ? (" " + params.className) : "");

        this._panel = new Panel(params);
        this._wrapper.add(this._panel);

        this._label = new Label({
            text: params.text
        });
        this._panel.add(this._label);

        this._inputContainer = new Panel();
        this._panel.add(this._inputContainer);

        this._input = document.createElement("input");
        this._input.type = "text";
        this._inputContainer.addElement(this._input);
        this._inputCallback = null;

        this._button = new Button({
            caption: "OK",
            className: "default",
            action: this.close.bind(this)
        });
        this._panel.add(this._button);

        this._optionActions = [];
        this._optionButtons = [];

        this._optionsPanel = new Panel({
            className: "options" + (params.optionsClassName ? (" " + params.optionsClassName) : "")
        });
        this._panel.add(this._optionsPanel);

        this.input = params.input || null;
        this.options = params.options || null;

        this.hidden = true;

        this._wrapper.addToPage();
    }

    public close(): void {
        if (this._inputCallback) {
            this._inputCallback(this._input.value);
        }
        if (this._queue.length <= 0) {
            this.hidden = true;
        } else {
            this.showMessage(this._queue.shift()!);
        }
    }

    public get hidden(): boolean {
        return this._wrapper.hidden;
    }
    public set hidden(value: boolean) {
        this._wrapper.hidden = value;
    }

    public set opacity(value: number) {
        this._panel.opacity = value;
    }

    public set border(value: string | undefined) {
        this._panel.border = value || "none";
    }

    public set outline(value: string | undefined) {
        this._panel.outline = value || "none";
    }

    public set background(value: string | undefined) {
        this._panel.background = value || "";
    }

    public set className(value: string) {
        this._panel.className = value;
        this._panel.setClass("popup", true);
    }

    public set optionsClassName(value: string) {
        this._optionsPanel.className = value;
        this._optionsPanel.setClass("options", true);
    }

    public set text(value: string) {
        this._label.text = value;
    }

    public set input(value: PopupInput | null) {
        if (value) {
            this._input.value = value.value || "";
            this._inputCallback = value.callback || null;
            this._inputContainer.hidden = false;
        } else {
            this._inputContainer.hidden = true;
            this._inputCallback = null;
        }
    }

    public set options(value: PopupOption[] | null) {
        this._optionActions.length = 0;
        this._optionsPanel.clear();
        let i = 0;
        if (value) {
            for (i = 0; i < value.length; i++) {
                const option = value[i];
                this._optionActions.push(option.action);
                if (this._optionButtons.length <= i) {
                    this._optionButtons.push(new Button({
                        caption: ("caption" in option) ? option.caption : undefined,
                        content: ("content" in option) ? option.content : undefined,
                        className: option.className,
                        action: function (this: Popup, index: number) {
                            const result = this._optionActions[index](this._input.value);
                            if (result !== false) {
                                this.close();
                            }
                        }.bind(this, i)
                    } as ButtonParams));
                } else {
                    if ("caption" in option) {
                        this._optionButtons[i].caption = option.caption;
                    } else {
                        this._optionButtons[i].content = option.content;
                    }
                    this._optionButtons[i].className = option.className || "";
                    this._optionButtons[i].blur();
                }
                this._optionsPanel.add(this._optionButtons[i]);
                this._optionButtons[i].hidden = false;
                if (value[i].newLineAfter) {
                    this._optionsPanel.addLineBreak();
                }
            }
            this._optionsPanel.hidden = false;
            this._button.hidden = true;
        } else {
            this._optionsPanel.hidden = true;
            this._button.hidden = false;
        }
    }

    public showMessage(params: PopupParams): void {
        this.className = params.className || "";
        this.optionsClassName = params.optionsClassName || "";
        this.text = params.text;
        this.input = params.input || null;
        this.border = params.border;
        this.outline = params.outline;
        this.background = params.background;
        this.options = params.options || null;
        this.hidden = false;
    }

    private _isTransient(): boolean {
        return this._button.hidden && this._optionActions.length === 0;
    }

    public queueMessage(params: PopupParams): void {
        if (!this.hidden && this._isTransient()) {
            this.close();
        }
        if (this.hidden) {
            this.showMessage(params);
        } else {
            this._queue.push(params);
        }
    }
}