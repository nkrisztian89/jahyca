/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

class HPBar extends GeneralView {
    
    private _ratio: number;
    private _set: boolean;
    private _hitBar: HTMLDivElement;
    private _healBar: HTMLDivElement;
    private _bar: HTMLDivElement;
    private _label: Label;
    
    constructor(params?: ViewParams) {
        
        params = params || {};
        
        super("HPBar");
        
        this.log("created HPBar instance", 2);

        this._ratio = 0;
        this._set = false;
        
        params.className = "hp-bar-background " + (params.className || "");

        this.setParams(params);
        
        this._hitBar = document.createElement("div");
        this._hitBar.className = "hp-bar hit";
        this.addElement(this._hitBar);
        
        this._healBar = document.createElement("div");
        this._healBar.className = "hp-bar heal";
        this.addElement(this._healBar);

        this._bar = document.createElement("div");
        this._bar.className = "hp-bar";
        this.addElement(this._bar);

        this._label = new Label({
            text: "0/10",
            fontSize: params.fontSize
        });
        this.add(this._label);
    }

    render(hp: number, maxHP: number) {
        const ratio = hp / maxHP;
        if (this._set) {
            if (ratio > this._ratio) {
                this._bar.classList.add("animated");
            } else if (ratio < this._ratio) {
                this._bar.classList.remove("animated");
            }
        }
        const width = (ratio * 100).toString() + "%";
        this._bar.style.width = width;
        this._hitBar.style.width = width;
        this._healBar.style.width = width;
        // if render is called multiple times, make sure the CSS style changes take effect in between, allowing for both the heal and hit animation to
        // happen at the same time - calculating offsetWidth triggers this
        this._bar.offsetWidth;
        this._healBar.offsetWidth;
        this._label.text = hp + " / " + maxHP;
        this._set = true;
        this._ratio = ratio;
    }
}