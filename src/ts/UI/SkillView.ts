/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

class SkillView extends Panel {
    
    private _image: ImageView;
    
    constructor(onclick: MouseEventHandler) {
        console.log("created SkillView instance");

        super({
            className: "skill-button",
            onclick: onclick
        });
        
        this._image = new ImageView();
        this.add(this._image);
    }

    render(skill: SkillType) {
        //console.log("rendering SkillView");
        
        this._image.source = "img/skills/" + skill.image;
    }
}