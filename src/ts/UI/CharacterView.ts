/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
class CharacterView extends Panel {
    
    private _character: Character | null;
    private _parentView: ShopView | RoomView | null;
    private _statsPanel: Panel;
    private _titleLabel: Label;
    private _hpBar: HPBar;
    private _damageLabel: Label;
    private _potionsPanel: Panel;
    private _potionsTitleLabel: Label;
    private _potionsListPanel: Panel;
    private _potionViews: PotionView[];
    private _blessingsPanel: Panel;
    private _blessingsTitleLabel: Label;
    private _blessingViews: BlessingView[];
    private _inventoryPanel: Panel;
    private _weaponView: WeaponView;
    private _moneyLabel: MoneyLabel;
    private _minimizeButton: ImageView;
    private _first: boolean;
    
    constructor() {

        super({
            className: "player minimal"
        });
        
        this.log("created CharacterView instance", 2);
        
        this._character = null;
        this._parentView = null;

        // stats

        this._statsPanel = new Panel({
            className: "stats"
        });
        this.add(this._statsPanel);

        this._titleLabel = new Label({
            text: "Player"
        });
        this._statsPanel.add(this._titleLabel);

        this._hpBar = new HPBar({
            width: "30vw"
        });
        this._statsPanel.add(this._hpBar);

        this._damageLabel = new Label({
            text: "Damage:"
        });
        this._statsPanel.add(this._damageLabel);

        this.addLineBreak();

        // potions

        this._potionsPanel = new Panel({
            className: "potions"
        });
        this.add(this._potionsPanel);

        this._potionsTitleLabel = new Label({
            text: "Potions:",
            inline: true
        });
        this._potionsPanel.add(this._potionsTitleLabel);

        this._potionsPanel.addLineBreak();
        
        this._potionsListPanel = new Panel({
            className: "potions-list"
        });
        this._potionsPanel.add(this._potionsListPanel);

        this._potionViews = [];

        // blessings

        this._blessingsPanel = new Panel({
            className: "blessings"
        });
        this.add(this._blessingsPanel);

        this._blessingsTitleLabel = new Label({
            text: "Blessings:",
            inline: true
        });
        this._blessingsPanel.add(this._blessingsTitleLabel);

        this._blessingsPanel.addLineBreak();

        this._blessingViews = [];

        // inventory

        this._inventoryPanel = new Panel({
            className: "inventory"
        });
        this.add(this._inventoryPanel);
        
        this._weaponView = new WeaponView({
            onclick: () => {
                if (this._character) {
                    this._character.swapWeapons();
                }
            }
        });
        this._inventoryPanel.add(this._weaponView);

        this._moneyLabel = new MoneyLabel();
        this._inventoryPanel.add(this._moneyLabel);

        this._minimizeButton = new ImageView({
            className: "menu-button cog",
            source: "img/cog.png",
            onclick: () => {
                this.toggleClass("minimal");
            }
        });
        this._minimizeButton.hidden = true; // hide until a more useful function is implemented
        this.add(this._minimizeButton);
        
        this._first = true;

        this._updateHP = this._updateHP.bind(this);
    }

    _updateHP() {
        if (this._character) {
            this._hpBar.render(this._character.hp, this._character.maxHP);
        }
    }

    render(character: Character, parentView: ShopView | RoomView) {
        this.log("rendering CharacterView", 3);

        if (this._character !== character) {
            if (this._character) {
                this._character.removeEventHandler("hpChanged", this._updateHP);
            }
            this._character = character;
            this._character.addEventHandler("hpChanged", this._updateHP);
        }

        this._parentView = parentView;
        
        this._titleLabel.text = character.name;

        // stats

        this._damageLabel.text =
                "Damage: " + Utils.formatDamageRange({
                    physical: character.physicalDamage,
                    magic: character.magicDamage
                }, true);

        this._updateHP();

        // potions

        if (character.hasPotions()) {
            Utils.updateViews(this._potionsListPanel, this._potionViews, character.potions, (index) => {
                return new PotionView(() => {
                    character.usePotion(index);
                    this._parentView?.render();
                });
            }, (view, potion, index) => {
                view.disabled = !character.canUsePotion(index);
                view.render(potion);
            });
            this._potionsPanel.hidden = false;
        } else {
            this._potionsPanel.hidden = true;
        }

        // blessings

        if (character.blessings.length > 0) {
            Utils.updateViews(this._blessingsPanel, this._blessingViews, character.blessings, () => {
                return new BlessingView();
            }, (view, blessing) => {
                view.render(blessing as (Effect & BlessingEffect));
            });
            this._blessingsPanel.hidden = false;
        } else {
            this._blessingsPanel.hidden = true;
        }

        // inventory
        
        this._weaponView.render(character.weapon, !this._first);
        this._weaponView.disabled = !character.backupWeapon || character.fighting;
        this._moneyLabel.update(character.gold, undefined, true);
        
        this._first = false;
    }
}