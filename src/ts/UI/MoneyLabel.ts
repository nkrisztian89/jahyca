/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

class MoneyLabel extends Label {
    
    private _set: boolean;
    private _value: number;
    
    constructor(params?: LabelParams) {
        
        params = params || {
            text: "0"
        };
        
        params.className = "money " + (params.className || "");
        
        super(params);
        
        this.log("created MoneyLabel instance", 2);
        
        this._set = false;
        this._value = 0;
    }

    update(value: number, compare?: number, animate?: boolean) {
        this.text = value.toString();
        if (compare !== undefined) {
            this.setClass("insufficient", value > compare);
        }
        if (animate) {
            if (this._set && (this._value !== value)) {
                this.playAnimation("changed", 600);
            }
        }
        this._set = true;
        this._value = value;
    }
}