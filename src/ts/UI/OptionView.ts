/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface OptionViewParams extends ViewParams {
    action: MouseEventHandler;
    textless?: boolean;
}

class OptionView extends Panel {

    private _image: ImageView;
    private _nameLabel?: Label;
    private _descLabel?: Label;

    constructor(params: OptionViewParams) {

        const panelParams: PanelParams = params;
        panelParams.onclick = params.action;
        panelParams.className = "option " + (params.className ?? "");

        super(panelParams);
        
        this.log("created OptionView instance", 2);

        this._image = new ImageView();
        this.add(this._image);

        this.addLineBreak();

        if (!params.textless) {
            this._nameLabel = new Label({
                className: "title",
                text: "Option name"
            });
            this.add(this._nameLabel);
            this._descLabel = new Label({
                className: "description",
                text: "Option description"
            });
            this.add(this._descLabel);
        }
    }

    protected set title(value: string) {
        if (!this._nameLabel) {
            throw new Error("Cannot set title on textless OptionView!");
        }
        this._nameLabel.text = value;
    }

    protected set description(value: string) {
        if (!this._descLabel) {
            throw new Error("Cannot set description on textless OptionView!");
        }
        this._descLabel.text = value;
    }

    protected set image(url: string) {
        this._image.source = url;
    }
}