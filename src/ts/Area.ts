/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface BossDefinition {
    typeName: EnemyTypeName;
    modifiers?: EnemyModifiers;
    loot?: LootChances;
}

interface AreaModifiers {
    level?: number;
    treasure?: number;
    enemyChance?: number;
    enemyTypeProbabilities?: EnemyTypeProbabilities;
    allEnemies?: EnemyModifiers;
    boss?: BossDefinition;
}

interface AreaState extends GameObjectState {
    type: string;
    visited: boolean;
    paths: AreaState[];
    startingRoom: RoomState;
    enemyCount: number;
    originalEnemyCount: number;
    roomCount: number;
    totalTreasure: number;
    _modifiers?: AreaModifiers;
    reward?: LootState;
    level: number;
    questId?: string;
}

class Area extends GameObject {
    private _type: AreaType | null;
    private _visited: boolean;
    private _pathCountProbabilities: number[] | null;
    private _paths: Area[];
    private _startingRoom: Room | null;
    private _enemyCount: number;
    private _originalEnemyCount: number;
    private _roomCount: number;
    private _totalTreasure: number;
    private _modifiers?: AreaModifiers;
    private _reward: Loot | null;
    private _level: number;
    private _questId?: string;

    constructor(type?: AreaType | null, pathCountProbabilities?: number[] | null, modifiers?: AreaModifiers, quest?: Quest) {
        super("Area");

        this.log("created Area instance", 2);

        this._type = type || null;

        this._visited = false;

        this._pathCountProbabilities = pathCountProbabilities || null;

        this._paths = [];

        this._startingRoom = null;
        this._enemyCount = 0;
        this._originalEnemyCount = 0;
        this._roomCount = 0;
        this._totalTreasure = 0;

        this._modifiers = modifiers;

        this._reward = null;

        this._level = (modifiers?.level !== undefined) ? modifiers.level : (this._type ? game.getAreaLevel(this._type.name) : 0);

        this._questId = quest?.id;

        if (type) {
            while (!this._enemyCount) {
                this.generateRooms(this._level);
            }
        }
    }

    public getObjectType(): string {
        return "Area";
    }

    public get questId(): string {
        return this._questId ?? "";
    }

    public save(): AreaState {
        if (!this._type) {
            throw new Error("Cannot save area without a type!");
        }
        if (!this._startingRoom) {
            throw new Error("Cannot save area without a starting room!");
        }
        return {
            _id: super.save()._id,
            type: this._type.name,
            visited: this._visited,
            paths: this._paths.map((path) => path.save()),
            startingRoom: this._startingRoom.save(),
            enemyCount: this._enemyCount,
            originalEnemyCount: this._originalEnemyCount,
            roomCount: this._roomCount,
            totalTreasure: this._totalTreasure,
            _modifiers: this._modifiers,
            reward: this._reward ? Utils.saveLoot(this._reward) : undefined,
            level: this._level,
            questId: this._questId
        };
    }

    public load(state: AreaState): Area {
        super.load(state);

        this._type = Utils.findByName(state.type, AreaTypes);
        this._visited = state.visited;
        this._paths = [];
        state.paths.forEach((path) => {
            this._paths.push(new Area(null, this._pathCountProbabilities).load(path));
        });
        this._startingRoom = new Room().load(state.startingRoom, this);
        this._enemyCount = state.enemyCount;
        this._originalEnemyCount = state.originalEnemyCount;
        this._roomCount = state.roomCount;
        this._totalTreasure = state.totalTreasure;
        this._modifiers = state._modifiers;

        this._reward = state.reward ? Utils.loadLoot(state.reward) : null;
        this._level = state.level;
        this._questId = state.questId;

        return this;
    }

    public handleRoomAdded(gold: number): void {
        this._roomCount++;
        this._totalTreasure += gold;
    }

    public get typeName(): string {
        return this._type?.name ?? "unknown area";
    }

    public get typeImage(): string {
        return this._type?.image ?? "";
    }

    public get enemyCount(): number {
        return this._enemyCount;
    }

    public get pathCount(): number {
        return this._paths.length;
    }

    public get level(): number {
        return this._level;
    }
    
    public hasBoss(): boolean {
        return !!this._modifiers?.boss;
    }

    public get reward(): Loot | null {
        return this._reward;
    }

    public clearReward(): void {
        this._reward = null;
    }

    public get description(): string {
        let result = "level " + (this._level + 1) + ", ";
        if (this._roomCount === 0) {
            result += "empty";
        } else if (this._roomCount <= 5) {
            result += "tiny";
        } else if (this._roomCount <= 10) {
            result += "small";
        } else if (this._roomCount <= 15) {
            result += "medium";
        } else if (this._roomCount <= 20) {
            result += "large";
        } else {
            result += "huge";
        }
        return result;
    }

    public get visited(): boolean {
        return this._visited;
    }

    public hasEnemies(): boolean {
        return this._enemyCount > 0;
    }

    public hasExtraTreasure(): boolean {
        return !!this._modifiers && ((this._modifiers.treasure ?? 1) > 1);
    }

    public hasQuest(): boolean {
        return !!this._questId;
    }

    public chooseRoomType(level?: number): RoomType {
        if (!this._type) {
            throw new Error("Cannot choose room type for area without a type!");
        }
        const probabilities = this._type.roomProbabilities;
        return this._type.roomTypes[Utils.chooseFromProbabilitiesArray(probabilities[Math.min(level || 0, probabilities.length - 1)])];
    }

    public clearModifiers() {
        this._modifiers = undefined;
    }

    private _getRoomParams(parent?: Room, boss: boolean = false): RoomParams {
        if (!this._type) {
            throw new Error("Cannot get room params for area without a type!");
        }
        return Object.assign({},
            {
                pathDepth: boss ? 0 : this._type.pathDepth,
                pathProbabilities: this._type.pathProbabilities,
                area: this,
                parent: parent ?? null,
                boss
            },
            this.chooseRoomType(this._level));
    }

    public generateRooms(level: number): void {
        if (!this._type) {
            throw new Error("Cannot generate rooms for area without a type!");
        }
        this._level = (this._modifiers?.level !== undefined) ? this._modifiers.level : level;
        const roomParams = this._getRoomParams();

        this._roomCount = 0;
        this._startingRoom = new Room(roomParams, this._level, this._modifiers);
        
        if (this._modifiers?.boss) {
            const endRooms: Room[] = [];
            this._startingRoom.traverse((room) => {
                if (room.pathCount <= 0) {
                    endRooms.push(room);
                }
            });
            const index = Utils.randomRange(0, endRooms.length - 1);
            endRooms[index].addPath(new Room(this._getRoomParams(endRooms[index], true), this._level, this._modifiers))
        }

        this._enemyCount = this._startingRoom.enemyCount;
        this._originalEnemyCount = this._enemyCount;

        this._reward = Utils.generateLoot(this._type.reward, Utils.getLevelFactor(this._level)) || {};

        this._visited = false;
    }

    public visit(): Room {
        if (!this._startingRoom) {
            throw new Error("Cannot visit area without a starting room!");
        }
        this._visited = true;
        return this._startingRoom;
    }

    public handleEnemyKilled(): void {
        if (!this._type) {
            throw new Error("Cannot handle enemy killed for area without a type!");
        }
        this._enemyCount--;
        this.log("Enemies left in area: " + this._enemyCount + " / " + this._originalEnemyCount, 1);
        if (this._enemyCount <= 0) {
            if (this._level === game.getAreaLevel(this._type.name)) {
                game.increaseAreaLevel(this._type.name);
            }
            game.player.onAreaCompleted(this);
            if (this._pathCountProbabilities) {
                const n = Utils.chooseFromProbabilitiesArray(this._pathCountProbabilities) + 1;
                for (let i = 0; i < n; i++) {
                    const areaType = AreaTypes[Utils.chooseFromProbabilitiesObject(this._type.nextAreaTypeProbabilities)];
                    this._paths.push(new Area(areaType, this._pathCountProbabilities));
                    game.player.onAreaDiscovered(this._paths[this._paths.length - 1]);
                }
            }
            game.addQuestById("Beginner");
            if ((this._level >= 1) && (this.typeName !== "Forest")) {
                game.addQuestById("Insects");
            }
            if (this._level >= 3) {
                game.addQuestById("Dragon");
            }
        }
    }

    public traverse(callback: (area: Area) => void): void {
        callback(this);
        for (let i = 0; i < this._paths.length; i++) {
            this._paths[i].traverse(callback);
        }
    }

    public handlePlayerMoved(): void {
        if (!this._startingRoom) {
            throw new Error("Cannot handle player moved for area without a starting room!");
        }
        this._startingRoom.handlePlayerMoved();
        for (let i = 0; i < this._paths.length; i++) {
            this._paths[i].handlePlayerMoved();
        }
    }

    public getSortValue(): number {
        return Object.keys(AreaTypes).findIndex((element) => AreaTypes[element] === this._type) * 1000000 + this._level * 1000 + this._roomCount;
    }
}