/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

const enum EffectType {
    HEAL = "heal",
    INCREASE_LIFE = "bless-life",
    INCREASE_DAMAGE = "bless-damage",
    INCREASE_BLESSING_DURATION = "blessing-duration",
    INCREASE_MAX_BLESSING_DURATION = "max-blessing-duration",
    INCREASE_MAX_POTION_PER_TYPE = "max-potion-per-type",
    WEAPON_QUALITY = "weapon-quality",
    WEAPON_ENCHANT = "weapon-enchant",
    WEAPON_DISENCHANT = "weapon-disenchant",
    WEAPON_CATEGORY_DAMAGE = "weapon-category-damage",
    REDUCE_DAMAGE = "reduce damage",
    STUN = "stun",
    LEECH_LIFE = "leech-life",
    MULTIPLY_DAMAGE = "multiply-damage",
    INCREASE_NATURAL_HEAL = "increase-natural-heal",
    TRACKING = "tracking"
};

interface BasicEffect {
    type: EffectType.WEAPON_DISENCHANT;
}

interface NumericEffect {
    type: EffectType.HEAL | EffectType.INCREASE_BLESSING_DURATION;
    value: number;
}

interface NumericRangeEffect {
    type: EffectType.WEAPON_ENCHANT;
    category?: WeaponCategory;
    value: NumberRange;
}

interface CombatEffect {
    rounds: number;
}

interface TemporaryDamageRangeEffect extends CombatEffect {
    type: EffectType.INCREASE_DAMAGE;
    name: string;
    value: DamageRange;
}

interface BlessingEffect {
    battles: number;
}

interface NumericBlessingEffect extends BlessingEffect {
    type: EffectType.INCREASE_LIFE;
    name: string;
    value: number;
}

interface DamageRangeBlessingEffect extends BlessingEffect {
    type: EffectType.INCREASE_DAMAGE;
    name: string;
    value: DamageRange;
}

interface PassiveSkillEffect {
    passiveSkill: PassiveSkillType;
}

interface PassiveNumericEffect extends PassiveSkillEffect {
    type: EffectType.INCREASE_NATURAL_HEAL | EffectType.INCREASE_MAX_BLESSING_DURATION | EffectType.INCREASE_MAX_POTION_PER_TYPE | EffectType.TRACKING | EffectType.INCREASE_LIFE;
    value: number;
}

interface NumericWeaponCategoryEffect {
    type: EffectType.WEAPON_QUALITY;
    category: WeaponCategory;
    value: number;
}

interface WeaponCategoryPassiveDamageEffect extends PassiveSkillEffect {
    type: EffectType.WEAPON_CATEGORY_DAMAGE;
    category: WeaponCategory;
    value: Damage;
}

interface ActiveEffect {
    chance: number;
}

interface ActiveDamageEffect extends ActiveEffect {
    type: EffectType.REDUCE_DAMAGE | EffectType.LEECH_LIFE;
    value: Damage;
}

interface ActiveStatusEffect extends ActiveEffect {
    type: EffectType.STUN;
    rounds: FixedNumberRange;
}

interface DamageAfterEffect extends CombatEffect {
    type: EffectType.MULTIPLY_DAMAGE;
    value: Damage;
}

type Effect = BasicEffect | NumericEffect | NumericRangeEffect | TemporaryDamageRangeEffect |
    NumericBlessingEffect | DamageRangeBlessingEffect |
    PassiveNumericEffect |
    NumericWeaponCategoryEffect | WeaponCategoryPassiveDamageEffect |
    ActiveDamageEffect | ActiveStatusEffect | DamageAfterEffect;

function isCombatEffect(effect: Effect | CombatEffect): effect is CombatEffect {
    return "rounds" in effect;
}

function isBlessingEffect(effect: Effect | BlessingEffect): effect is BlessingEffect {
    return "battles" in effect;
}

function isPassiveSkillEffect(effect: Effect | PassiveSkillEffect): effect is PassiveSkillEffect {
    return "passiveSkill" in effect;
}

function isActiveEffect(effect: Effect | ActiveEffect): effect is ActiveEffect {
    return "chance" in effect;
}

function isImmediateEffect(effect: Effect): boolean {
    return !isCombatEffect(effect) && !isBlessingEffect(effect) && !isPassiveSkillEffect(effect);
}

function getCombatEffects(effects: Effect[]): CombatEffect[] {
    return (effects as (Effect | CombatEffect)[]).filter(isCombatEffect);
}

function getBlessings(effects: Effect[]): BlessingEffect[] {
    return (effects as (Effect | BlessingEffect)[]).filter(isBlessingEffect);
}

function getPassiveSkills(effects: Effect[]): PassiveSkillEffect[] {
    return (effects as (Effect | PassiveSkillEffect)[]).filter(isPassiveSkillEffect);
}

function getEffectChance(effect: Effect): number {
    return isActiveEffect(effect) ? effect.chance : 1;
}

function isEffectStronger(effect1: Effect, effect2: Effect): boolean {
    switch (effect1.type) {
        case EffectType.HEAL:
        case EffectType.INCREASE_LIFE:
        case EffectType.INCREASE_BLESSING_DURATION:
        case EffectType.INCREASE_MAX_BLESSING_DURATION:
        case EffectType.INCREASE_MAX_POTION_PER_TYPE:
        case EffectType.INCREASE_NATURAL_HEAL:
        case EffectType.TRACKING:
        case EffectType.WEAPON_QUALITY:
            return (effect1.value > (effect2 as NumericEffect).value);
        case EffectType.WEAPON_CATEGORY_DAMAGE:
        case EffectType.REDUCE_DAMAGE:
        case EffectType.LEECH_LIFE:
        case EffectType.MULTIPLY_DAMAGE:
            return Utils.isDamageAnyGreater((effect2 as WeaponCategoryPassiveDamageEffect).value, effect1.value);
        case EffectType.INCREASE_DAMAGE:
            return Utils.isDamageRangeAnyGreater((effect2 as DamageRangeBlessingEffect).value, effect1.value);
        case EffectType.WEAPON_ENCHANT:
            return (effect1.value[0] + effect1.value[1] > (effect2 as NumericRangeEffect).value[0] + (effect2 as NumericRangeEffect).value[1]);
        case EffectType.WEAPON_DISENCHANT:
            return false;
        case EffectType.STUN:
            return effect1.rounds[0] + effect1.rounds[1] > (effect2 as ActiveStatusEffect).rounds[0] + (effect2 as ActiveStatusEffect).rounds[1];
        default:
            return Utils.assertUnreachable(effect1);
    }
}

function sameNamedEffect(effect1: Effect, effect2: Effect): boolean {
    return ("name" in effect1) && ("name" in effect2) && (effect1.name === effect2.name);
}

function samePassiveSkillEffect(effect1: Effect, effect2: Effect): boolean {
    return isPassiveSkillEffect(effect1) && isPassiveSkillEffect(effect2) && (effect1.passiveSkill.name === effect2.passiveSkill.name);
}