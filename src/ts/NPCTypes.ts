/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface NPCServiceItems {
    weapon?: WeaponType;
    weaponInstance?: Weapon;
    potion?: PotionType;
}

interface NPCServiceDiscover {
    areas: readonly AreaType[];
    modifiers?: AreaModifiers;
}

interface NPCGeneralService {
    level?: number;
    unlockId?: string;
    image: string;
    cost: number;
    qualityImprovement?: number;
}

interface NPCEffectService {
    name: string;
    effect: Effect;
}

interface NPCItemService {
    items: NPCServiceItems;
}

interface NPCSkillService {
    skill: SkillType;
}

interface NPCDiscoverService {
    name: string;
    discover: NPCServiceDiscover;
}

type NPCService = NPCGeneralService & (NPCEffectService | NPCItemService | NPCSkillService | NPCDiscoverService);

interface NPCType extends NamedObject {
    image: string;
    upgradeCosts: readonly number[];
    maxImprovement: number;
    improvementItems: readonly NPCImprovementItem[];
    services: readonly NPCService[];
}

type NPCTypeMap = NamedObjectMap<NPCType>;

const NPCTypes: NPCTypeMap = {
    HEALER: {
        name: "Healer",
        image: "healer.png",
        upgradeCosts: [500, 1500],
        maxImprovement: 0.2,
        improvementItems: [{
            name: "Healing runes",
            quality: 0.05
        }],
        services: [{
            name: "Minor heal",
            image: "heal-minor.png",
            cost: 5,
            effect: {
                type: EffectType.HEAL,
                value: 10
            }
        }, {
            name: "Heal",
            level: 2,
            image: "heal.png",
            cost: 10,
            effect: {
                type: EffectType.HEAL,
                value: 25
            }
        }, {
            name: "Major heal",
            level: 3,
            image: "heal-major.png",
            cost: 15,
            effect: {
                type: EffectType.HEAL,
                value: 50
            }
        }, {
            name: "Life blessing",
            level: 3,
            image: "bless-life.png",
            cost: 100,
            effect: {
                name: "Life blessing",
                type: EffectType.INCREASE_LIFE,
                value: 25,
                battles: 3
            }
        }, {
            name: "Healing practices",
            level: 2,
            image: "heal-natural.png",
            cost: 350,
            effect: {
                type: EffectType.INCREASE_NATURAL_HEAL,
                value: 0.25,
                passiveSkill: PassiveSkills.NATURAL_HEALING
            }
        }, {
            name: "Divine light",
            level: 3,
            image: "bless-blessings.png",
            cost: 500,
            effect: {
                type: EffectType.INCREASE_MAX_BLESSING_DURATION,
                value: 1,
                passiveSkill: PassiveSkills.DIVINE_LIGHT
            }
        }]
    },
    SWORDSMITH: {
        name: "Swordsmith",
        image: "swordsmith.png",
        upgradeCosts: [500, 2000],
        maxImprovement: 0.2,
        improvementItems: [{
            name: "Magic whetstone",
            quality: 0.05
        }],
        services: [{
            name: "Sharpen sword",
            image: "sword-minor.png",
            cost: 100,
            effect: {
                type: EffectType.WEAPON_QUALITY,
                category: "sword",
                value: 1.1
            }
        }, {
            name: "Strengthen sword",
            level: 2,
            image: "sword-medium.png",
            cost: 500,
            effect: {
                type: EffectType.WEAPON_QUALITY,
                category: "sword",
                value: 1.2
            }
        }, {
            name: "Rework sword",
            level: 3,
            image: "sword-major.png",
            cost: 2500,
            effect: {
                type: EffectType.WEAPON_QUALITY,
                category: "sword",
                value: 1.3
            }
        }, {
            image: "sword.png",
            cost: 250,
            items: {
                weapon: WeaponTypes.ARMING_SWORD
            }
        }, {
            level: 3,
            image: "sword-master.png",
            cost: 2500,
            items: {
                weapon: WeaponTypes.MASTERPIECE_SWORD
            }
        }]
    },
    MAGE: {
        name: "Mage",
        image: "mage.png",
        upgradeCosts: [750, 2500],
        maxImprovement: 0.5,
        improvementItems: [{
            name: "Tome of sorcery",
            quality: 0.1
        }],
        services: [{
            name: "Minor enchantment",
            image: "enchant-minor.png",
            cost: 150,
            effect: {
                type: EffectType.WEAPON_ENCHANT,
                value: [1, 1]
            }
        }, {
            name: "Enchantment",
            level: 2,
            image: "enchant-medium.png",
            cost: 600,
            effect: {
                type: EffectType.WEAPON_ENCHANT,
                value: [2, 3]
            }
        }, {
            name: "Major enchantment",
            level: 3,
            image: "enchant-major.png",
            cost: 3000,
            effect: {
                type: EffectType.WEAPON_ENCHANT,
                value: [3, 5]
            }
        }, {
            name: "Epic enchantment",
            unlockId: "enchant-epic",
            image: "enchant-epic.png",
            cost: 10000,
            effect: {
                type: EffectType.WEAPON_ENCHANT,
                value: [5, 10]
            }
        }, {
            name: "Disenchant weapon",
            level: 2,
            image: "enchant-disenchant.png",
            cost: 500,
            effect: {
                type: EffectType.WEAPON_DISENCHANT
            }
        }, {
            image: "leech-life.png",
            cost: 600,
            skill: Skills.LEECH_LIFE
        }, {
            level: 2,
            image: "arcane-blast.png",
            cost: 1000,
            skill: Skills.ARCANE_BLAST
        }, {
            name: "Damage blessing",
            level: 2,
            image: "bless-damage.png",
            cost: 200,
            effect: {
                name: "Damage blessing",
                type: EffectType.INCREASE_DAMAGE,
                value: {
                    physical: [1, 2]
                },
                battles: 3
            }
        }, {
            name: "Direct magic",
            image: "staff-magic-minor.png",
            cost: 750,
            effect: {
                type: EffectType.WEAPON_CATEGORY_DAMAGE,
                category: "staff",
                value: {
                    magic: 1.5
                },
                passiveSkill: PassiveSkills.STAFF_MAGIC_FOCUS
            }
        }, {
            name: "Focus magic",
            level: 3,
            image: "staff-magic-major.png",
            cost: 3500,
            effect: {
                type: EffectType.WEAPON_CATEGORY_DAMAGE,
                category: "staff",
                value: {
                    magic: 2.5
                },
                passiveSkill: PassiveSkills.STAFF_MAGIC_FOCUS
            }
        }, {
            image: "staff-apprentice.png",
            cost: 250,
            items: {
                weapon: WeaponTypes.APPRENTICE_STAFF
            }
        }, {
            level: 2,
            image: "staff-mage.png",
            cost: 1250,
            items: {
                weapon: WeaponTypes.MAGE_STAFF
            }
        }]
    },
    ALCHEMIST: {
        name: "Alchemist",
        image: "alchemist.png",
        upgradeCosts: [500, 1750],
        maxImprovement: 0.2,
        improvementItems: [{
            name: "Enchanted alembic",
            quality: 0.05
        }],
        services: [{
            image: "potion-minor.png",
            cost: 10,
            items: {
                potion: PotionTypes.SMALL_LIFE
            }
        }, {
            level: 2,
            image: "potion-medium.png",
            cost: 15,
            items: {
                potion: PotionTypes.MEDIUM_LIFE
            }
        }, {
            level: 3,
            image: "potion-major.png",
            cost: 20,
            items: {
                potion: PotionTypes.LARGE_LIFE
            }
        }, {
            image: "potion-magic-minor.png",
            cost: 25,
            items: {
                potion: PotionTypes.SMALL_MAGIC
            }
        }, {
            level: 2,
            image: "potion-magic-medium.png",
            cost: 35,
            items: {
                potion: PotionTypes.MEDIUM_MAGIC
            }
        }, {
            level: 3,
            image: "potion-magic-major.png",
            cost: 50,
            items: {
                potion: PotionTypes.LARGE_MAGIC
            }
        }, {
            level: 2,
            image: "potion-blessings.png",
            cost: 100,
            items: {
                potion: PotionTypes.BLESSINGS
            }
        }, {
            level: 3,
            name: "Efficient potion storage",
            image: "potion-satchel.png",
            cost: 500,
            effect: {
                type: EffectType.INCREASE_MAX_POTION_PER_TYPE,
                value: 1,
                passiveSkill: PassiveSkills.EFFICIENT_POTION_STORAGE
            }
        }]
    },
    SCOUT: {
        name: "Scout",
        image: "scout.png",
        upgradeCosts: [500, 2000],
        maxImprovement: 1.0,
        improvementItems: [{
            name: "Lost map",
            quality: 0.25
        }],
        services: [{
            name: "Scout surroundings",
            image: "scout-minor.png",
            cost: 50,
            discover: {
                areas: [AreaTypes.FOREST, AreaTypes.MOUNTAINS, AreaTypes.RIVERLAND]
            }
        }, {
            name: "Scout for riches",
            image: "scout-treasure-minor.png",
            cost: 100,
            discover: {
                areas: [AreaTypes.FOREST, AreaTypes.MOUNTAINS, AreaTypes.RIVERLAND],
                modifiers: {
                    treasure: 1.5
                }
            }
        }, {
            level: 2,
            name: "Scout ahead",
            image: "scout-medium.png",
            cost: 200,
            discover: {
                areas: [AreaTypes.FIELDS, AreaTypes.DESERT, AreaTypes.SWAMP]
            }
        }, {
            level: 2,
            name: "Scout for treasure",
            image: "scout-treasure-medium.png",
            cost: 400,
            discover: {
                areas: [AreaTypes.FIELDS, AreaTypes.DESERT, AreaTypes.SWAMP],
                modifiers: {
                    treasure: 1.5
                }
            }
        }, {
            level: 3,
            name: "Scout far",
            image: "scout-major.png",
            cost: 1000,
            discover: {
                areas: [AreaTypes.CAVES, AreaTypes.SEASIDE, AreaTypes.CASTLE]
            }
        }, {
            level: 3,
            name: "Scout for secrets",
            image: "scout-treasure-major.png",
            cost: 2000,
            discover: {
                areas: [AreaTypes.CAVES, AreaTypes.SEASIDE, AreaTypes.CASTLE],
                modifiers: {
                    treasure: 1.5
                }
            }
        }, {
            name: "Tracking",
            level: 2,
            image: "tracking.png",
            cost: 750,
            effect: {
                type: EffectType.TRACKING,
                value: 1,
                passiveSkill: PassiveSkills.TRACKING
            }
        }]
    },
    TRAINER: {
        name: "Trainer",
        image: "trainer.png",
        upgradeCosts: [1000, 3000],
        maxImprovement: 0.2,
        improvementItems: [{
            name: "Training manual",
            quality: 0.05
        }],
        services: [{
            name: "Basic sword training",
            image: "training-sword-basic.png",
            cost: 500,
            effect: {
                type: EffectType.WEAPON_CATEGORY_DAMAGE,
                category: "sword",
                value: {
                    physical: 1.1
                },
                passiveSkill: PassiveSkills.SWORD_EXPERTISE
            }
        }, {
            name: "Advanced sword training",
            level: 2,
            image: "training-sword-advanced.png",
            cost: 1000,
            effect: {
                type: EffectType.WEAPON_CATEGORY_DAMAGE,
                category: "sword",
                value: {
                    physical: 1.3
                },
                passiveSkill: PassiveSkills.SWORD_EXPERTISE
            }
        }, {
            name: "Master sword training",
            level: 3,
            image: "training-sword-master.png",
            cost: 2000,
            effect: {
                type: EffectType.WEAPON_CATEGORY_DAMAGE,
                category: "sword",
                value: {
                    physical: 1.5
                },
                passiveSkill: PassiveSkills.SWORD_EXPERTISE
            }
        }, {
            name: "Basic axe training",
            image: "training-axe-basic.png",
            cost: 500,
            effect: {
                type: EffectType.WEAPON_CATEGORY_DAMAGE,
                category: "axe",
                value: {
                    physical: 1.1
                },
                passiveSkill: PassiveSkills.AXE_EXPERTISE
            }
        }, {
            name: "Advanced axe training",
            level: 2,
            image: "training-axe-advanced.png",
            cost: 1000,
            effect: {
                type: EffectType.WEAPON_CATEGORY_DAMAGE,
                category: "axe",
                value: {
                    physical: 1.3
                },
                passiveSkill: PassiveSkills.AXE_EXPERTISE
            }
        }, {
            name: "Master axe training",
            level: 3,
            image: "training-axe-master.png",
            cost: 2000,
            effect: {
                type: EffectType.WEAPON_CATEGORY_DAMAGE,
                category: "axe",
                value: {
                    physical: 1.5
                },
                passiveSkill: PassiveSkills.AXE_EXPERTISE
            }
        }, {
            name: "Fortitude",
            level: 2,
            image: "fortitude.png",
            cost: 1000,
            effect: {
                type: EffectType.INCREASE_LIFE,
                value: 15,
                passiveSkill: PassiveSkills.FORTITUDE
            }
        }, {
            name: "Endurance training",
            level: 3,
            image: "endurance.png",
            cost: 2500,
            effect: {
                type: EffectType.INCREASE_LIFE,
                value: 30,
                passiveSkill: PassiveSkills.ENDURANCE
            }
        }, {
            image: "crippling-cut.png",
            cost: 600,
            skill: Skills.CRIPPLING_CUT
        }, {
            image: "stunning-blow.png",
            cost: 600,
            skill: Skills.STUNNING_BLOW
        }]
    }
};

