/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface GameObjectState {
    _id: string;
}

abstract class GameObject extends GeneralObject {
    constructor(id: string) {
        super(id);
    }

    public save(): GameObjectState {
        this.log("Saving...", 2);
        return {
            _id: this._id
        };
    }

    public load(state: GameObjectState): GameObject {
        this._id = state._id;
        this.clearEventHandlers();
        this.log("Loading...", 2);
        return this;
    }
}