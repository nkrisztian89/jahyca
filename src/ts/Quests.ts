const Quests: QuestParams[] = [{
    id: "Beginner",
    name: "Elven forest",
    giver: "Healer",
    introText: "I see you have cleared an entire area of monsters by yourself! Thank Cascados we finally have a true warrior in town! If you venture a little deeper into the forest and make it safe for us, I would be grateful! It has become really dangerous to pick my herbs because of the elves.",
    type: "clearNewAreas",
    areaParams: [{
        typeName: "Forest",
        modifiers: {}
    }],
    completionText: "You have done it! I can gather all the ingredients I need for healing safely again. I am thankful on behalf of all the people of this town. Please, take this humble reward!",
    reward: {
        loot: {
            gold: {
                amount: [300, 300],
                chance: 1
            },
            potions: [{
                type: PotionTypes.LARGE_LIFE,
                chances: [0, 1]
            }]
        }
    }
}, {
    id: "Insects",
    name: "Insects in the riverlands",
    giver: "Swordsmith",
    introText: "Recently I have had trouble getting the materials for smithing from the riverlands. The trade caravans are getting attacked by vicious insects who are trying to expand their control over the area. If you drive the insects back, I will offer you one of my services for free!",
    type: "clearNewAreas",
    areaParams: [{
        typeName: "Riverland",
        modifiers: {
            allEnemies: {
                hpFactor: 2,
                nameReplacements: [["Insect Warrior", "Insect Marauder"]]
            },
            enemyChance: 0.9,
            enemyTypeProbabilities: {
                INSECT_WARRIOR: 1
            }
        }
    }],
    completionText: "The trade routes to inner Cascadia are safe again! Thank you, adventurer! As promised, you may use any of my services once for free!",
    reward: {
        freeServices: 1
    }
}, {
    id: "Dragon",
    name: "Dragon of the mountains",
    giver: "Mage",
    introText: "Glad you came, hero! You have proven your worth in battling dangerous beasts, and this talent of yours is just what I need right now. The wizards of the Obno mountains have always been expert enchanters. I have finally located where they had hidden some of their scrolls that describe high level enchanting. But they had summoned a powerful dragon to guard them! If you defeat the dragon and bring me the scrolls, I might learn more powerful enchantments from them to cast on your weapons!",
    type: "retrieveItem",
    itemToComplete: {
        name: "Scrolls of enchantment"
    },
    areaParams: [{
        typeName: "Mountains",
        modifiers: {
            boss: {
                typeName: "DRAGON",
                modifiers: {
                    glowColor: "orange",
                    hpFactor: 2,
                    nameReplacements: [["Dragon", "Guardian Dragon"]]
                },
                loot: {
                    gold: {
                        chance: 1,
                        amount: [1250, 1500]
                    },
                    questItems: [{
                        item: {
                            name: "Scrolls of enchantment",
                            questId: "Dragon"
                        },
                        chance: 1
                    }]
                }
            }
        }
    }],
    completionText: "You have brought back the scrolls! Let me take a look... Just as I have thought! These spells are more powerful than the ones I learned in the Arcane Library. With a little study, I will be able to cast them myself!",
    reward: {
        unlockServices: ["enchant-epic"]
    }
}];

function getQuest(id: string): QuestParams | null {
    return Quests.find(quest => quest.id === id) ?? null;
}