/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

type GeneralEventHandler = (params?: any) => void;

interface EventHandlerMap {
    [name: string]: GeneralEventHandler[];
}

abstract class GeneralObject {

    private _index: number;
    protected _id: string;
    private _eventHandlers: EventHandlerMap;

    constructor(id: string) {
        this._index = Utils.count(this.getObjectType());
        this._id = id;
        this._eventHandlers = {};

        this.log("created " + this.getObjectType() + " instance", 2);

        Utils.incCount(this.getObjectType());
    }
    
    public get id(): string {
        return this._id;
    }

    public abstract getObjectType(): string;

    public log(message: string, logLevel: number): void {
        if ((logLevel || 0) <= DEBUG_LOG_LEVEL) {
            console.log("[" + this._index + (this._id ? ":" + this._id : "") + "]: " + message);
        }
    }

    public warn(message: string, logLevel: number): void {
        if ((logLevel || 0) <= DEBUG_LOG_LEVEL) {
            console.warn("[" + this._index + (this._id ? ":" + this._id : "") + "]: " + message);
        }
    }

    /**
     * @param {String} name
     * @param {Function} handler
     */
    public addEventHandler(name: string, handler: GeneralEventHandler): void {
        this._eventHandlers[name] = this._eventHandlers[name] || [];
        this._eventHandlers[name].push(handler);
        this.log("Added handler for '" + name + "' events.", 2);
    }

    /**
     * @param {String} name
     * @param {Function} handler
     */
    public removeEventHandler(name: string, handler: GeneralEventHandler): void {
        if (this._eventHandlers[name]) {
            const handlers = this._eventHandlers[name];
            const index = handlers.indexOf(handler);
            if (index >= 0) {
                handlers.splice(index, 1);
                this.log("Removed handler for '" + name + "' events.", 2);
            } else {
                this.log("Cannot remove handler for '" + name + "' events!", 1);
            }
        } else {
            this.log("Cannot remove handler for '" + name + "' events!", 1);
        }
    }
    
    public clearEventHandlers(): void {
        this._eventHandlers = {};
    }

    public triggerEvent(name: string, params?: any) {
        this.log("Triggering '" + name + "' event...", 2);
        if (this._eventHandlers[name]) {
            const handlers = this._eventHandlers[name];
            for (let i = 0; i < handlers.length; i++) {
                handlers[i](params);
            }
        }
    }
}