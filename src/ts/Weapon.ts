/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface WeaponState {
    type: string;
    _quality: number;
    _enchantDamage: NumberRange;
}

class Weapon {
    private _type: WeaponType | null;
    private _quality: number;
    private _damage: NumberRange;
    private _physicalDamage: NumberRange;
    private _magicDamage: NumberRange;
    private _enchantDamage: NumberRange;

    constructor(type: WeaponType | null = null, quality: number = 1) {
        console.log("created Weapon instance");

        this._type = type;

        this._quality = quality;

        this._damage = [0, 0];

        this._physicalDamage = [0, 0];
        this._magicDamage = [0, 0];

        this._enchantDamage = [0, 0];

        if (type) {
            this.updateDamage();
        }
    }

    public save(): WeaponState {
        if (!this._type) {
            throw new Error("Cannot save weapon without a type!");
        }
        return {
            type: this._type.name,
            _quality: this._quality,
            _enchantDamage: this._enchantDamage
        };
    }

    public load(state: WeaponState): Weapon {
        this._type = Utils.findByName(state.type, WeaponTypes);
        this._quality = state._quality;
        this._enchantDamage = state._enchantDamage;

        this.updateDamage();

        return this;
    }

    public copy(): Weapon {
        return new Weapon(this._type, this._quality);
    }

    private updateDamage(): void {
        if (!this._type) {
            throw new Error("Cannot update damage of weapon without a type!");
        }
        this._physicalDamage[0] = Math.round(this._type.damage[0] * this._quality);
        this._physicalDamage[1] = Math.round(this._type.damage[1] * this._quality);
        this._magicDamage[0] = this._enchantDamage[0];
        this._magicDamage[1] = this._enchantDamage[1];
        this._damage[0] = this._physicalDamage[0] + this._magicDamage[0];
        this._damage[1] = this._physicalDamage[1] + this._magicDamage[1];
    }

    public get category(): WeaponCategory {
        return this._type ? this._type.category : "unknown";
    }
    
    public get physicalDamage(): FixedNumberRange {
        return this._physicalDamage;
    }
    
    public get magicDamage(): FixedNumberRange {
        return this._magicDamage;
    }

    public get hasMagicDamage(): boolean {
        return (this._magicDamage[0] > 0) || (this._magicDamage[1] > 0);
    }

    public get quality(): number {
        return this._quality;
    }
    public set quality(value: number) {
        this._quality = value;
        this.updateDamage();
    }
    
    public get typeName(): string {
        return this._type?.name ?? "unknown weapon";
    }

    public get description(): string {
        return this._type ? (this._type.category + "<br>" + Utils.formatRange("physical-damage", this._damage)) : "unknown weapon";
    }

    public get longDescription(): string {
        return '<span class="weapon-description">' +
            (this._type ?
                (this._type.category + ", " +
                    Utils.formatRange("physical-damage", this._physicalDamage) +
                    ((this._quality !== 1) ? (' <span class="item-quality">(' + Math.round(this._quality * 100) + "%)</span>") : "") +
                    (this.hasMagicDamage ? ", " + Utils.formatRange("magic-damage", this._magicDamage) : "")) :
                "unknown weapon") +
            "</span>";
    }

    public get image(): string {
        return this._type ? this._type.image: "";
    }

    public get attackEffect(): AttackEffect {
        return this._type ? this._type.attackEffect : PUNCH_ATTACK_EFFECT;
    }

    public toString(): string {
        return this._type ? (this._type.name + " (" + this.longDescription + ")") : "unknown weapon";
    }

    public getDamage(): number {
        return Utils.randomRange(this._damage[0], this._damage[1]);
    }

    public get enchanted(): boolean {
        return (this._enchantDamage[0] > 0) || (this._enchantDamage[1] > 0);
    }

    public enchant(damage: NumberRange): void {
        this._enchantDamage[0] = damage[0];
        this._enchantDamage[1] = damage[1];
        this.updateDamage();
    }
    
    public disenchant(): void {
        this._enchantDamage[0] = 0;
        this._enchantDamage[1] = 0;
        this.updateDamage();
    }
    
    public anyStrongerThan(weapon: Weapon, compareBaseStats: boolean = false): boolean {
        if (!this._type || !weapon._type) {
            return false;
        }
        const damage1 = compareBaseStats ? this._type.damage : this._damage;
        const damage2 = compareBaseStats ? weapon._type.damage : weapon._damage;
        return (damage1[0] > damage2[0]) || (damage1[1] > damage2[1]);
    }
    
    public anyWeakerThan(weapon: Weapon, compareBaseStats: boolean = false): boolean {
        if (!this._type || !weapon._type) {
            return false;
        }
        const damage1 = compareBaseStats ? this._type.damage : this._damage;
        const damage2 = compareBaseStats ? weapon._type.damage : weapon._damage;
        return (damage1[0] < damage2[0]) || (damage1[1] < damage2[1]);
    }
}