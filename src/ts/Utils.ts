/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface AnyObject {
    [name: string]: any;
}

interface NumberMap {
    [name: string]: number;
}

type ProbabilityMap = NumberMap;

interface NamedObject {
    name: string;
}

interface NamedObjectMap<T extends NamedObject> {
    [name: string]: T;
}

type WeaponCategory = "unknown" | "staff" | "sword" | "axe";

interface FullDamage {
    physical: number;
    magic: number;
}

type Damage = Partial<FullDamage>;

type NumberRange = [number, number];

interface FullDamageRange {
    physical: NumberRange;
    magic: NumberRange;
}

type DamageRange = Partial<FullDamageRange>;

type FixedNumberRange = Readonly<NumberRange>;

interface FixedFullDamageRange {
    physical: FixedNumberRange;
    magic: FixedNumberRange;
}

type FixedDamageRange = Partial<FixedFullDamageRange>;

type DamageFactor = Damage;

type FullDamageFactor = FullDamage;

interface GoldLootChance {
    chance: number;
    amount: FixedNumberRange;
}

interface WeaponLootChance {
    type: WeaponType;
    chance: number;
    quality?: FixedNumberRange;
}

interface PotionLootChances {
    type: PotionType;
    chances: readonly number[];
}

type NPCName = "Healer" | "Swordsmith" | "Mage" | "Alchemist" | "Scout" | "Trainer";

interface NPCImprovementItemChance {
    npc: NPCName;
    chance: number;
}

interface QuestItemChance {
    item: QuestItem;
    chance: number;
}

interface LootChances {
    gold?: GoldLootChance;
    weapons?: readonly WeaponLootChance[];
    potions?: readonly PotionLootChances[];
    npcImprovementItem?: readonly NPCImprovementItemChance[];
    questItems?: readonly QuestItemChance[];
}

interface Potions {
    type: PotionType;
    amount: number;
}

interface NPCImprovementItem {
    name: string;
    quality: number;
}

interface QuestItem {
    name: string;
    questId: string;
}

interface Loot {
    gold?: number;
    weapon?: Weapon;
    potions?: Potions[];
    npcImprovementItem?: NPCImprovementItem;
    questItems?: QuestItem[];
}

interface LootState {
    gold?: number;
    weapon?: WeaponState;
    potions?: Potions[];
    npcImprovementItem?: NPCImprovementItem;
    questItems?: QuestItem[];
}

const WEAPON_CATEGORY_WITH = {
    sword: "with swords",
    axe: "with axes",
    staff: "with staves",
    unknown: "-"
};

const MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

const _counters: NumberMap = {};

const Utils = {
    shallowCopy: (data: AnyObject): AnyObject => Object.assign({}, data),
    deepCopy: (data: AnyObject | AnyObject[]): AnyObject | AnyObject[] => {
        let result: AnyObject | AnyObject[], i: number;
        if (typeof data === "object") {
            if (data instanceof Array) {
                result = [];
                for (i = 0; i < data.length; i++) {
                    result.push(Utils.deepCopy(data[i]));
                }
                return result;
            }
            result = {};
            const propertyNames = Object.keys(data);
            for (i = 0; i < propertyNames.length; i++) {
                result[propertyNames[i]] = Utils.deepCopy(data[propertyNames[i]]);
            }
            return result;
        }
        return data;
    },
    deepCopyInto: (destination: any, source: any): void => {
        let i: number;
        if (!destination || (typeof destination !== "object")) {
            return;
        }
        if (source instanceof Array && destination instanceof Array) {
            for (i = 0; i < source.length; i++) {
                if (typeof source[i] === "object") {
                    if (source[i] === null) {
                        switch (typeof destination[i]) {
                            case "object":
                            case "undefined":
                                destination[i] = null;
                                break;
                            case "number":
                                destination[i] = 0;
                                break;
                            case "boolean":
                                destination[i] = false;
                                break;
                            case "string":
                                destination[i] = "";
                                break;
                        }
                    } else {
                        Utils.deepCopyInto(destination[i], source[i]);
                    }
                } else {
                    if (source[i] !== undefined) {
                        destination[i] = source[i];
                    }
                }
            }
        }
        if (!(source instanceof Array) && !(destination instanceof Array)) {
            const propertyNames = Object.keys(source);
            for (i = 0; i < propertyNames.length; i++) {
                if (typeof source[propertyNames[i]] === "object") {
                    if (source[propertyNames[i]] === null) {
                        switch (typeof destination[propertyNames[i]]) {
                            case "object":
                            case "undefined":
                                (destination as AnyObject)[propertyNames[i]] = null;
                                break;
                            case "number":
                                (destination as AnyObject)[propertyNames[i]] = 0;
                                break;
                            case "boolean":
                                (destination as AnyObject)[propertyNames[i]] = false;
                                break;
                            case "string":
                                (destination as AnyObject)[propertyNames[i]] = "";
                                break;
                        }
                    } else {
                        Utils.deepCopyInto(destination[propertyNames[i]], source[propertyNames[i]]);
                    }
                } else {
                    if (source[propertyNames[i]] !== undefined) {
                        (destination as AnyObject)[propertyNames[i]] = source[propertyNames[i]];
                    }
                }
            }
        }
    },
    objectsDeepEqual: (object1: AnyObject, object2: AnyObject): boolean => {
        let keys1: string[], keys2: string[];
        if ((object1 === null) && (object2 === null)) {
            return true;
        }
        if (object1) {
            keys1 = Object.keys(object1).sort();
        } else {
            return false;
        }
        if (object2) {
            keys2 = Object.keys(object2).sort();
        } else {
            return false;
        }
        if (keys1.length !== keys2.length) {
            return false;
        }
        for (let i = 0; i < keys1.length; i++) {
            if ((keys1[i] !== keys2[i]) || ((object1[keys1[i]] !== object2[keys2[i]]) && (
                (typeof object1[keys1[i]] !== "object") || (typeof object2[keys2[i]] !== "object") ||
                !Utils.objectsDeepEqual(object1[keys1[i]], object2[keys2[i]])))) {
                return false;
            }
        }
        return true;
    },
    randomRange: (a: number, b: number): number => a + Math.floor(Math.random() * (b - a + 1)),
    chance: (p: number): boolean => Math.random() <= p,
    chooseFromProbabilitiesObject: (probabilities: ProbabilityMap): string => {
        const probArray: number[] = Object.values(probabilities);

        const p = Math.random();
        let n = 0, pSum = 0;
        while ((n < probArray.length) && (p >= pSum)) {
            pSum += probArray[n];
            n++;
        }

        return Object.keys(probabilities)[n - 1];
    },
    chooseFromProbabilitiesArray: (probabilities: number[] | readonly number[]): number => {

        const p = Math.random();
        let n = 0, pSum = 0;
        while ((n < probabilities.length) && (p >= pSum)) {
            pSum += probabilities[n];
            n++;
        }

        return n - 1;
    },
    updateViews: <T, V extends GeneralView>(parent: GeneralView, views: V[], objects: T[], createViewFn: (index: number) => V, updateCallback: (view: V, object: T, index: number) => void): void => {
        let i;
        for (i = 0; i < objects.length; i++) {
            if (views.length <= i) {
                views.push(createViewFn(i));
                parent.add(views[i]);
            }
            views[i].hidden = false;
            updateCallback(views[i], objects[i], i);
        }
        while (i < views.length) {
            views[i].hidden = true;
            i++;
        }
    },
    span: (className: string, text: string): string => ('<span class="' + className + '">' + text + '</span>'),
    findByName: <T extends NamedObject>(name: string, objects: NamedObjectMap<T>): T | null => {
        const keys = Object.keys(objects);
        for (let i = 0; i < keys.length; i++) {
            if (objects[keys[i]].name === name) {
                return objects[keys[i]];
            }
        }
        return null;
    },
    mapByNames: <T extends NamedObject>(list: string[], objects: NamedObjectMap<T>): T[] => {
        const result: T[] = [];
        list.forEach(name => {
            const found = Utils.findByName(name, objects);
            if (found) {
                result.push(found);
            } else {
                console.warn("Could not find '" + name + "' among objects!");
            }
        });
        return result;
    },
    findKey: <T extends NamedObject>(name: string, objects: NamedObjectMap<T>): string | null => {
        const value = Utils.findByName(name, objects);
        return Object.keys(objects).find((key) => objects[key] === value) ?? null;
    },
    chooseRandom: <T>(array: T[] | readonly T[]): T => array[Utils.randomRange(0, array.length - 1)],
    /**
     * @param relative With relative = false: 0.5 -> 50%, 1.5 -> 150%, with relative = true: 0.5 -> -50%, 1.5 -> +50%
     */
    percent: (value: number, relative: boolean = false): string => {
        if (relative) {
            value -= 1;
        }
        return ((relative && (value > 0)) ? "+" : "") + Math.round(value * 100) + "%";
    },
    withWeapon: (category: WeaponCategory): string => WEAPON_CATEGORY_WITH[category],
    format: (className: string, text: string): string => '<span' + (className ? ' class="' + className + '"' : '') + '>' + text + "</span>",
    formatRange: (className: string, range: NumberRange | FixedNumberRange): string => Utils.format(className, range[0] + " - " + range[1]),
    incCount: (name: string): void => {
        _counters[name] = _counters[name] || 0;
        _counters[name]++;
    },
    count: (name: string): number => (_counters[name] || 0),
    generateLoot: (params: LootChances | null, factor: number): Loot | null => {
        if (!params) {
            return null;
        }
        factor = factor || 1;
        let result: Loot | null = null;
        if (params.gold) {
            if (Utils.chance(params.gold.chance)) {
                result = result || {};
                result.gold = Math.round(Utils.randomRange(params.gold.amount[0], params.gold.amount[1]) * factor);
            }
        }
        if (params.weapons) {
            for (let i = 0; i < params.weapons.length; i++) {
                if (Utils.chance(params.weapons[i].chance)) {
                    result = result || {};
                    const quality = Utils.randomRange(100 * (params.weapons[i].quality?.[0] ?? 1), 100 * (params.weapons[i].quality?.[1] ?? 1)) * 0.01;
                    result.weapon = new Weapon(params.weapons[i].type, quality);
                    break;
                }
            }
        }
        if (params.potions) {
            for (let i = 0; i < params.potions.length; i++) {
                const potion = params.potions[i];
                const amount = Utils.chooseFromProbabilitiesArray(potion.chances);
                if (amount > 0) {
                    result = result || {};
                    result.potions = result.potions || [];
                    result.potions.push({
                        type: potion.type,
                        amount
                    });
                }
            }
        }
        if (params.npcImprovementItem) {
            const item = params.npcImprovementItem;
            const r = Math.random();
            let p = 0;
            for (let i = 0; i < item.length; i++) {
                p += item[i].chance;
                if (r < p) {
                    const npc = Utils.findByName(item[i].npc, NPCTypes);
                    if (npc) {
                        result = result || {};
                        result.npcImprovementItem = Utils.chooseRandom(npc.improvementItems);
                    } else {
                        console.warn("Cannot find improvement item for NPC: '" + item[i].npc + "' as such NPC doesn't exist!");
                    }
                    break;
                }
            }
        }
        if (params.questItems) {
            const item = params.questItems;
            for (let i = 0; i < item.length; i++) {
                const r = Math.random();
                if (r < item[i].chance) {
                    result = result || {};
                    result.questItems = result.questItems || [];
                    result.questItems.push((item[i].item));
                }
            }
        }
        return result;
    },
    valueCount: (object: any) => {
        let result = 0;
        if (typeof object === "object") {
            for (const key of Object.keys(object)) {
                if (object[key] !== undefined) {
                    result++;
                }
            }
        } else {
            return 0;
        }
        return result;
    },
    saveLoot: (loot: Loot): LootState => {
        return Object.assign({}, loot, {weapon: loot.weapon ? loot.weapon.save() : undefined});
    },
    loadLoot: (state: LootState): Loot => {
        return Object.assign({}, state, {weapon: state.weapon ? new Weapon().load(state.weapon) : undefined});
    },
    getDamage: (damage: FullDamageRange | FixedFullDamageRange, factor?: DamageFactor): FullDamage => {
        factor = factor || {};
        if (factor.physical === undefined)
            factor.physical = 1;
        if (factor.magic === undefined)
            factor.magic = 1;
        return {
            physical: Math.round(factor.physical * Utils.randomRange(damage.physical[0], damage.physical[1])),
            magic: Math.round(factor.magic * Utils.randomRange(damage.magic[0], damage.magic[1]))
        };
    },
    addDamageRange: (original: DamageRange, extra: DamageRange | FixedDamageRange): DamageRange => {
        if (extra.physical) {
            original.physical = original.physical || [0, 0];
            original.physical[0] += extra.physical[0];
            original.physical[1] += extra.physical[1];
        }
        if (extra.magic) {
            original.magic = original.magic || [0, 0];
            original.magic[0] += extra.magic[0];
            original.magic[1] += extra.magic[1];
        }
        return original;
    },
    setDamage: (original: Damage, newValue: Damage): Damage => {
        if (newValue.physical) {
            original.physical = newValue.physical;
        }
        if (newValue.magic) {
            original.magic = newValue.magic;
        }
        return original;
    },
    /**
     * (2, 0.25) -> 0.5
     */
    mulDamage: (original: Damage, newValue: Damage): Damage => {
        if (newValue.physical !== undefined) {
            if (original.physical === undefined) {
                original.physical = 1;
            }
            original.physical *= newValue.physical;
        }
        if (newValue.magic !== undefined) {
            if (original.magic === undefined) {
                original.magic = 1;
            }
            original.magic *= newValue.magic;
        }
        return original;
    },
    /**
     * (2, 0.25) -> 1.5 (2 * (1 - 0.25))
     */
    subRelDamage: (original: Damage, modifier: Damage): Damage => {
        if (modifier.physical !== undefined) {
            if (original.physical === undefined) {
                original.physical = 1;
            }
            original.physical *= (1 - modifier.physical);
        }
        if (modifier.magic !== undefined) {
            if (original.magic === undefined) {
                original.magic = 1;
            }
            original.magic *= (1 - modifier.magic);
        }
        return original;
    },
    isDamageAnyGreater: (original: Damage, newValue: Damage): boolean => {
        let result = false;
        if (newValue.physical) {
            result = (original === undefined) ||
                (original.physical === undefined) ||
                (original.physical < newValue.physical);
        }
        if (newValue.magic) {
            return result || (original === undefined) ||
                (original.magic === undefined) ||
                (original.magic < newValue.magic);
        } else {
            return result;
        }
    },
    isDamageRangeAnyGreater: (original: DamageRange, newValue: DamageRange): boolean => {
        let result = false;
        if (newValue.physical) {
            result = (original === undefined) ||
                (original.physical === undefined) ||
                ((original.physical[0] + original.physical[1]) < (newValue.physical[0] + newValue.physical[1]));
        }
        if (newValue.magic) {
            return result || (original === undefined) ||
                (original.magic === undefined) ||
                ((original.magic[0] + original.magic[1]) < (newValue.magic[0] + newValue.magic[1]));
        } else {
            return result;
        }
    },
    formatDamageRange: (damage: DamageRange | FixedDamageRange, withSum: boolean = false): string => {
        let result = "";
        if (withSum) {
            result += Utils.formatRange("damage", [(damage.physical?.[0] ?? 0) + (damage.magic?.[0] ?? 0), (damage.physical?.[1] ?? 0) + (damage.magic?.[1] ?? 0)]) + " (";
        }
        if (damage.physical && (damage.physical[1] > 0)) {
            result += Utils.formatRange("physical-damage", damage.physical);
        }
        if (damage.magic && (damage.magic[1] > 0)) {
            result += (damage.physical ? ", " : "") + Utils.formatRange("magic-damage", damage.magic);
        }
        if (withSum) {
            result += ")";
        }
        return result || "-";
    },
    formatDamagePercent: (damage: Damage, relative: boolean = false, negative: boolean = false): string => {
        let result = "";
        if (damage.physical !== undefined) {
            if (!relative || damage.physical !== 1) {
                result += Utils.format("physical-damage", Utils.percent(damage.physical * (negative ? -1 : 1), relative));
            }
        }
        if (damage.magic !== undefined) {
            if (!relative || damage.magic !== 1) {
                result += (result ? ", " : "") + Utils.format("magic-damage", Utils.percent(damage.magic * (negative ? -1 : 1), relative));
            }
        }
        return result || "-";
    },
    getLevelFactor: (level: number): number => {
        return (1 + 0.1 * (level || 0));
    },
    formatTimeAgo: (timestamp: string, now: number) => {
        const time = Date.UTC(+timestamp.substring(0, 4), +timestamp.substring(5, 7) - 1, +timestamp.substring(8, 10), +timestamp.substring(11, 13), +timestamp.substring(14, 16), +timestamp.substring(17, 19), parseFloat(timestamp.substring(20)));
        const seconds = (now - time) * 0.001;
        if (seconds < 60) {
            return Math.floor(seconds) + " second" + ((seconds < 2) ? "" : "s");
        }
        const minutes = seconds / 60;
        if (minutes < 60) {
            return Math.floor(minutes) + " minute" + ((minutes < 2) ? "" : "s");
        }
        const hours = minutes / 60;
        if (hours < 24) {
            return Math.floor(hours) + " hour" + ((hours < 2) ? "" : "s");
        }
        const days = hours / 24;
        if (days < 7) {
            return Math.floor(days) + " day" + ((days < 2) ? "" : "s");
        }
        const weeks = days / 7;
        if (days < 365) {
            return Math.floor(weeks) + " week" + ((weeks < 2) ? "" : "s");
        }
        const years = days / 365;
        return Math.floor(years) + " year" + ((years < 2) ? "" : "s");
    },
    formatSeconds: (seconds: number) => {
        const formatTimePart = (timePart: number) => {
            const floor = Math.floor(timePart) % 60;
            return ((floor < 10) ? "0" : "") + floor.toString(); 
        };
        const minutes = seconds / 60;
        const hours = minutes / 60;
        return `${Math.floor(hours)}:${formatTimePart(minutes)}:${formatTimePart(seconds)}`;
    },
    formatTimeUntil: (time: Date, dateWhenPassed: boolean = false) => {
        const seconds = (time.getTime() - new Date().getTime()) * 0.001;
        if ((seconds > 60 * 60 * 24) || ((seconds < 0) && dateWhenPassed)) {
            return `on ${time.getDate()} ${MONTHS[time.getMonth()]}`;
        } else if (seconds > 0) {
            return `in ${Utils.formatSeconds(seconds)}`;
        } else {
            return "passed";
        }
    },
    removeExtension: (filename: string) => {
        const dotIndex = filename.lastIndexOf(".");
        if (dotIndex >= 0) {
            return filename.substring(0, dotIndex);
        }
        return filename;
    },
    assertUnreachable: (bad: never): never => {
        throw new Error("Unhandled case: " + bad);
    }
};