/**
 * Copyright 2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

type QuestType = "unknown" | "clearNewAreas" | "retrieveItem";

const enum QuestEvent {
    TAKEN = "taken",
    COMPLETED = "completed"
}

interface QuestArea {
    typeName: string;
    modifiers: AreaModifiers;
}

interface QuestRewardChances {
    loot?: LootChances;
    freeServices?: number;
    unlockServices?: string[];
}

interface QuestReward {
    loot?: Loot;
    freeServices?: number;
    unlockServices?: string[];
}

interface QuestRewardState {
    loot?: LootState;
    freeServices?: number;
    unlockServices?: string[];
}

interface QuestItemParams {
    name: string;
}

interface QuestParams {
    id: string;
    name: string;
    giver: string;
    introText: string,
    type: QuestType;
    areaParams: QuestArea[];
    itemToComplete?: QuestItemParams;
    completionText: string;
    reward: QuestRewardChances;
}

interface QuestState extends GameObjectState {
    name: string;
    giver: string;
    taken: boolean;
    completed: boolean;
    rewardTaken: boolean;
    introText: string;
    type: QuestType;
    areaParams: QuestArea[];
    itemToComplete?: QuestItemParams;
    areasCompleted: boolean[];
    completionText: string;
    reward: QuestRewardState;
}

class Quest extends GameObject {

    private _name: string;
    private _giver: NPC | null;
    private _taken: boolean;
    private _completed: boolean;
    private _rewardTaken: boolean;
    private _introText: string;
    private _type: QuestType;
    private _areaParams: QuestArea[];
    private _itemToComplete: QuestItemParams | null;
    private _areas: Area[];
    private _areasCompleted: boolean[];
    private _completionText: string;
    private _reward: QuestReward;

    constructor(params?: QuestParams) {
        super(params?.id ?? "Quest");
        this._name = params?.name ?? "";
        this._giver = null;
        this._taken = false;
        this._completed = false;
        this._rewardTaken = false;
        this._introText = params?.introText ?? "";
        this._type = params?.type ?? "unknown";
        this._areaParams = params?.areaParams ?? [];
        this._itemToComplete = params?.itemToComplete ?? null;
        this._areas = [];
        this._areasCompleted = [];
        this._completionText = params?.completionText ?? "";
        this._reward = {};
        if (params?.reward.loot) {
            this._reward.loot = Utils.generateLoot(params.reward.loot, 1) ?? undefined;
        }
        if (params?.reward.freeServices) {
            this._reward.freeServices = params.reward.freeServices;
        }
        if (params?.reward.unlockServices) {
            this._reward.unlockServices = params.reward.unlockServices.slice();
        }
        this._setGiver(params?.giver ? game.getNPC(params.giver) : null);
        game.player.addEventHandler(CharacterEvent.FIGHT_FINISHED, () => {
            this.update();
        });
        game.player.addEventHandler(CharacterEvent.QUEST_ITEM_ACQUIRED, () => {
            this.update();
        });
    }

    public _setGiver(giver: NPC | null): void {
        if (this._giver) {
            throw new Error("Attempting to set giver of a quest twice!");
        }
        if (giver) {
            giver.addQuest(this);
            giver.addEventHandler(NPCEvent.VISITED, () => {
                if (!this._taken) {
                    this.take();
                } else if (this._completed && !this._rewardTaken) {
                    this.complete();
                }
            });
            this._giver = giver;
        }
    }

    public getObjectType(): string {
        return "Quest";
    }

    public save(): QuestState {
        if (!this._giver) {
            throw new Error("Cannot save quest without a giver!");
        }
        const reward: QuestRewardState = {};
        if (this._reward.loot) {
            reward.loot = Utils.saveLoot(this._reward.loot);
        }
        if (this._reward.freeServices) {
            reward.freeServices = this._reward.freeServices;
        }
        if (this._reward.unlockServices) {
            reward.unlockServices = this._reward.unlockServices.slice();
        }
        const result: QuestState = {
            _id: super.save()._id,
            name: this._name,
            giver: this._giver.name,
            taken: this._taken,
            completed: this._completed,
            rewardTaken: this._rewardTaken,
            introText: this._introText,
            type: this._type,
            areaParams: this._areaParams,
            areasCompleted: this._areasCompleted,
            completionText: this._completionText,
            reward
        };
        if (this._itemToComplete) {
            result.itemToComplete = this._itemToComplete;
        }
        return result;
    }

    public load(state: QuestState): Quest {
        super.load(state);
        this._name = state.name ?? "";
        const giver = game.getNPC(state.giver);
        if (!giver) {
            throw new Error("Cannot load quest: NPC '" + state.giver + "' doesn't exist!");
        }
        this._taken = state.taken;
        this._completed = state.completed;
        this._rewardTaken = state.rewardTaken;
        this._introText = state.introText;
        this._type = state.type;
        this._areaParams = state.areaParams;
        this._itemToComplete = state.itemToComplete ?? null;
        this._areasCompleted = state.areasCompleted;
        this._completionText = state.completionText;
        this._reward = {};
        if (state.reward.loot) {
            this._reward.loot = Utils.loadLoot(state.reward.loot)
        }
        if (state.reward.freeServices) {
            this._reward.freeServices = state.reward.freeServices;
        }
        if (state.reward.unlockServices) {
            this._reward.unlockServices = state.reward.unlockServices.slice();
        }
        this._setGiver(giver);
        this._areas = [];
        if (this._taken) {
            game.forQuestAreas((area) => {
                if (area.questId === this._id) {
                    this._areas.push(area);
                }
            });
        }
        return this;
    }

    public get taken(): boolean {
        return this._taken;
    }

    public get giver(): NPC {
        if (!this._giver) {
            throw new Error("Quest is always supposed to have a giver!");
        }
        return this._giver;
    }

    public isOpen(): boolean {
        return !this._rewardTaken;
    }

    public hasUpdate(): boolean {
        return !this._taken || (this._completed && !this._rewardTaken);
    }

    public take() {
        game.showPopup(this._introText, "info quest", [{
            caption: "Start quest!",
            action: () => {}
        }]);
        this._taken = true;
        const area = game.addArea(this._areaParams[0].typeName, this._areaParams[0].modifiers, this);
        this._areas.push(area);
        this._areasCompleted.push(false);
        this.triggerEvent(QuestEvent.TAKEN);
    }

    public onItemCollected(item: QuestItem) {
        this.log("Quest item '" + item.name + "' was given to NPC!", 2);
    }

    public update(): void {
        if (!this._taken) {
            return;
        }
        let completed = true;
        this._areas.forEach((area, index) => {
            const areaCompleted = !area.hasEnemies();
            if (!this._areasCompleted[index] && areaCompleted) {
                this._areasCompleted[index] = true;
                game.player.onAreaQuestCompleted(area);
            }
            completed = completed && areaCompleted;
        });
        switch (this._type) {
            case "clearNewAreas":
                if (!this._completed && completed) {
                    game.showPopup(`You have completed a quest! Head back to the ${this._giver?.name} to collect your reward!`, "info quest");
                }
                this._completed = completed;
                break;
            case "retrieveItem":
                if (!this._completed) {
                    if (this._itemToComplete && game.player.hasQuestItem({name: this._itemToComplete.name, questId: this._id})) {
                        this._completed = true;
                        game.showPopup(`You have found the item necessary to complete a quest! Head back to the ${this._giver?.name} to collect your reward!`, "info quest");
                    }
                }
                break;
        }
    }

    public complete(): void {
        game.showPopup(this._completionText, "info quest", [{
            caption: "Get reward!",
            action: () => {
                if (this._reward.loot) {
                    game.showPopup(game.getLootMessage(this._reward.loot, "You have received "), "info bag");
                    game.takeLoot(this._reward.loot, () => {
                        game.renderShop();
                    }, true);
                }
                if (this._reward.freeServices) {
                    this._giver?.addFreeServices(this._reward.freeServices);
                }
                if (this._reward.unlockServices) {
                    for (const unlock of this._reward.unlockServices) {
                        this._giver?.unlockServices(unlock);
                    }
                }
                this._rewardTaken = true;
            }
        }]);
    }

}