/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface RoomParams extends RoomType {
    area: Area;
    parent: Room | null;
    pathDepth: number;
    pathProbabilities: readonly number[];
    boss: boolean;
}

interface RoomState {
    type: string;
    visited?: boolean;
    traversed?: boolean;
    hasHealing?: boolean;
    paths: RoomState[];
    enemy?: EnemyState;
    chest?: ChestState;
    enemyCount: number;
}

class Room {
    private _visited: boolean;
    private _traversed: boolean;
    private _area: Area | null;
    private _parent: Room | null;
    private _type: RoomParams | null;
    private _enemy: Enemy | null;
    private _chest: Chest | null;
    private _hasHealing: boolean;
    private _enemyCount: number;
    private _paths: Room[];

    constructor(params?: RoomParams, level: number = 0, modifiers?: AreaModifiers) {
        console.log("created Room instance");

        this._visited = false;
        this._traversed = false;

        let p: number; // used for probabilities

        this._area = null;
        this._parent = null;
        this._paths = [];

        this._type = params || null;

        this._enemy = null;
        this._chest = null;

        this._hasHealing = params ? Utils.chance(params.healing.chance) : false;

        this._enemyCount = 0;

        if (params) {
            this._area = params.area;
            this._parent = params.parent;

            p = Math.random();
            if (params.boss || (p <= (modifiers?.enemyChance ?? params.enemyChance))) {
                const enemyType = EnemyTypes[(params.boss && modifiers?.boss) ? modifiers.boss.typeName : Utils.chooseFromProbabilitiesObject(modifiers?.enemyTypeProbabilities ?? params.enemyTypeProbabilities)];
                this._enemy = new Enemy(enemyType, level, params.area, (params.boss && modifiers?.boss) ? modifiers.boss.modifiers : modifiers?.allEnemies, params.boss);
                this._enemyCount++;
                let room = params.parent;
                while (room) {
                    room._enemyCount++;
                    room = room._parent;
                }
            }

            let gold = 0;
            
            const lootChances = (params.boss && modifiers?.boss) ? modifiers.boss.loot : params.loot;

            if (lootChances) {
                const loot = Utils.generateLoot(lootChances, (modifiers ? modifiers.treasure || 1 : 1) * Utils.getLevelFactor(level));
                if (loot) {
                    this._chest = new Chest(loot);
                    gold = this._chest.gold;
                }
            }

            this._area.handleRoomAdded(gold);

            if (params.pathDepth > 0) {
                let n = Utils.chooseFromProbabilitiesArray(params.pathProbabilities) + 1;
                for (let i = 0; i < n; i++) {
                    const childParams: RoomParams = Object.assign({}, this._type, this._area.chooseRoomType(level));
                    childParams.parent = this;
                    childParams.pathDepth = params.pathDepth - 1;
                    this._paths.push(new Room(childParams, level, modifiers));
                }
            }
        }
    }

    public save(): RoomState {
        if (!this._type) {
            throw new Error("Cannot save room without a type!");
        }
        const type = {
            roomName: this._type.roomName,
            roomImage: this._type.roomImage,
            pathImage: this._type.pathImage,
            light: this._type.light,
            healing: this._hasHealing ? this._type.healing : undefined
        };
        return {
            type: JSON.stringify(type),
            visited: this._visited || undefined,
            traversed: this._traversed || undefined,
            hasHealing: this._hasHealing || undefined,
            paths: this._paths.map((path) => path.save()),
            enemy: this._enemy ? this._enemy.save() : undefined,
            chest: this._chest ? this._chest.save() : undefined,
            enemyCount: this._enemyCount
        };
    }

    public load(state: RoomState, area: Area, parent?: Room) {
        this._type = JSON.parse(state.type);
        this._visited = state.visited ?? false;
        this._traversed = state.traversed ?? false;
        this._hasHealing = state.hasHealing ?? false;
        this._area = area;
        this._parent = parent || null;
        this._paths = [];
        state.paths.forEach((path) => {
            this._paths.push(new Room().load(path, area, this));
        });
        this._enemy = state.enemy ? new Enemy(null, 0, area).load(state.enemy) : null;
        this._chest = state.chest ? new Chest().load(state.chest) : null;
        this._enemyCount = state.enemyCount;

        return this;
    }
    
    public get roomName(): string {
        return this._type?.roomName ?? "unknown room";
    }
    
    public get pathImage(): string {
        return this._type?.pathImage ?? "";
    }
    
    public get roomImage(): string {
        return this._type?.roomImage ?? "";
    }
    
    public get light(): boolean {
        return this._type?.light ?? false;
    }
    
    public get visited(): boolean {
        return this._visited;
    }
    public get traversed(): boolean {
        return this._traversed;
    }
    
    public get enemyCount(): number {
        return this._enemyCount;
    }

    public get hasEnemy(): boolean {
        return !!this._enemy && !this._enemy.dead;
    }
    
    public get enemy(): Enemy | null {
        return this._enemy;
    }
    public get chest(): Chest| null {
        return this._chest;
    }
    
    public get hasHealing(): boolean {
        return this._hasHealing;
    }
    
    public get healing(): NaturalHealing | null {
        return this._hasHealing ? this._type?.healing ?? null : null;
    }
    
    public clearHealing(): void {
        this._hasHealing = false;
    }
    
    public get pathCount(): number {
        return this._paths.length;
    }
    
    public addPath(room: Room): void {
        this._paths.push(room);
    }
    
    public getPath(index: number): Room {
        return this._paths[index];
    }

    public get hasTreasure(): boolean {
        return !!this._chest && !this._chest.empty;
    }
    
    public get parent(): Room | null {
        return this._parent;
    }
    
    public get area(): Area | null {
        return this._area;
    }
    
    public takeTreasure(callback: () => void): void {
        this._chest?.take(callback);
    }

    public visit(): void {
        this._visited = true;
    }

    public leave(): void {
        if (this._enemy && this._enemy.dead) {
            this._enemy = null;
        }
        if (this._chest && this._chest.empty && this._chest.goldOnly) {
            this._chest = null;
        }
        let i = 0;
        while ((i < this._paths.length) && (this._paths[i]._traversed)) {
            i++;
        }
        this._traversed = (i === this._paths.length) && !this.hasEnemy && !this.hasTreasure;
    }

    public handlePlayerMoved(): void {
        if (this._enemy && !this._enemy.dead) {
            this._enemy.regenerate();
        }
        for (let i = 0; i < this._paths.length; i++) {
            this._paths[i].handlePlayerMoved();
        }
    }
    
    public traverse(callback: (room: Room) => void): void {
        callback(this);
        for (const path of this._paths) {
            path.traverse(callback);
        }
    }
}