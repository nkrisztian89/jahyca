/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface ChestState {
    loot?: LootState;
}

class Chest {
    private _loot: Loot | null;
    private _gold: number;
    private _goldOnly: boolean;
    
    constructor(loot?: Loot) {
        console.log("created Chest instance");
        
        this._loot = loot || null;

        this._gold = 0;
        this._goldOnly = false;
        this._update();
        
        return this;
    }
    
    private _update(): void {
        this._gold = this._loot ? this._loot.gold || 0 : 0;
        this._goldOnly = this._loot ? (Utils.valueCount(this._loot) === 1) && ((this._loot.gold || 0) > 0) : false;
    }
    
    public save(): ChestState {
        return {
            loot: this._loot ? Utils.saveLoot(this._loot) : undefined
        }
    }
    
    public load(state: ChestState): Chest {
        this._loot = null;
        if (state.loot && (Utils.valueCount(state.loot) > 0)) {
            this._loot = Utils.loadLoot(state.loot);
        }
        this._update();
        
        return this;
    }
    
    public get empty(): boolean {
        return !this._loot;
    }
    
    public get gold(): number {
        return this._gold;
    }
    
    public get goldOnly(): boolean {
        return this._goldOnly;
    }
    
    public take(callback: () => void): void {
        if (!this._loot) {
            return;
        }
        if (!this._goldOnly) {
            game.showPopup(game.getLootMessage(this._loot), "info bag");
        }
        game.takeLoot(this._loot, () => {
            this._loot = null;
            callback();
        });
    }
}