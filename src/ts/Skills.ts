/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface SkillType extends NamedObject {
    image: string;
    weapon?: WeaponCategory;
    damage: DamageFactor;
    targetEffect?: Effect;
    selfEffect?: Effect;
    afterEffect?: Effect;
}

type SkillTypeMap = NamedObjectMap<SkillType>;

const Skills: SkillTypeMap = {
    CRIPPLING_CUT: {
        name: "Crippling cut",
        image: "crippling-cut.png",
        weapon: "sword",
        damage: {
            physical: 0.5,
            magic: 0.5
        },
        targetEffect: {
            type: EffectType.REDUCE_DAMAGE,
            chance: 0.5,
            value: {
                physical: 0.25
            }
        }
    },
    STUNNING_BLOW: {
        name: "Stunning blow",
        image: "stunning-blow.png",
        weapon: "axe",
        damage: {
            physical: 0.5,
            magic: 0.5
        },
        targetEffect: {
            type: EffectType.STUN,
            chance: 0.5,
            rounds: [2, 3]
        }
    },
    LEECH_LIFE: {
        name: "Leech life",
        image: "leech-life.png",
        damage: {
            physical: 0.5
        },
        selfEffect: {
            chance: 1.0,
            type: EffectType.LEECH_LIFE,
            value: {
                magic: 0.75
            }
        },
        afterEffect: {
            type: EffectType.MULTIPLY_DAMAGE,
            rounds: 1,
            value: {
                magic: 0
            }
        }
    },
    ARCANE_BLAST: {
        name: "Arcane blast",
        image: "arcane-blast.png",
        damage: {
            physical: 0,
            magic: 3.0
        },
        afterEffect: {
            type: EffectType.MULTIPLY_DAMAGE,
            rounds: 1,
            value: {
                magic: 0
            }
        }
    }
};