/**
 * Copyright 2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface LoginResponse {
    newlyCreated: boolean;
    id: number;
    userID: string;
    season: number;
}

interface HeroCreateResponse {
    newlyCreated: boolean;
    id: number;
    heroID: string;
    season: number;
}

interface HeroIdMap {
    [heroId: string]: number;
}

interface QueueItem {
    request: XMLHttpRequest;
    body: string | null;
}

interface AreaStats {
    discovered: number;
    scouted: number;
    quest: number;
    completed: number;
    quest_completed: number;
    highest_completed: number;
    kills: number;
}

interface AreaStatsMap {
    [areaTypeName: string]: AreaStats;
}

interface HeroStats {
    money: number;
    money_collected: number;
    areas: AreaStatsMap;
}

interface HeroEditData {
    name?: string;
    portrait?: string;
}

interface LeaderboardEntry {
    id: number;
    hero_id: string;
    hero_name: string;
    hero_portrait: string;
    frames: string;
    score: number;
    money_collected: number;
    kills: number;
    areas_completed: number;
    last_login: string;
    own_hero?: boolean;
}

interface LeaderboardResponse {
    season: number;
    seasonEnd: string;
    results: LeaderboardEntry[];
}

class Analytics {

    private _enabled: boolean;
    private _debug: boolean;
    private _baseUrl: string;
    private _version: string;
    private _queue: QueueItem[];

    private _id: number;
    private _userId: string;

    private _heroIds: HeroIdMap;
    private _heroSeasons: HeroIdMap;

    private _loginSent: boolean;
    
    private _season: number;

    constructor(baseUrl: string, version: string, debug: boolean = false) {

        this._enabled = true;
        this._debug = debug;
        this._baseUrl = baseUrl;
        this._version = version;
        this._queue = [];

        this._id = localStorage["jahyca_analytics_id"];
        this._userId = localStorage["jahyca_analytics_userId"];
        this._heroIds = localStorage["jahyca_analytics_heroIds"] ? JSON.parse(localStorage["jahyca_analytics_heroIds"]) : {};
        this._heroSeasons = localStorage["jahyca_analytics_heroSeasons"] ? JSON.parse(localStorage["jahyca_analytics_heroSeasons"]) : {};
        
        for (const heroId of Object.keys(this._heroIds)) {
            if (!this._heroSeasons[heroId]) {
                this._heroSeasons[heroId] = 1;
            }
        }

        this._loginSent = false;
        
        this._season = 0;

    }

    private _log(message: string): void {
        if (this._debug) {
            console.log(message);
        }
    }

    private _processQueue(): void {
        const item = this._queue.shift();
        if (item) {
            const isGet = item.body === "GET";
            if (!isGet && item.body) {
                item.request.setRequestHeader("Content-Type", "application/json");
            }
            item.request.send(isGet ? null : item.body || null);
        }
    }

    private _queueRequest(path: string, body: string | null, retry: number, independent: boolean, onfinish: (request: XMLHttpRequest) => void, onerror?: (request: XMLHttpRequest) => boolean): void {
        const request = new XMLHttpRequest();
        const handleError = (message: string) => {
            this._log(message);
            if (onerror) {
                if (!onerror(request)) {
                    retry = 0;
                }
            }
            if (retry > 0) {
                this._log("Will resend in " + Math.round(retry * 0.001) + " seconds.");
                setTimeout(() => {
                    this._log("Resending failed request...");
                    this._queueRequest(path, body, retry * 2, true, (request) => {
                        onfinish(request);
                        if (!independent) {
                            this._processQueue();
                        }
                    }, onerror);
                }, retry);
            } else {
                if (!independent) {
                    this._processQueue();
                }
            }
        };
        if (!this._enabled) {
            return;
        }
        request.onload = () => {
            this._log("Analytics request: '" + path + "' successfully completed.");
            onfinish(request);
            this._processQueue();
        };
        request.onerror = () => {
            handleError("ERROR: An error occured during analytics request: '" + path + "'. The status of the request was: '" + request.statusText + "' when the error happened.");
        };
        request.ontimeout = () => {
            handleError("Analytics request : '" + path + "' timed out.");
        };
        request.overrideMimeType("text/plain; charset=utf-8");
        const isGet = body === "GET";
        request.open(isGet ? "GET" : "POST", this._baseUrl + path, true);
        if (independent) {
            request.send(isGet ? null : body);
        } else {
            this._queue.push({request, body});
            if (this._queue.length === 1) {
                this._processQueue();
            }
        }
    }

    public login(): void {
        const newUser = !(this._id && this._userId);
        if (this._loginSent) {
            return;
        }
        this._loginSent = true;
        this._queueRequest("start" + (newUser ? "" : ("/" + this._id + "/" + this._userId)) + "?version=" + this._version, null, 1000, false, (request) => {
            if (request) {
                const data = JSON.parse(request.responseText) as LoginResponse;
                if (data.newlyCreated && data.id && data.userID) {
                    this._id = data.id;
                    this._userId = data.userID;
                    localStorage["jahyca_analytics_id"] = this._id;
                    localStorage["jahyca_analytics_userId"] = this._userId;
                    this._log("Successfully created new user '" + this._id + "/" + this._userId + "' for analytics.");
                } else {
                    this._log("Successfully logged user '" + this._id + "/" + this._userId + "' for analytics.");
                }
                if (data.season) {
                    this._season = data.season;
                    localStorage["jahyca_analytics_season"] = this._season;
                }
            }
        });
    }

    public createHero(name: string, callback: (id: string) => void, onError: (request: XMLHttpRequest) => boolean): void {
        this._queueRequest("create/" + this._id + "/" + this._userId + "/" + name + "?version=" + this._version, null, 0, false, (request) => {
            if (request) {
                const data = JSON.parse(request.responseText) as HeroCreateResponse;
                if (data.newlyCreated && data.id && data.heroID) {
                    this._heroIds[data.heroID] = data.id;
                    localStorage["jahyca_analytics_heroIds"] = JSON.stringify(this._heroIds);
                    this._heroSeasons[data.heroID] = data.season;
                    localStorage["jahyca_analytics_heroSeasons"] = JSON.stringify(this._heroSeasons);
                    this._log("Successfully created new hero '" + name + "' (" + data.id + "/" + data.heroID + ") for analytics.");
                    callback(data.heroID);
                } else {
                    this._log("There was a problem creating hero '" + name + "' for analytics.");
                    this._log(request.responseText);
                }
            }
        }, (request) => {
            this._log("There was a problem creating hero '" + name + "' for analytics.");
            return onError(request);
        });
    }

    public loadHero(heroId: string): void {
        if (this._heroIds[heroId] !== undefined) {
            this._queueRequest("load/" + this._heroIds[heroId] + "/" + this._userId + "/" + heroId + "?version=" + this._version, null, 1000, false, (request) => {
                if (request) {
                    this._log("Successfully logged loading of hero '" + heroId + "' for analytics.");
                }
            });
        }
    }

    public updateHero(heroId: string, data: HeroStats, callback?: () => void, onError?: (request: XMLHttpRequest) => boolean): void {
        if ((this._heroIds[heroId] !== undefined) && (this._heroSeasons[heroId] === this._season)) {
            this._queueRequest("update/" + this._heroIds[heroId] + "/" + this._userId + "/" + heroId, JSON.stringify(data), 1000, false, (request) => {
                if (request) {
                    this._log("Successfully updated hero '" + heroId + "' for analytics.");
                    if (callback) {
                        callback();
                    }
                }
            }, onError);
        }
    }

    public getLeaderboard(callback: (leaderboard: LeaderboardEntry[], season: number, seasonEnd: string) => void, onError?: (request: XMLHttpRequest) => boolean): void {
        this._queueRequest("leaderboard/" + this._id + "/" + this._userId, "GET", 0, false, (request) => {
            if (request) {
                const data = JSON.parse(request.responseText) as LeaderboardResponse;
                this._log("Successfully queried the leaderboard.");
                for (const entry of data.results) {
                    if (this._heroIds[entry.hero_id] === entry.id) {
                        entry.own_hero = true;
                    }
                }
                callback(data.results, data.season, data.seasonEnd);
            }
        }, onError);
    }
    
    public getLeaderboardArchive(season: number, callback: (leaderboard: LeaderboardEntry[], season: number, seasonEnd: string) => void, onError?: (request: XMLHttpRequest) => boolean): void {
        this._queueRequest("leaderboard/season/" + season + "/" + this._id + "/" + this._userId, "GET", 0, false, (request) => {
            if (request) {
                const data = JSON.parse(request.responseText) as LeaderboardResponse;
                this._log("Successfully queried the leaderboard.");
                for (const entry of data.results) {
                    if (this._heroIds[entry.hero_id] === entry.id) {
                        entry.own_hero = true;
                    }
                }
                callback(data.results, data.season, data.seasonEnd);
            }
        }, onError);
    }
    
    public editHero(heroId: string, data: HeroEditData): void {
        if (Object.keys(data).length <= 0) {
            return;
        }
        if (this._heroIds[heroId] !== undefined) {
            this._queueRequest("edit/" + this._heroIds[heroId] + "/" + this._userId + "/" + heroId, JSON.stringify(data), 1000, false, (request) => {
                if (request) {
                    this._log("Successfully edited hero '" + heroId + "'");
                }
            });
        }
    }
    
    public isHeroFromPreviousSeason(heroId: string): boolean {
        return (this._season > 0) && (this._heroSeasons[heroId] < this._season);
    }

};