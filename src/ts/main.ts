declare const version: string;

const DEBUG_LOG_LEVEL = 0;

let game: Game;
try {
    game = new Game(version);
    game.showMainMenu(false, false);
} catch(error) {
    alert("Sorry, the game encountered an error!\n\n" + error.message);
}
