/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface AttackEffect {
    image: string;
    animations: string[];
}

const PUNCH_ATTACK_EFFECT: AttackEffect = {
    image: "img/fight-effects/punch.png",
    animations: ["expand0", "expand1", "expand2"]
}

interface WeaponType extends NamedObject {
    category: WeaponCategory;
    image: string;
    attackEffect: AttackEffect;
    damage: [number, number];
}

type WeaponTypeMap = NamedObjectMap<WeaponType>;

const WeaponTypes: WeaponTypeMap = {
    SHAMAN_STAFF: {
        name: "Shaman staff",
        category: "staff",
        image: "staff-1.png",
        attackEffect: {
            image: "slash-staff.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [4, 7]
    },
    APPRENTICE_STAFF: {
        name: "Apprentice's staff",
        category: "staff",
        image: "staff-1.png",
        attackEffect: {
            image: "slash-staff.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [6, 8]
    },
    MAGE_STAFF: {
        name: "Mage's staff",
        category: "staff",
        image: "staff-2.png",
        attackEffect: {
            image: "slash-staff.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [9, 10]
    },
    RUBY_STAFF: {
        name: "Ruby staff",
        category: "staff",
        image: "staff-2.png",
        attackEffect: {
            image: "slash-staff.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [9, 12]
    },
    ARCHMAGE_STAFF: {
        name: "Archmage's staff",
        category: "staff",
        image: "staff-3.png",
        attackEffect: {
            image: "slash-staff.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [11, 12]
    },
    RUSTY_SWORD: {
        name: "Rusty sword",
        category: "sword",
        image: "sword-1.png",
        attackEffect: {
            image: "slash-sword.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [3, 6]
    },
    ARMING_SWORD: {
        name: "Arming sword",
        category: "sword",
        image: "sword-1.png",
        attackEffect: {
            image: "slash-sword.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [5, 10]
    },
    FINE_SWORD: {
        name: "Fine sword",
        category: "sword",
        image: "sword-2.png",
        attackEffect: {
            image: "slash-sword.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [8, 12]
    },
    MASTERPIECE_SWORD: {
        name: "Masterpiece sword",
        category: "sword",
        image: "sword-2.png",
        attackEffect: {
            image: "slash-sword.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [10, 15]
    },
    FLAMING_SWORD: {
        name: "Flaming sword",
        category: "sword",
        image: "sword-flaming.png",
        attackEffect: {
            image: "slash-sword-flaming.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [6, 16]
    },
    TEMPLAR_SWORD: {
        name: "Templar sword",
        category: "sword",
        image: "sword-3.png",
        attackEffect: {
            image: "slash-sword.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [12, 18]
    },
    VON_TIQULUS_SWORD: {
        name: "Von Tiqulus' sword",
        category: "sword",
        image: "sword-flaming.png",
        attackEffect: {
            image: "slash-sword-flaming.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [12, 20]
    },
    HAND_AXE: {
        name: "Hand axe",
        category: "axe",
        image: "hand-axe.png",
        attackEffect: {
            image: "slash-axe.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [2, 8]
    },
    BATTLE_AXE: {
        name: "Battle axe",
        category: "axe",
        image: "battle-axe.png",
        attackEffect: {
            image: "slash-axe.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [6, 14]
    },
    LARGE_AXE: {
        name: "Large axe",
        category: "axe",
        image: "large-axe.png",
        attackEffect: {
            image: "slash-axe.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [2, 20]
    },
    VIKING_AXE: {
        name: "Viking axe",
        category: "axe",
        image: "battle-axe.png",
        attackEffect: {
            image: "slash-axe.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [4, 22]
    },
    BERSERKER_AXE: {
        name: "Berserker axe",
        category: "axe",
        image: "battle-axe.png",
        attackEffect: {
            image: "slash-axe.png",
            animations: ["slide30", "slide45", "slide60", "slide0", "slide45left"]
        },
        damage: [5, 30]
    }
};