/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

const BASE_MAX_BLESSING_DURATION = 5;
const BASE_MAX_POTION_PER_TYPE = 5;

const DEFAULT_AREA_STATS: AreaStats = {
    discovered: 0,
    scouted: 0,
    quest: 0,
    completed: 0,
    quest_completed: 0,
    highest_completed: 0,
    kills: 0
};

const getDefaultAreaStats: () => AreaStatsMap = () => {
    return {
        forest: Object.assign({}, DEFAULT_AREA_STATS),
        mountains: Object.assign({}, DEFAULT_AREA_STATS),
        riverland: Object.assign({}, DEFAULT_AREA_STATS),
        fields: Object.assign({}, DEFAULT_AREA_STATS),
        desert: Object.assign({}, DEFAULT_AREA_STATS),
        swamp: Object.assign({}, DEFAULT_AREA_STATS),
        caves: Object.assign({}, DEFAULT_AREA_STATS),
        seaside: Object.assign({}, DEFAULT_AREA_STATS),
        castle: Object.assign({}, DEFAULT_AREA_STATS)
    };
};

interface CharacterParams {
    name: string;
    analyticsId: string;
    hp: number;
    weapon?: Weapon;
    gold: number;
}

type WeaponCategoryDamageFactors = {
    [P in WeaponCategory]?: DamageFactor;
}

interface CharacterState extends GameObjectState {
    name: string;
    _analyticsId?: string;
    _baseMaxHP: number;
    hp: number;
    _unarmedDamage: FixedNumberRange;
    weapon: WeaponState | null;
    backupWeapon: WeaponState | null;
    gold: number;
    activeEffects: Effect[];
    potions: Potions[];
    skills: string[];
    npcImprovementItems: NPCImprovementItem[];
    questItems: QuestItem[];
    _stats: HeroStats;
}

const enum CharacterEvent {
    MOVED = "moved",
    HP_CHANGED = "hpChanged",
    FIGHT_FINISHED = "fightFinished",
    QUEST_ITEM_ACQUIRED = "questItemAcquired",
    WEAPONS_SWAPPED = "weaponsSwapped"
}

interface EffectOptions {
    damage?: FullDamage;
}

class Character extends GameObject {
    private _name: string;
    private _baseMaxHP: number;
    private _maxHP: number;
    private _hp: number;
    private _maxBlessingDuration: number;
    private _maxPotionPerType: number;
    private _unarmedDamage: FixedNumberRange;
    private _bonusDamage: FullDamageRange;
    private _damage: NumberRange;
    private _physicalDamage: NumberRange;
    private _magicDamage: NumberRange;
    private _damageModifier: FullDamageFactor;
    private _weapon: Weapon | null;
    private _weaponDamage: WeaponCategoryDamageFactors;
    private _backupWeapon: Weapon | null;
    private _gold: number;
    private _safeGold: number;
    private _room: Room | null;
    private _npc: NPC | null;
    private _activeEffects: Effect[];
    private _potions: Potions[];
    private _skills: SkillType[];
    private _npcImprovementItems: NPCImprovementItem[];
    private _questItems: QuestItem[];
    private _tracking: number;
    private _potionUsed: boolean;
    private _naturalHealFactor: number;
    private _enemy: Enemy | null;
    private _analyticsId: string;
    private _stats: HeroStats;

    constructor(params?: CharacterParams) {
        super("Character");

        this.log("created Character instance", 2);

        this._name = params?.name ?? "Character";

        this._baseMaxHP = params?.hp ?? 100;
        this._maxHP = this._baseMaxHP;
        this._hp = this._maxHP;

        this._maxBlessingDuration = BASE_MAX_BLESSING_DURATION;
        this._maxPotionPerType = BASE_MAX_POTION_PER_TYPE;

        this._unarmedDamage = [2, 3];

        this._bonusDamage = {
            physical: [0, 0],
            magic: [0, 0]
        };

        this._damage = [0, 0];
        this._physicalDamage = [0, 0];
        this._magicDamage = [0, 0];

        this._damageModifier = {
            physical: 1,
            magic: 1
        };

        this._weapon = params?.weapon ?? null;
        this._backupWeapon = null;

        this._weaponDamage = {};

        this._gold = params?.gold ?? 0;
        this._safeGold = this._gold;
        this._room = null;
        this._npc = null;
        this._activeEffects = [];
        this._potions = [];
        this._skills = [];
        this._npcImprovementItems = [];
        this._questItems = [];
        this._tracking = 0;
        this._potionUsed = false;
        this._naturalHealFactor = 1;
        this._enemy = null;
        this._analyticsId = params?.analyticsId ?? "";
        this._stats = {
            money: this._gold,
            money_collected: this._gold,
            areas: getDefaultAreaStats()
        };
    }

    public getObjectType(): string {
        return "Character";
    }

    public save(): CharacterState {
        // make sure HP is saved correctly
        const activeEffects = this._activeEffects;
        this._activeEffects = [];
        this._updateEffects();
        const hp = this._hp;
        this._activeEffects = activeEffects;
        this._updateEffects();
        return {
            _id: super.save()._id,
            name: this._name,
            _analyticsId: this._analyticsId,
            _baseMaxHP: this._baseMaxHP,
            hp: hp,
            _unarmedDamage: this._unarmedDamage,
            weapon: this._weapon ? this._weapon.save() : null,
            backupWeapon: this._backupWeapon ? this._backupWeapon.save() : null,
            gold: this._gold,
            activeEffects: this._activeEffects,
            potions: this._potions,
            skills: this._skills.map((skill) => skill.name),
            npcImprovementItems: this._npcImprovementItems,
            questItems: this._questItems,
            _stats: this._stats
        };
    }

    public load(state: CharacterState): Character {
        super.load(state);

        this._name = state.name;
        this._analyticsId = state._analyticsId ?? "";
        this._baseMaxHP = state._baseMaxHP;
        this._hp = state.hp;
        this._unarmedDamage = state._unarmedDamage;
        this._weapon = state.weapon ? new Weapon().load(state.weapon) : null;
        this._backupWeapon = state.backupWeapon ? new Weapon().load(state.backupWeapon) : null;
        this._gold = state.gold;
        this._safeGold = this._gold;
        this._activeEffects = state.activeEffects || [];

        this._potions = state.potions || [];

        this._skills = Utils.mapByNames(state.skills, Skills);

        this._npcImprovementItems = state.npcImprovementItems || [];
        this._questItems = state.questItems || [];

        this._stats = {
            money: this._gold,
            money_collected: this._gold,
            areas: getDefaultAreaStats()
        };

        if (state._stats) {
            Utils.deepCopyInto(this._stats, state._stats);
        }

        this._room = null;
        this._npc = null;

        this._enemy = null;

        this._updateEffects();

        return this;
    }

    public addToAnalytics(analyticsId: string): void {
        if (this._analyticsId) {
            this.warn(`Character '${this._name}' already has an analytics ID (${this._analyticsId})! Overwriting...`, 0);
        }
        this._analyticsId = analyticsId;
    }

    public get analyticsId(): string {
        return this._analyticsId;
    }

    public get name(): string {
        return this._name;
    }

    public get hp(): number {
        return this._hp;
    }

    public get maxHP(): number {
        return this._maxHP;
    }

    public get gold(): number {
        return this._gold;
    }

    public get safeGold(): number {
        return this._safeGold;
    }

    public get npc(): NPC | null {
        return this._npc;
    }

    public get weapon(): Weapon | null {
        return this._weapon;
    }
    
    public get backupWeapon(): Weapon | null {
        return this._backupWeapon;
    }
    
    public swapWeapons(): void {
        if (this._weapon && this._backupWeapon && !this.fighting) {
            const weapon = this._weapon;
            this._weapon = this._backupWeapon;
            this._backupWeapon = weapon;
            this.triggerEvent(CharacterEvent.WEAPONS_SWAPPED);
        }
    }

    public get dead(): boolean {
        return this._hp <= 0;
    }

    public get room(): Room | null {
        return this._room;
    }

    public get enemy(): Enemy | null {
        return this._room?.enemy ?? null;
    }

    public get area(): Area | null {
        return this._room?.area ?? null;
    }

    public get physicalDamage(): FixedNumberRange {
        const factor: number = (this._weapon && this._weaponDamage[this._weapon.category]?.physical) ?? 1;
        this._physicalDamage[0] = Math.round((this._weapon ? (this._weapon.physicalDamage[0] * factor) : this._unarmedDamage[0]) + this._bonusDamage.physical[0]);
        this._physicalDamage[1] = Math.round((this._weapon ? (this._weapon.physicalDamage[1] * factor) : this._unarmedDamage[1]) + this._bonusDamage.physical[1]);
        if (this._damageModifier) {
            if (this._damageModifier.physical !== undefined) {
                this._physicalDamage[0] *= this._damageModifier.physical;
                this._physicalDamage[1] *= this._damageModifier.physical;
            }
        }
        return this._physicalDamage;
    }

    public get magicDamage(): FixedNumberRange {
        const factor: number = (this._weapon && this._weaponDamage[this._weapon.category]?.magic) ?? 1;
        this._magicDamage[0] = Math.round((this._weapon ? (this._weapon.magicDamage[0] * factor) : 0) + this._bonusDamage.magic[0]);
        this._magicDamage[1] = Math.round((this._weapon ? (this._weapon.magicDamage[1] * factor) : 0) + this._bonusDamage.magic[1]);
        if (this._damageModifier) {
            if (this._damageModifier.magic !== undefined) {
                this._magicDamage[0] *= this._damageModifier.magic;
                this._magicDamage[1] *= this._damageModifier.magic;
            }
        }
        return this._magicDamage;
    }

    public get damage(): FixedNumberRange {
        this._damage[0] = this.physicalDamage[0] + this.magicDamage[0];
        this._damage[1] = this.physicalDamage[1] + this.magicDamage[1];
        return this._damage;
    }

    public hasMagicDamage(): boolean {
        return (this.magicDamage[0] > 0) || (this.magicDamage[1] > 0);
    }

    public get fighting(): boolean {
        return !!this._enemy;
    }

    public get tracking(): number {
        return this._tracking;
    }

    public visitNPC(npc: NPC): void {
        this.log("Character visits NPC", 1);
        for (let i = 0; i < this._npcImprovementItems.length; i++) {
            if (npc.canApplyImprovementItem(this._npcImprovementItems[i])) {
                npc.applyImprovementItem(this._npcImprovementItems[i]);
                this._npcImprovementItems.splice(i, 1);
                i--;
            }
        }
        for (let i = 0; i < this._questItems.length; i++) {
            if (npc.canGiveQuestItem(this._questItems[i])) {
                npc.giveQuestItem(this._questItems[i]);
                this._questItems.splice(i, 1);
                i--;
            }
        }
        npc.visit();
        this._npc = npc;
    }

    public canUseService(service: NPCService): boolean {
        return (
            (("effect" in service) && this.canApplyEffect(service.effect)) ||
            (("items" in service) && this._canTakeLoot(NPC.getItems(service.items))) ||
            ("discover" in service) ||
            (("skill" in service) && this.canLearnSkill(service.skill)));
    }

    public canBuyService(service: NPCService, npc: NPC): boolean {
        return (npc.hasFreeServices() || (this._gold >= service.cost)) && this.canUseService(service);
    }

    public buyService(service: NPCService, npc: NPC, callback: () => void): void {
        const cost = npc.hasFreeServices() ? 0 : service.cost;
        if ("effect" in service) {
            this._gold -= cost;
            npc.buyService();
            this.addEffect(service.effect);
            callback();
        }
        if ("items" in service) {
            game.takeLoot(NPC.getItems(service.items), (weaponSwitchResponse) => {
                if (weaponSwitchResponse !== "drop") {
                    this._gold -= cost;
                    npc.buyService();
                }
                callback();
            }, true);
        }
        if ("discover" in service) {
            this._gold -= cost;
            npc.buyService();
            let areaType = Utils.chooseRandom(service.discover.areas);
            game.addArea(areaType.name, service.discover.modifiers);
            callback();
        }
        if ("skill" in service) {
            this._gold -= cost;
            npc.buyService();
            this.learnSkill(service.skill);
            callback();
        }
    }

    public get blessings(): BlessingEffect[] {
        return getBlessings(this._activeEffects);
    }

    public get passiveSkills(): PassiveSkillEffect[] {
        return getPassiveSkills(this._activeEffects);
    }

    public get combatEffects(): CombatEffect[] {
        return getCombatEffects(this._activeEffects);
    }

    private _finishFight(): void {
        if (this._enemy) {
            this._enemy.finishFight();
            this._enemy = null;
            this.blessings.forEach((blessing) => blessing.battles--);
            this._activeEffects = this._activeEffects.filter((effect) => !isCombatEffect(effect) && (!isBlessingEffect(effect) || (effect.battles !== 0)));
            this._updateEffects();
            this._potionUsed = false;
            this.triggerEvent(CharacterEvent.FIGHT_FINISHED);
        }
    }

    private _leaveRoom(): void {
        if (this._room) {
            this._room.leave();
            this._finishFight();
        }
        this.triggerEvent(CharacterEvent.MOVED);
    }

    public leaveTown(): void {
        this._safeGold = this._gold;
    }

    public enterRoom(room: Room): void {
        this.log("Character enters room", 1);
        this._leaveRoom();
        room.visit();
        this._room = room;
    }

    public goToParentRoom(): void {
        if (!this._room?.parent) {
            throw new Error("Cannot go to parent of first room!");
        }
        this.enterRoom(this._room.parent)
    }

    public enterArea(area: Area): void {
        this.log("Character enters area", 1);
        this.enterRoom(area.visit());
    }

    public exitArea(): void {
        this._leaveRoom();
        this._room = null;
    }

    public getDamage(factor?: DamageFactor): FullDamage {
        return Utils.getDamage({
            physical: this.physicalDamage,
            magic: this.magicDamage
        }, factor);
    }

    public die(): void {
        this._hp = 0;
        this.log("Character life dropped to zero", 1);
    }

    public hasPotions(): boolean {
        return this._potions.length > 0;
    }

    public getPotionAmount(): number {
        let result = 0;
        for (let i = 0; i < this._potions.length; i++) {
            result += this._potions[i].amount;
        }
        return result;
    }

    public get potions(): Potions[] {
        return this._potions;
    }

    public revive(): void {
        this.exitArea();
        this._gold = this._safeGold;
        this._weapon = null;
        this._backupWeapon = null;
        this._activeEffects = this._activeEffects.filter(isPassiveSkillEffect);
        this._updateEffects();
        this._potions = [];
        this._hp = Math.round(this._maxHP * 0.5);
    }

    public hit(damage: FullDamage): void {
        this.log("Character got hit for " + damage.physical + " physical and " + damage.magic + " magic damage", 1);
        this._hp -= damage.physical;
        this._hp -= damage.magic;
        this.triggerEvent(CharacterEvent.HP_CHANGED);
        if (this.dead) {
            this.die();
        }
    }

    public attack(skill: SkillType | null): void {
        this.log("Character attacks enemy", 1);
        if (!this._room) {
            throw new Error("Character cannot attack while not in a room!");
        }
        this._enemy = this._room.enemy;
        if (!this._enemy) {
            throw new Error("Character cannot attack if there is no enemy in the room!");
        }
        const damage = this.getDamage(skill ? skill.damage : undefined);
        this._enemy.hit(damage);
        const targetEffect = !!skill && !!skill.targetEffect && Utils.chance(getEffectChance(skill.targetEffect)) ? skill.targetEffect : null;
        if (targetEffect) {
            this._enemy.applyEffect(targetEffect);
        }
        this._enemy.attack(this);
        const selfEffect = !!skill && !!skill.selfEffect && Utils.chance(getEffectChance(skill.selfEffect)) ? skill.selfEffect : null;
        if (selfEffect) {
            this.addEffect(selfEffect, {damage: damage});
        }
        this.combatEffects.forEach((effect) => effect.rounds--);
        this._activeEffects = this._activeEffects.filter((effect) => !isCombatEffect(effect) || (effect.rounds !== 0));
        this._updateEffects();
        if (skill && skill.afterEffect) {
            this.addEffect(skill.afterEffect);
        }
        this._potionUsed = false;
        if (this._enemy.dead) {
            this._finishFight();
            if (this._room.area) {
                const key = Utils.findKey(this._room.area.typeName, AreaTypes);
                if (key) {
                    this._stats.areas[key.toLowerCase()].kills++;
                }
            }
        }
    }

    public canApplyEffect(effect: Effect): boolean {
        if (isCombatEffect(effect)) {
            if (!this._room || !this._room.enemy || this._room.enemy.dead) {
                return false;
            }
        }
        const existingEffect = this.getEffect(effect);
        if (existingEffect) {
            if (isBlessingEffect(effect) && isBlessingEffect(existingEffect) && (effect.battles) && (effect.battles > existingEffect.battles)) {
                return true;
            }
            if (isCombatEffect(effect) && isCombatEffect(existingEffect) && (effect.rounds) && (effect.rounds > existingEffect.rounds)) {
                return true;
            }
            return isEffectStronger(effect, existingEffect);
        } else {
            switch (effect.type) {
                case EffectType.HEAL:
                    return (this._hp < this._maxHP);
                case EffectType.INCREASE_LIFE:
                    return true;
                case EffectType.INCREASE_DAMAGE:
                    return (!!effect.value.physical && (this._damageModifier.physical > 0)) || (!!effect.value.magic && (this._damageModifier.magic > 0));
                case EffectType.INCREASE_BLESSING_DURATION:
                    const blessings = this.blessings;
                    if (blessings.length > 0) {
                        for (let i = 0; i < blessings.length; i++) {
                            if (blessings[i].battles < this._maxBlessingDuration) {
                                return true;
                            }
                        }
                    }
                    return false;
                case EffectType.WEAPON_QUALITY:
                    return !!this._weapon && (!effect.category || (effect.category === this._weapon.category)) && (this._weapon.quality < effect.value);
                case EffectType.WEAPON_ENCHANT:
                    return !!this._weapon && (!effect.category || (effect.category === this._weapon.category)) && !this._weapon.enchanted;
                case EffectType.WEAPON_DISENCHANT:
                    return !!this._weapon && this._weapon.enchanted;
                case EffectType.WEAPON_CATEGORY_DAMAGE:
                    return true; //TODO: multiple effects on weapon category damage not supported
                case EffectType.LEECH_LIFE:
                    return true;
                case EffectType.MULTIPLY_DAMAGE:
                    return true;
                case EffectType.INCREASE_MAX_BLESSING_DURATION:
                    return true;
                case EffectType.INCREASE_MAX_POTION_PER_TYPE:
                    return true;
                case EffectType.INCREASE_NATURAL_HEAL:
                    return true;
                case EffectType.TRACKING:
                    return true;
                case EffectType.STUN:
                case EffectType.REDUCE_DAMAGE:
                    return false;
                default:
                    Utils.assertUnreachable(effect);
            }
        }
        return false;
    }

    private _updateEffects(): void {
        const hpRatio = this._hp / this._maxHP;
        this._maxHP = this._baseMaxHP;
        this._bonusDamage.physical[0] = 0;
        this._bonusDamage.physical[1] = 0;
        this._bonusDamage.magic[0] = 0;
        this._bonusDamage.magic[1] = 0;
        this._damageModifier.physical = 1;
        this._damageModifier.magic = 1;
        this._maxBlessingDuration = BASE_MAX_BLESSING_DURATION;
        this._maxPotionPerType = BASE_MAX_POTION_PER_TYPE;
        this._weaponDamage = {};
        this._naturalHealFactor = 1;
        this._tracking = 0;
        for (let i = 0; i < this._activeEffects.length; i++) {
            this.applyEffect(this._activeEffects[i]);
        }
        if (this._hp > 0) {
            this._hp = Math.max(1, Math.round(this._maxHP * hpRatio));
        }
    }

    public applyEffect(effect: Effect, options?: EffectOptions): void {
        switch (effect.type) {
            case EffectType.HEAL:
                this._hp = Math.min(this._hp + effect.value, this._maxHP);
                this.triggerEvent(CharacterEvent.HP_CHANGED);
                break;
            case EffectType.INCREASE_LIFE:
                this._maxHP += effect.value;
                break;
            case EffectType.INCREASE_DAMAGE:
                Utils.addDamageRange(this._bonusDamage, effect.value);
                break;
            case EffectType.INCREASE_BLESSING_DURATION:
                this.blessings.forEach((blessing) => blessing.battles = Math.min(blessing.battles + effect.value, this._maxBlessingDuration));
                break;
            case EffectType.WEAPON_QUALITY: //TODO: does not work with durations
                if (this._weapon) {
                    this._weapon.quality = effect.value;
                }
                break;
            case EffectType.WEAPON_ENCHANT: //TODO: does not work with durations
                if (this._weapon) {
                    this._weapon.enchant(effect.value);
                }
                break;
            case EffectType.WEAPON_DISENCHANT:
                if (this._weapon) {
                    this._weapon.disenchant();
                }
                break;
            case EffectType.WEAPON_CATEGORY_DAMAGE:
                this._weaponDamage[effect.category] = this._weaponDamage[effect.category] || {};
                Utils.setDamage(this._weaponDamage[effect.category]!, effect.value); //TODO: multiple effects on weapon category damage not supported
                break;
            case EffectType.LEECH_LIFE:
                if (!options || !options.damage) {
                    throw new Error("Damage needs to be given when applying leech life effect!");
                }
                let value = 0;
                if (effect.value.physical) {
                    value += Math.round(options.damage.physical * effect.value.physical);
                }
                if (effect.value.magic) {
                    value += Math.round(options.damage.magic * effect.value.magic);
                }
                this.log("Character leeched " + value + " life", 1);
                this._hp = Math.min(this._hp + value, this._maxHP);
                this.triggerEvent(CharacterEvent.HP_CHANGED);
                break;
            case EffectType.MULTIPLY_DAMAGE:
                Utils.mulDamage(this._damageModifier, effect.value);
                break;
            case EffectType.INCREASE_MAX_BLESSING_DURATION:
                this._maxBlessingDuration += effect.value;
                break;
            case EffectType.INCREASE_MAX_POTION_PER_TYPE:
                this._maxPotionPerType += effect.value;
                break;
            case EffectType.INCREASE_NATURAL_HEAL:
                this._naturalHealFactor += effect.value;
                break;
            case EffectType.TRACKING:
                this._tracking = effect.value;
                break;
            case EffectType.STUN:
                // not implemented, enemies cannot stun
                break;
            case EffectType.REDUCE_DAMAGE:
                // not implemented, enemies cannot reduce player damage
                break;
            default:
                Utils.assertUnreachable(effect);
        }
    }

    public getEffect(effect: Effect): Effect | null {
        if (!("name" in effect) && !isPassiveSkillEffect(effect)) {
            return null;
        }
        for (let i = 0; i < this._activeEffects.length; i++) {
            if (sameNamedEffect(this._activeEffects[i], effect) || samePassiveSkillEffect(this._activeEffects[i], effect)) {
                return this._activeEffects[i];
            }
        }
        return null;
    }

    public addEffect(effect: Effect, options?: EffectOptions): void {
        if (isImmediateEffect(effect)) {
            this.applyEffect(effect, options);
            return;
        }
        const existingEffect = this.getEffect(effect);
        if (existingEffect) {
            Object.assign(existingEffect, Utils.shallowCopy(effect));
        } else {
            this._activeEffects.push(Utils.shallowCopy(effect) as Effect);
        }
        this._updateEffects();
    }

    public getPotion(type: PotionType): Potions | null {
        for (let i = 0; i < this._potions.length; i++) {
            if (Utils.objectsDeepEqual(this._potions[i].type, type)) {
                return this._potions[i];
            }
        }
        return null;
    }

    public canTakePotion(potion: Potions): boolean {
        let ownPotion = this.getPotion(potion.type);
        if (ownPotion) {
            return ownPotion.amount < this._maxPotionPerType;
        } else {
            return true;
        }
    }

    public takePotion(potion: Potions): void {
        let ownPotion = this.getPotion(potion.type);
        if (ownPotion) {
            ownPotion.amount = Math.min(ownPotion.amount + potion.amount, this._maxPotionPerType);
        } else {
            this._potions.push(Object.assign({}, potion));
        }
    }

    public canUsePotion(index: number): boolean {
        if (this._potionUsed) {
            return false;
        }
        const potion = this._potions[index];
        return this.canApplyEffect(potion.type.effect);
    }

    public usePotion(index: number): void {
        const potion = this._potions[index];
        this.addEffect(potion.type.effect);
        potion.amount--;
        if (potion.amount <= 0) {
            this._potions.splice(index, 1);
        }
        if (this.fighting) {
            this._potionUsed = true;
        }
    }

    private _canTakeLoot(items: Loot): boolean {
        if (items.gold) {
            return true;
        }
        if (items.weapon) {
            return true;
        }
        if (items.potions) {
            for (let potion of items.potions) {
                if (this.canTakePotion(potion)) {
                    return true;
                }
            }
        }
        if (items.npcImprovementItem) {
            return true;
        }
        if (items.questItems && (items.questItems.length > 0)) {
            return true;
        }
        return false;
    }

    public takeLoot(items: Loot, takeWeaponAsBackup: boolean = false): void {
        if (items.gold) {
            this._gold += items.gold;
            this._stats.money_collected += items.gold;
        }
        if (items.weapon) {
            if (takeWeaponAsBackup) {
                this._backupWeapon = items.weapon;
            } else {
                this._weapon = items.weapon;
            }
        }
        if (items.potions) {
            for (let i = 0; i < items.potions.length; i++) {
                this.takePotion(items.potions[i]);
            }
        }
        if (items.npcImprovementItem) {
            const item = items.npcImprovementItem;
            const npc = game.town.npcs.find(npc => npc.canApplyImprovementItem(item));
            if (npc && !npc.isMaxImproved()) {
                this._npcImprovementItems.push(items.npcImprovementItem);
            }
        }
        if (items.questItems) {
            items.questItems.forEach(item => this._questItems.push(item));
            this.triggerEvent(CharacterEvent.QUEST_ITEM_ACQUIRED);
        }
    }

    public hasQuestItem(item: QuestItem): boolean {
        return !!this._questItems.find(questItem => (questItem.name === item.name) && (questItem.questId === item.questId));
    }

    public getSkill(name: string): SkillType | null {
        for (let i = 0; i < this._skills.length; i++) {
            if (this._skills[i].name === name) {
                return this._skills[i];
            }
        }
        return null;
    }

    public canLearnSkill(skill: SkillType): boolean {
        return !this.getSkill(skill.name);
    }

    public learnSkill(skill: SkillType): void {
        this._skills.push(Object.assign({}, skill));
    }

    private _skillRequiresMagicDamage(skill: SkillType): boolean {
        if (skill.selfEffect) {
            if (skill.selfEffect.type === EffectType.LEECH_LIFE) {
                return !skill.selfEffect.value.physical;
            }
        } else {
            return !skill.damage.physical;
        }
        return false;
    }

    public canUseSkill(skill: SkillType): boolean {
        if (this._skillRequiresMagicDamage(skill)) {
            const damage = this.magicDamage;
            if ((damage[0] <= 0) && (damage[1] <= 0)) {
                return false;
            }
        }
        return !skill.weapon || (this._weapon?.category === skill.weapon);
    }

    public get usableSkills(): SkillType[] {
        return this._skills.filter((skill) => this.canUseSkill(skill));
    }

    public openTreasure(callback: () => void): void {
        if (!this._room) {
            throw new Error("Character cannot open chest when not in a room!");
        }
        this._room.takeTreasure(callback);
    }

    public naturalHeal(value: number): number {
        const healing = Math.min(Math.round(value * this._naturalHealFactor), this._maxHP - this._hp);
        this._hp += healing;
        return healing;
    }

    public couldWantWeapon(weapon: Weapon): boolean {
        const couldWantInsteadOf = (current: Weapon) => {
            return (
                // a weapon with higher base damage can have higher potential total damage (even if not higher current total damage, because current weapon is e.g. enchanted)
                weapon.anyStrongerThan(current, true) ||
                // a weapon with higher current total damage could obviously be better
                weapon.anyStrongerThan(current, false) ||
                // you might want the same base damage weapon if the current one is already enchanted, to apply a different (more powerful) enchantment
                (current.enchanted && !weapon.anyWeakerThan(current, true))
            );
        }
        if (!this._weapon || !this._backupWeapon) {
            return true;
        }
        if (weapon.category === this._weapon.category) {
            return couldWantInsteadOf(this._weapon);
        }
        if (weapon.category === this._backupWeapon.category) {
            return couldWantInsteadOf(this._backupWeapon);
        }
        // you might want the weapon if it is a different category, to use skills for that category
        return true;
    }

    public upgradeNPC(): void {
        if (!this._npc) {
            throw new Error("Character cannot upgrade NPC when not visiting one!");
        }
        this._gold -= this._npc.upgradeCost;
        this._npc.upgrade();
    }

    public updateStats(): void {
        this._stats.money = this.gold;
    }

    public getStats(): HeroStats {
        this.updateStats();
        return Object.assign({}, this._stats);
    }

    public onAreaDiscovered(area: Area): void {
        const key = Utils.findKey(area.typeName, AreaTypes);
        if (key) {
            this._stats.areas[key.toLowerCase()].discovered++;
        }
    }

    public onAreaCompleted(area: Area): void {
        const key = Utils.findKey(area.typeName, AreaTypes);
        if (key) {
            this._stats.areas[key.toLowerCase()].completed++;
            this._stats.areas[key.toLowerCase()].highest_completed = Math.max(this._stats.areas[key.toLowerCase()].highest_completed, area.level);
        }
    }

    public onAreaScouted(area: Area): void {
        const key = Utils.findKey(area.typeName, AreaTypes);
        if (key) {
            this._stats.areas[key.toLowerCase()].scouted++;
        }
    }

    public onAreaQuest(area: Area): void {
        const key = Utils.findKey(area.typeName, AreaTypes);
        if (key) {
            this._stats.areas[key.toLowerCase()].quest++;
        }
    }

    public onAreaQuestCompleted(area: Area): void {
        const key = Utils.findKey(area.typeName, AreaTypes);
        if (key) {
            this._stats.areas[key.toLowerCase()].quest_completed++;
        }
    }
}