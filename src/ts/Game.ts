/*!
 * Copyright 2018-2021 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface AreaLevels {
    [name: string]: number;
}

interface GameState extends GameObjectState {
    date: string;
    version: string;
    _areaLevels: AreaLevels;
    player: CharacterState;
    town: TownState;
    quests: QuestState[];
    startingArea: AreaState;
    scoutedAreas: AreaState[];
    questAreas: AreaState[];
    worldView: WorldViewSettings;
    townView: TownViewSettings;
}

const HERO_PORTRAITS = ["default", "male1", "male2", "male3", "female1", "female2", "female3"];
const VERSION_KEY = "jahyca_version";
const UNLOCKED_PORTRAITS_KEY = "jahyca_unlocked_portraits";
const AUTOSAVE_KEY = "autosave";
const SAVES_KEY = "saves";

type WeaponSwitchResponse = "current" | "backup" | "drop" | "auto";

class Game extends GameObject {

    private _version: string;
    private _areaPathProbabilities: number[];
    private _areaLevels: AreaLevels;
    private _player: Character;
    private _town: Town;
    private _quests: Quest[];
    private _startingArea: Area;
    private _scoutedAreas: Area[];
    private _questAreas: Area[];
    private _popup: Popup;
    private _playerView: CharacterView;
    private _worldView: WorldView;
    private _townView: TownView;
    private _curtainPanel: Panel;
    private _analytics: Analytics;
    private _unlockedPortraits: string[];

    private _curtainTimeouts: number[] = [];

    constructor(version: string) {
        super("Game");

        this._version = version;

        this.log("created Game instance, version: " + version, 2);

        this._areaPathProbabilities = [
            0.7, 0.25, 0.05
        ];
        this._areaLevels = {};

        this._player = new Character();
        this._startingArea = new Area(null, this._areaPathProbabilities);
        this._scoutedAreas = [];
        this._questAreas = [];
        this._popup = new Popup({text: ""});

        this._playerView = new CharacterView();
        this._worldView = new WorldView(this);
        this._worldView.addToPage();
        this._town = new Town();
        this._quests = [];
        this._townView = new TownView(this, this._town, this._player);
        this._townView.addToPage();
        this._curtainPanel = new Panel({
            className: "popup-wrapper curtain",
            opacity: 0,
            visible: false
        });
        this._curtainPanel.addToPage();

        this._unlockedPortraits = localStorage.getItem(UNLOCKED_PORTRAITS_KEY)?.split(",") ?? [];

        const lastVersion: string = localStorage.getItem(VERSION_KEY) || "";

        if (!lastVersion || (version > lastVersion)) {
            const existingPlayer = !!localStorage.getItem(AUTOSAVE_KEY);
            if (existingPlayer) {
                const releaseNotes = getReleaseNotes(lastVersion).map(notes => {
                    return `<div class="releaseNote"><span class="version">${notes.version}</span>: ${notes.notes}</div>`;
                }).join("<br>");
                this.showPopup(`<div class="releaseParagraph">Welcome back to Jahyca!<br>The game has been updated to version <span class="version">${version}</span>!</div>Here's what's new:<div class="releaseNotes inside-scroll">${releaseNotes}</div>`, "info");
            }
            const startupMessages = getStartupMessages(lastVersion);
            for (const message of startupMessages) {
                if (existingPlayer || !message.onlyForExistingPlayers) {
                    this.showPopup(message.message, message.className ? "info " + message.className : "info");
                }
            }
        }

        localStorage.setItem(VERSION_KEY, version);

        this._analytics = new Analytics("https://jahyca-analytics.herokuapp.com/", version, false);
        this._analytics.login();
    }

    public getObjectType(): string {
        return "Game";
    }

    public get town(): Town {
        return this._town;
    }

    public get player(): Character {
        return this._player;
    }

    public get playerView(): CharacterView {
        return this._playerView;
    }

    public getNPC(name: string): NPC | null {
        return this._town.getNPC(name);
    }

    private static countAreas(save: GameState): [number, number] {
        let result: [number, number] = [0, 0];
        const addArea = (area: AreaState) => {
            result[1]++;
            if (area.enemyCount <= 0) {
                result[0]++;
            }

            area.paths.forEach(addArea);
        }
        addArea(save.startingArea);
        if (save.scoutedAreas) {
            save.scoutedAreas.forEach(addArea);
        }
        if (save.questAreas) {
            save.questAreas.forEach(addArea);
        }
        return result;
    }

    private _getSaveButtonContent(save: GameState, auto: boolean = false): string {
        const areas = Game.countAreas(save);
        return (auto ? "Autosave:<br>" : "") +
            `<span class="name">${save.player.name}</span>` +
            `<span class="date"> (${save.date})</span><br>` +
            `<div class="label money">${save.player.gold}</div>` +
            `<div class="label health">${save.player.hp}</div>` +
            `<div class="label areas">${areas[0]} / ${areas[1]}</div>` +
            (save.player._analyticsId ? `<img class="leaderboard${this._analytics.isHeroFromPreviousSeason(save.player._analyticsId) ? " archive" : ""}" src="img/leaderboard.png">` : "");
    }

    public showLoadMenu(showProgressWarning: boolean, onCancel?: () => void): void {
        const options: PopupOption[] = [];
        const addOption = (save: GameState, auto: boolean = false) => {
            options.push({
                content: this._getSaveButtonContent(save, auto),
                className: "load-menu-option",
                action: () => {
                    this.showPopup("Are you sure you want to load " + save.player.name + "?" + (showProgressWarning ? "<br>Your current unsaved progress will be lost!" : ""), "gray", [{
                        caption: "Load",
                        action: () => {
                            this.load(save);
                        }
                    }, {
                        caption: "Cancel",
                        action: () => {
                            this.showLoadMenu(showProgressWarning, onCancel);
                        }
                    }]);
                }
            });
        };
        const autosave = this.getAutoSave();
        if (autosave) {
            addOption(autosave, true);
        }
        const saves = this.getSaves();
        if (saves) {
            for (let i = 0; i < saves.length; i++) {
                addOption(saves[i]);
            }
        }
        options.push({
            caption: "Cancel",
            action: () => {
                if (onCancel) {
                    onCancel();
                }
            }
        });
        this.showPopup("Choose game to load:", "gray", options);
    }

    public showSaveMenu(onChange?: () => void, onCancel?: () => void): void {
        const options: PopupOption[] = [];
        const saves = this.getSaves();
        if (saves) {
            for (let i = 0; i < saves.length; i++) {
                options.push({
                    content: this._getSaveButtonContent(saves[i], false),
                    className: "save-menu-option",
                    action: () => {
                        this.showPopup("Are you sure you want to overwrite " + saves[i].player.name + "?<br>This action cannot be undone.", "gray", [{
                            caption: "Save",
                            action: () => {
                                this.saveToSlot(i);
                                if (onChange) {
                                    onChange();
                                }
                            }
                        }, {
                            caption: "Cancel",
                            action: () => {
                                this.showSaveMenu(onChange, onCancel);
                            }
                        }]);
                    }
                });
                options.push({
                    caption: "X",
                    className: "save-menu-delete",
                    newLineAfter: true,
                    action: () => {
                        this.showPopup("Are you sure you want to delete " + saves[i].player.name + "?<br>This action cannot be undone.", "gray", [{
                            caption: "Delete",
                            action: () => {
                                this.deleteSave(i);
                                this.showSaveMenu(onChange, onCancel);
                            }
                        }, {
                            caption: "Cancel",
                            action: () => {
                                this.showSaveMenu(onChange, onCancel);
                            }
                        }]);
                    }
                });
            }
        }
        options.push({
            caption: "New",
            action: () => {
                this.saveToSlot();
                if (onChange) {
                    onChange();
                }
            }
        });
        options.push({
            caption: "Cancel",
            action: () => {
                if (onCancel) {
                    onCancel();
                }
            }
        });
        this.showPopup("Choose slot to save to:", "gray", options);
    }

    private _showMyHeroes(leaderboard: LeaderboardEntry[], onDone: () => void): void {
        const options: PopupOption[] = [];
        const heroes = leaderboard.filter(entry => entry.own_hero);
        heroes.forEach(hero => {
            options.push({
                content: `<img src="img/portraits/${hero.hero_portrait}.png">${hero.hero_name}`,
                className: "hero",
                newLineAfter: true,
                action: () => {
                    const portraits: PopupOption[] = [];
                    HERO_PORTRAITS.concat(this._unlockedPortraits).forEach(portrait => {
                        portraits.push({
                            content: `<img src="img/portraits/${portrait}.png">`,
                            className: "portrait",
                            action: () => {
                                this._analytics.editHero(hero.hero_id, {
                                    portrait
                                });
                                onDone();
                            }
                        });
                    })
                    portraits[portraits.length - 1].newLineAfter = true;
                    portraits.push({
                        caption: "Cancel",
                        action: () => {
                            this._showMyHeroes(leaderboard, onDone);
                        }
                    });
                    this.showPopup(`Select portrait:<br>(${portraits.length - 1} / ${HERO_PORTRAITS.length + Object.keys(EnemyTypes).length} portraits unlocked)`, "info", portraits, "portraits");
                }

            });
        });
        options.push({
            caption: "Done",
            action: onDone
        });
        this.showPopup("Choose a hero to change portrait:", "gray", options);
    }

    private _askJoinLeaderboard(callback: (answer: boolean) => void): void {
        this.showPopup("Do you wish to participate in the online leaderboard with this hero?<br>" +
            "If you choose yes, the name of your hero and its game statistics required to calculate its score will be stored in an online database, and all players will be able to see the hero and its score on the leaderboard.<br>" +
            "If you choose no, the hero will not be visible on the leaderboard and all data about it will be stored on your device.", "info", [{
                caption: "Yes",
                action: () => callback(true)
            }, {
                caption: "No",
                action: () => callback(false)
            }]);
    }

    public showLeaderboard(allowJoin: boolean, onBack: () => void, archiveSeason?: number) {
        this.showPopup("Loading leaderboard...", "loading", []);
        const callback = (leaderboard: LeaderboardEntry[], season: number, seasonEnd: string) => {
            const now = new Date().getTime();
            let table = '<table class="leaderboard"><thead>' +
                '<th class="rank"><img src="img/leaderboard.png"></th><th class="name">Hero</th><th class="score">Score</th><th class="money"><img src="img/coin.png" class="coin"></th><th class="kills"><img src="img/skull.png"></th><th class="areas"><img src="img/map-small.png"></th><th class="time"><img src="img/time.png"></th>' +
                '</thead><tbody class="inside-scroll">';
            leaderboard.forEach((entry, index) => {
                const frames = entry.frames ? entry.frames.split(",").map(frame => '<img src="img/frames/' + frame + '.png" class="portrait-frame">').join("") : "";
                table += `<tr ${entry.own_hero ? 'class="own"' : ""}><td class="rank">${index + 1}</td>` +
                    `<td class="name"><img src="img/portraits/${entry.hero_portrait}.png">${frames}${entry.hero_name}</td><td class="score">${entry.score}</td><td class="money">${entry.money_collected}</td><td class="kills">${entry.kills}</td><td class="areas">${entry.areas_completed}</td><td class="time">${Utils.formatTimeAgo(entry.last_login, now)}<br>ago</td></tr>`;
            });
            table += "</tbody></table>";
            const options: PopupOption[] = archiveSeason ? [] : [{
                caption: "My heroes",
                action: () => {
                    this._showMyHeroes(leaderboard, onBack);
                }
            }];
            options.push({
                caption: "Back",
                action: onBack
            });
            if (allowJoin && !archiveSeason && !this._player.analyticsId) {
                options.push({
                    caption: "Join leaderboard",
                    action: () => {
                        this._askJoinLeaderboard((answer) => {
                            if (answer) {
                                this._addHeroToLeaderboard(this._player.name, (heroId: string) => {
                                    if (heroId) {
                                        this._player.addToAnalytics(heroId);
                                        this.showPopup("Updating hero data...", "loading", []);
                                        this._analytics.updateHero(this._player.analyticsId, this._player.getStats(), () => {
                                            this._popup.close();
                                            this.showPopup(`Hero has been successfully added to the leaderboard!<br>Playing with old saved games of this hero will not update the leaderboard score! Only saves marked with <img src="img/leaderboard.png" class="inline"> are synced to the leaderboard. Make a new save to keep your score updated!`, "info", [{
                                                caption: "OK",
                                                action: () => {
                                                    this.showLeaderboard(allowJoin, onBack);
                                                }
                                            }]);
                                        }, () => {
                                            this._popup.close();
                                            this.showPopup("A problem occured while updating hero data! We will try to update the data later as you play.", "info error", [{
                                                caption: "OK",
                                                action: () => {
                                                    this.showLeaderboard(allowJoin, onBack);
                                                }
                                            }]);
                                            return false;
                                        });
                                    } else {
                                        this.showLeaderboard(allowJoin, onBack);
                                    }
                                })
                            } else {
                                this.showLeaderboard(allowJoin, onBack);
                            }
                        })
                    }
                });
            }
            if (archiveSeason) {
                if (season > 1) {
                    options.push({
                        caption: "Previous",
                        className: "leaderboard-previous",
                        action: () => {
                            this.showLeaderboard(allowJoin, onBack, season - 1);
                        }
                    });
                }
                options.push({
                    caption: "Current",
                    className: "leaderboard-current",
                    action: () => {
                        this.showLeaderboard(allowJoin, onBack);
                    }
                });
            } else {
                options.push({
                    caption: "Archive",
                    className: "leaderboard-previous",
                    action: () => {
                        this.showLeaderboard(allowJoin, onBack, season - 1);
                    }
                });
            }
            const title = `<h2>Leaderboard<br><span class="subtitle">Season ${season}</span></h2>`;
            const seasonEndLabel = archiveSeason ?  "" : (`<div class="season-end">Season end${archiveSeason ? "ed" : "s"}:<br>${Utils.formatTimeUntil(new Date(seasonEnd), !!archiveSeason)}</div>`);
            this.showPopup(`${title}${seasonEndLabel}${table}`, "leaderboard wide", options);
        };
        const onError = () => {
            this.showPopup("Sorry! An error happened while trying to download the leaderboard.", "info error", [{
                caption: "OK",
                action: onBack
            }]);
            return false;
        };
        if (archiveSeason) {
            this._analytics.getLeaderboardArchive(archiveSeason, callback, onError);
        } else {
            this._analytics.getLeaderboard(callback, onError);
        }
    }

    public showMainMenu(allowSave: boolean, showProgressWarning: boolean, onContinue?: () => void): void {
        const menuOptions: PopupOption[] = [];

        const showStartPopup = () => {
            this.showPopup("Enter the name of your hero:", "", [{
                caption: "Start",
                action: (name) => {
                    this._askJoinLeaderboard((answer) => {
                        this.start(name || "Player", answer);;
                    });
                }
            }, {
                caption: "Cancel",
                action: () => {
                    this.showMainMenu(allowSave, showProgressWarning, onContinue);
                }
            }], undefined,
                {
                    value: "Player"
                });
        };

        if (onContinue) {
            menuOptions.push({
                caption: "Continue game",
                className: "main-menu-option",
                action: () => {
                    onContinue();
                }
            });
        }

        menuOptions.push({
            caption: "Start new game",
            className: "main-menu-option",
            action: () => {
                if (showProgressWarning) {
                    this.showPopup("Are you sure you want to start a new game?<br>Unsaved progress will be lost!", "info", [{
                        caption: "Yes",
                        action: showStartPopup
                    }, {
                        caption: "No",
                        action: () => {
                            this.showMainMenu(allowSave, showProgressWarning, onContinue);
                        }
                    }]);
                } else {
                    showStartPopup();
                }
            }
        });

        if (allowSave) {
            menuOptions.push({
                caption: "Save game",
                className: "main-menu-option",
                action: () => {
                    this.showSaveMenu(() => {
                        this.showMainMenu(allowSave, showProgressWarning, onContinue);
                    }, () => {
                        this.showMainMenu(allowSave, showProgressWarning, onContinue);
                    });
                }
            });
        }

        if (this.canLoad()) {
            menuOptions.push({
                caption: "Load game",
                className: "main-menu-option",
                action: () => {
                    this.showLoadMenu(showProgressWarning, () => {
                        this.showMainMenu(allowSave, showProgressWarning, onContinue);
                    });
                }
            });
        }

        menuOptions.push({
            caption: "Leaderboard",
            className: "main-menu-option",
            action: () => {
                this.showLeaderboard(allowSave, () => {
                    this.showMainMenu(allowSave, showProgressWarning, onContinue);
                });
            }
        });

        menuOptions.push({
            caption: "Credits",
            className: "main-menu-option",
            action: () => {
                this.showPopup(
                    `<h2>Credits</h2>` +
                    `<p>I have designed and programmed the game on my own, but most of the art (expect for a few pieces I couldn't find and had to make myself) is taken or adapted from royalty free assets from <a href="https://opengameart.org" target="_blank" rel="noreferrer">OpenGameArt.org</a>.<br>` +
                    `See <a href="img/sources.txt" target="_blank" rel="noreferrer">img/sources.txt</a> for a complete list of where each of the images is from and what license corresponds to them.` +
                    `</p><p>` +
                    `The fonts used in the game are <a href="https://fonts.google.com/specimen/Oregano" target="_blank" rel="noreferrer">Oregano</a> and <a href="https://fonts.google.com/specimen/Macondo+Swash+Caps" target="_blank" rel="noreferrer">Macondo Swash Caps</a>, available under the <a href="https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL" target="_blank" rel="noreferrer">Open Font License</a>.` +
                    `</p><p>` +
                    `The other source files (HTML, JS, CSS) are released by me under the <a href="http://www.gnu.org/licenses/gpl-3.0-standalone.html" target="_blank" rel="noreferrer">GNU GPL v3 license</a>.` +
                    `</p><p>` +
                    `Copyright 2018-2020 <strong>Krisztián Nagy</strong>` +
                    `</p>` +
                    `<h2>Special thanks</h2>` +
                    `<p>To <strong>Flóra Tápi</strong> for testing and providing valuable suggestions and ideas for the game :)</p>`, "info", [{
                        caption: "OK",
                        action: () => {
                            this.showMainMenu(allowSave, showProgressWarning, onContinue);
                        }
                    }]);
            }
        });

        this.showPopup(
            '<span class="jahyca">Jahyca</span>',
            "grey",
            menuOptions);
    }

    private _addHeroToLeaderboard(name: string, callback: (heroId: string) => void): void {
        this.showPopup("Adding hero to leaderboard...", "loading", []);
        this._analytics.createHero(name, (heroId: string) => {
            if (heroId) {
                this.log("Successfully created hero for analytics: " + heroId, 1);
            }
            this._popup.close();
            callback(heroId);
        }, () => {
            this.showPopup("Sorry! An error happened while trying to add the hero to the leaderboard!<br>Try again or skip adding to the leaderboard?", "info error", [{
                caption: "Try again",
                action: () => {
                    this._addHeroToLeaderboard(name, callback);
                }
            }, {
                caption: "Skip",
                action: () => {
                    callback("");
                }
            }]);
            return false;
        });
    }

    public start(name: string, analytics: boolean): void {
        this._areaLevels = {};
        const finishStart = (heroId: string) => {
            this._player = new Character({
                name,
                analyticsId: heroId,
                hp: 100,
                gold: 50,
                weapon: new Weapon(WeaponTypes.RUSTY_SWORD)
            });

            this._quests = [];

            this._startingArea = new Area(AreaTypes.FOREST, this._areaPathProbabilities);
            this._player.onAreaDiscovered(this._startingArea);
            this._scoutedAreas = [];
            this._questAreas = [];

            this._town.reset();

            this._player.addEventHandler(CharacterEvent.MOVED, () => {
                this.forAreas(area => area.handlePlayerMoved());
            });

            this.curtain(() => {
                this.renderTown();

                const nextButton: PopupOption[] = [{
                    caption: "Next",
                    action: () => {}
                }];
                const startButton: PopupOption[] = [{
                    caption: "Start adventure!",
                    action: () => {}
                }];

                this.showPopup("The Long War is finally over. The realm of Jahyca is united once again.", "info intro", nextButton);
                this.showPopup("But while the human armies battled, evil creatures, orcs, elves and magical beasts have infected the lands.", "info intro", nextButton);
                this.showPopup(`Brave adventurers like you, ${name}, are needed to drive back these monsters, so that trade, travel and rebuilding can start.`, "info intro", nextButton);
                this.showPopup("You have arrived at a little town that needs your help.", "info intro", nextButton);
                this.showPopup("The local healer already gathered a skilled group of useful talents, but they lack a warrior.", "info intro", nextButton);
                this.showPopup("Help them clear the nearby lands of monsters, use their services to become more powerful and earn your fame and fortune!", "info intro", startButton);
            })
        };
        if (analytics) {
            this._addHeroToLeaderboard(name, finishStart);
        } else {
            finishStart("");
        }
    }

    public getSaves(): GameState[] | null {
        if (localStorage.getItem(SAVES_KEY)) {
            return JSON.parse(localStorage.getItem(SAVES_KEY) || "[]");
        }
        return null;
    }

    public save(): GameState {
        return {
            _id: super.save()._id,
            date: new Date().toLocaleString(),
            version: this._version,
            _areaLevels: this._areaLevels,
            player: this._player.save(),
            town: this._town.save(),
            quests: this._quests.map(quest => quest.save()),
            startingArea: this._startingArea.save(),
            scoutedAreas: this._scoutedAreas.map(area => area.save()),
            questAreas: this._questAreas.map(area => area.save()),
            worldView: this._worldView.save(),
            townView: this._townView.save()
        };
    }

    public saveToSlot(index?: number): void {
        const saves = this.getSaves() || [];
        const save = this.save();
        if (index !== undefined) {
            saves[index] = save;
        } else {
            saves.push(save);
        }
        localStorage.setItem(SAVES_KEY, JSON.stringify(saves));
        this.showPopup("Game saved!", "green");
    }

    public autoSave(): void {
        localStorage.setItem(AUTOSAVE_KEY, JSON.stringify(this.save()));
        if (this._player.analyticsId) {
            this._analytics.updateHero(this._player.analyticsId, this._player.getStats());
        }
        this.log("autosave done", 2);
    }

    public getAutoSave(): GameState | null {
        if (localStorage.getItem(AUTOSAVE_KEY)) {
            return JSON.parse(localStorage.getItem(AUTOSAVE_KEY) || "");
        }
        return null;
    }

    public canLoad(): boolean {
        const autosave = this.getAutoSave();
        const saves = this.getSaves();
        return !!autosave || (!!saves && saves.length > 0);
    }

    public load(save: GameState): Game {
        super.load(save);
        if (!this._player) {
            throw new Error("Cannot load game without a player!");
        }
        if (!this._startingArea) {
            throw new Error("Cannot load game without a starting area!");
        }
        this._areaLevels = save._areaLevels;
        this._player.load(save.player);
        this._town.load(save.town);
        this._startingArea.load(save.startingArea);
        this._scoutedAreas = [];
        if (save.scoutedAreas) {
            save.scoutedAreas.forEach(area => this._scoutedAreas.push(new Area().load(area)));
        }
        this._questAreas = [];
        if (save.questAreas) {
            save.questAreas.forEach(area => this._questAreas.push(new Area().load(area)));
        }
        this._quests = [];
        if (save.quests) {
            save.quests.forEach(quest => this._quests.push(new Quest().load(quest)));
        }
        if (this._player.analyticsId) {
            this._analytics.loadHero(this._player.analyticsId);
        }

        this._worldView.load(save.worldView);
        this._townView.load(save.townView);

        this.curtain(() => {
            this.renderTown();
        });
        return this;
    }

    public deleteSave(index: number): void {
        const saves = this.getSaves();
        if (saves) {
            saves.splice(index, 1);
            localStorage.setItem(SAVES_KEY, JSON.stringify(saves));
            this.showPopup("Saved game deleted!", "orange");
        }
    }

    public renderTown(): void {
        this._townView.render();
        this._townView.hidden = false;
        this._quests.forEach(quest => {
            if (!quest.taken) {
                this.showPopup(`The ${quest.giver.name} has something important to say to you! Visit him to find out!`, "info quest");
            }
        });
    }

    public renderWorld(): void {
        this._worldView.render();
        this._worldView.hidden = false;
    }

    public renderShop(): void {
        this._townView.renderShop();
    }

    public addQuest(params: QuestParams, readd: boolean = false): void {
        if (readd || !this._quests.find(quest => quest.id === params.id)) {
            this._quests.push(new Quest(params));
        }
    }

    public addQuestById(id: string, readd: boolean = false): void {
        const quest = getQuest(id);
        if (quest) {
            this.addQuest(quest, readd);
        }
    }

    public addArea(typeName: string, modifiers?: AreaModifiers, quest?: Quest): Area {
        const areaType = Object.values(AreaTypes).find(at => at.name === typeName);
        if (!areaType) {
            throw new Error("Cannot add area of type '" + areaType + "' - such type doesn't exist!");
        }
        const area = new Area(areaType, null, modifiers, quest);
        if (quest) {
            this._questAreas.push(area);
            this._player.onAreaQuest(area);
        } else {
            this._scoutedAreas.push(area);
            this._player.onAreaScouted(area);
        }
        return area;
    }

    public forAreas(callback: (area: Area) => void): void {
        for (const area of this._questAreas) {
            area.traverse(callback);
        }
        this._startingArea.traverse(callback);
        for (const area of this._scoutedAreas) {
            area.traverse(callback);
        }
    }

    public forQuestAreas(callback: (area: Area) => void): void {
        for (const area of this._questAreas) {
            area.traverse(callback);
        }
    }

    public showPopup(text: string, className: string, options?: PopupOption[], optionsClassName?: string, input?: PopupInput) {
        const params: PopupParams = {
            text: text,
            options: options,
            input: input,
            className,
            optionsClassName
        };
        this._popup.queueMessage(params);
    }

    public get curtainInProgress(): boolean {
        return !this._curtainPanel.hidden;
    }

    public curtain(callback: () => void): void {
        if (this._curtainTimeouts.length > 0) {
            for (const timeout of this._curtainTimeouts) {
                clearTimeout(timeout);
            }
            this._curtainTimeouts.length = 0;
        }
        this._curtainPanel.opacity = 0;
        this._curtainPanel.hidden = false;
        this._curtainPanel.playAnimation("fade-in", 300, () => {
            this._curtainPanel.opacity = 1;
        });
        setTimeout(callback, 300);
        this._curtainTimeouts.push(setTimeout(() => {
            this._curtainPanel.playAnimation("fade-out", 300, () => {
                this._curtainPanel.opacity = 0;
                this._curtainPanel.hidden = true;
            });
        }, 500));
    }

    public getLootMessage(loot: Loot, prefix: string = "You have found "): string {
        let messages: string[] = [];
        if (loot.gold) {
            messages.push(prefix + loot.gold + " gold!");
        }
        if (loot.weapon) {
            messages.push(prefix + "a weapon: " + loot.weapon.typeName + (this._player.couldWantWeapon(loot.weapon) ? "!" : ", but it is not stronger than yours."));
        }
        if (loot.potions) {
            for (let i = 0; i < loot.potions.length; i++) {
                const amount = loot.potions[i].amount;
                messages.push(prefix + ((amount > 1) ? amount + " flasks of " : "a flask of ") + loot.potions[i].type.name + (this._player.canTakePotion(loot.potions[i]) ? "!" : ", but you cannot carry any more of this type."));
            }
        }
        if (loot.npcImprovementItem) {
            const item = loot.npcImprovementItem;
            const npc = this._town.npcs.find(npc => npc.canApplyImprovementItem(item));
            if (npc && !npc.isMaxImproved()) {
                messages.push(prefix + "an item: " + item.name + ", which might be useful for someone in town!");
            }
        }
        if (loot.questItems) {
            for (const item of loot.questItems) {
                messages.push(prefix + "an item: " + item.name + "!");
            }
        }
        return messages.join("<br>");
    }

    public takeLoot(loot: Loot, callback: (weaponSwitchResponse: WeaponSwitchResponse) => void, trade: boolean = false): void {
        if (this._player.weapon && this._player.backupWeapon && loot.weapon && (trade || this._player.couldWantWeapon(loot.weapon))) {
            const weaponView = new WeaponView();
            weaponView.render(this._player.weapon);
            const playerWeapon = weaponView.html;
            weaponView.render(this._player.backupWeapon);
            const backupWeapon = weaponView.html;
            weaponView.render(loot.weapon);
            const lootWeapon = weaponView.html;
            this.showPopup(`You are already carrying two weapons.<br>Current weapon:<br>${playerWeapon}<br>Backup weapon:<br>${backupWeapon}<br>New weapon:<br>${lootWeapon}<br>Would you like to switch to the new weapon?`, "info", [{
                caption: "Switch current",
                action: () => {
                    this._player.takeLoot(loot);
                    callback("current");
                }
            }, {
                caption: "Switch backup",
                newLineAfter: true,
                action: () => {
                    this._player.takeLoot(loot, true);
                    callback("backup");
                }
            }, {
                caption: "Don't switch",
                action: () => {
                    delete loot.weapon;
                    this._player.takeLoot(loot);
                    callback("drop");
                }
            }]);
        } else {
            if (loot.weapon && !this._player.couldWantWeapon(loot.weapon)) {
                delete loot.weapon;
            }
            const takeAsBackup = !!loot.weapon && !!this._player.weapon && !this._player.backupWeapon;
            this._player.takeLoot(loot, takeAsBackup);
            if (takeAsBackup) {
                const weaponView = new WeaponView();
                weaponView.render(this._player.backupWeapon);
                const backupWeapon = weaponView.html;
                this.showPopup(`You have taken a new weapon as backup:<br>${backupWeapon}<br>You can carry one active and one backup weapon at a time. To swap between them, just click on your current weapon in the bottom center of the screen. You cannot swap while you are in active combat!`, "info bag");
            }
            callback("auto");
        }
    }

    public getAreaLevel(name: string): number {
        return this._areaLevels[name] || 0;
    }

    public increaseAreaLevel(name: string): void {
        this._areaLevels[name] = (this._areaLevels[name] || 0) + 1;
        this.log("'" + name + "' area level increased to " + this._areaLevels[name], 1);
    }

    public unlockPortrait(portrait: string): boolean {
        if (!this._unlockedPortraits.includes(portrait)) {
            this._unlockedPortraits.push(portrait);
            localStorage.setItem(UNLOCKED_PORTRAITS_KEY, this._unlockedPortraits.join(","));
            return true;
        }
        return false;
    }
}