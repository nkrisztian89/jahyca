/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface NPCState extends GameObjectState {
    type: string;
    level: number;
    _qualityImprovement: number;
    maxImproved: boolean;
    _freeServices: number;
    serviceUnlocks?: string[];
}

const enum NPCEvent {
    UPGRADED = "upgraded",
    IMPROVEMENT_ITEM_APPLIED = "improvementItemApplied",
    QUEST_ITEM_GIVEN= "questItemGiven",
    SERVICES_UNLOCKED = "servicesUnlocked",
    VISITED = "visited"
}

class NPC extends GameObject {
    private _type: NPCType | null;
    private _level: number;
    private _qualityImprovement: number;
    private _maxImproved: boolean;
    private _freeServices: number;
    private _serviceUnlocks: string[];
    private _quests: Quest[];
    
    constructor(type?: NPCType) {
        super((type ? type.name : ""));

        this._type = Object.assign({}, type);

        this._level = 1;
        this._qualityImprovement = 0;
        this._maxImproved = false;
        this._freeServices = 0;
        this._serviceUnlocks = [];
        this._quests = [];

        this._mapService = this._mapService.bind(this);

        if (type) {
            this._generateItems();
        }
        
        this.log("created NPC instance", 2);
    }
    
    public getObjectType(): string {
        return "NPC";
    }

    public save(): NPCState {
        if (!this._type) {
            throw new Error("Cannot save NPC without a type!");
        }
        const result: NPCState = {
            _id: super.save()._id,
            type: this._type.name,
            level: this._level,
            _qualityImprovement: this._qualityImprovement,
            maxImproved: this._maxImproved,
            _freeServices: this._freeServices
        };
        if (this._serviceUnlocks.length > 0) {
            result.serviceUnlocks = this._serviceUnlocks.slice();
        }
        return result;
    }

    load(state: NPCState): NPC {
        super.load(state);
        
        this._type = Utils.findByName(state.type, NPCTypes);
        this._level = state.level;
        this._qualityImprovement = state._qualityImprovement;
        this._maxImproved = state.maxImproved ?? false;
        this._freeServices = state._freeServices ?? 0;
        this._serviceUnlocks = state.serviceUnlocks?.slice() ?? [];
        this._quests = [];

        this._generateItems();

        return this;
    }
    
    public get name(): string {
        return this._type?.name ?? "unknown";
    }
    
    public get level(): number {
        return this._level;
    }
    
    public get image(): string {
        return this._type?.image ?? "";
    }

    public get qualityFactor(): number {
        return 1 + this._qualityImprovement;
    }
    
    public isMaxImproved(): boolean {
        return this._maxImproved;
    }
    
    public addQuest(quest: Quest): void {
        this._quests.push(quest);
    }
    
    public hasOpenQuests(): boolean {
        return this._quests.filter(quest => quest.isOpen()).length > 0;
    }
    
    public hasQuestUpdates(): boolean {
        return this._quests.filter(quest => quest.hasUpdate()).length > 0;
    }
    
    public unlockServices(unlockId: string): void {
        if (!this._serviceUnlocks.includes(unlockId)) {
            this._serviceUnlocks.push(unlockId);
        }
        this.triggerEvent(NPCEvent.SERVICES_UNLOCKED);
    }

    private _generateItems(): void {
        if (!this._type) {
            throw new Error("Cannot generate items for NPC without a type!");
        }
        this._type.services.forEach((service) => {
            if ("items" in service) {
                if (service.items.weapon) {
                    service.items.weaponInstance = new Weapon(service.items.weapon);
                }
            }
        });
    }

    private _mapAbsValue(value: number): number {
        return Math.round(value * this.qualityFactor);
    }

    private _mapRelValue(value: number): number {
        return value + this._qualityImprovement;
    }
    
    private _mapRelDamage(value: Damage) {
        if (value.physical) {
            value.physical = this._mapRelValue(value.physical);
        }
        if (value.magic) {
            value.magic = this._mapRelValue(value.magic);
        }
        return value;
    }
    
    private _mapDamageRange(value: DamageRange) {
        if (value.physical) {
            value.physical[0] = this._mapAbsValue(value.physical[0]);
            value.physical[1] = this._mapAbsValue(value.physical[1]);
        }
        if (value.magic) {
            value.magic[0] = this._mapAbsValue(value.magic[0]);
            value.magic[1] = this._mapAbsValue(value.magic[1]);
        }
        return value;
    }

    private _mapEffect(effect: Effect): Effect {
        switch (effect.type) {
            case EffectType.HEAL: 
            case EffectType.INCREASE_LIFE:
            case EffectType.INCREASE_BLESSING_DURATION:
            case EffectType.INCREASE_MAX_BLESSING_DURATION:
                effect.value = this._mapAbsValue(effect.value);
                break;
            case EffectType.INCREASE_DAMAGE:
                effect.value = this._mapDamageRange(effect.value);
                break;
            case EffectType.WEAPON_QUALITY:
                effect.value = this._mapRelValue(effect.value);
                break;
            case EffectType.WEAPON_ENCHANT:
                effect.value[0] = this._mapAbsValue(effect.value[0]);
                effect.value[1] = this._mapAbsValue(effect.value[1]);
                break;
            case EffectType.WEAPON_CATEGORY_DAMAGE:
                effect.value = this._mapRelDamage(effect.value);
                break;
            case EffectType.INCREASE_MAX_POTION_PER_TYPE:
            case EffectType.INCREASE_NATURAL_HEAL:
            case EffectType.TRACKING:
            case EffectType.WEAPON_DISENCHANT:
                break;
            default:
                console.warn("Unknown effect type: " + effect.type + " - effect quality cannot be improved.")
        }
        return effect;
    }

    private _mapService(service: NPCService): NPCService {
        const newService: NPCService = Utils.deepCopy(service) as NPCService;
        if (("items" in service) && service.items.weaponInstance && ("items" in newService)) {
            newService.items!.weaponInstance = service.items.weaponInstance;
        }
        if ("effect" in newService) {
            newService.effect = this._mapEffect(newService.effect);
        }
        if ("items" in newService) {
            if (newService.items.potion) {
                newService.items.potion.effect = this._mapEffect(newService.items.potion.effect);
            }
        }
        if ("discover" in newService) {
            if (newService.discover.modifiers) {
                newService.discover.modifiers.treasure = this._mapRelValue(newService.discover.modifiers.treasure || 1);
            }
        }
        newService.qualityImprovement = this._qualityImprovement;
        return newService;
    }

    public get services(): NPCService[] {
        if (!this._type) {
            throw new Error("Cannot get services from NPC without a type!");
        }
        return this._type.services.filter((service) => ((!service.level || (service.level <= this._level)) && (!service.unlockId || this._serviceUnlocks.includes(service.unlockId)))).map(this._mapService);
    }

    public get upgradeCost(): number {
        if (!this._type) {
            throw new Error("Cannot get upgrade cost from NPC without a type!");
        }
        return this._type.upgradeCosts[this._level - 1];
    }

    public canUpgrade(): boolean {
        return ((this._type?.upgradeCosts.length ?? -1) >= this._level);
    }

    public upgrade(): void {
        this._level++;
        this.log("NPC upgraded to level " + this._level, 1);
        this.triggerEvent(NPCEvent.UPGRADED, {npc: this});
    }
    
    public visit(): void {
        this.triggerEvent(NPCEvent.VISITED);
    }

    public canApplyImprovementItem(item: NPCImprovementItem): boolean {
        return !!this._type && !!this._type.improvementItems.find(i => (i.name === item.name));
    }

    public applyImprovementItem(item: NPCImprovementItem): void {
        if (!this._type) {
            throw new Error("Cannot apply improvement item to NPC without a type!");
        }
        this.log("Applying " + item.name + " to " + this._type.name + "...", 1);
        const originalImprovement = this._qualityImprovement;
        this._qualityImprovement = Math.min(this._qualityImprovement + item.quality, this._type.maxImprovement);
        this.log("Service quality improvement is now " + Utils.percent(this._qualityImprovement), 1);
        this.triggerEvent(NPCEvent.IMPROVEMENT_ITEM_APPLIED, {item: item, npc: this, improvement: (this._qualityImprovement - originalImprovement)});
        if (this._qualityImprovement === originalImprovement) {
            this._maxImproved = true;
        }
    }
    
    public canGiveQuestItem(item: QuestItem): boolean {
        return !!this._quests.find(quest => quest.id === item.questId);
    }
    
    public giveQuestItem(item: QuestItem): void {
        this.triggerEvent(NPCEvent.QUEST_ITEM_GIVEN, {item: item, npc: this});
        const quest = this._quests.find(quest => quest.id === item.questId);
        quest?.onItemCollected(item);
    }
    
    public addFreeServices(amount: number): void {
        this._freeServices += amount;
    }
    
    public hasFreeServices(): boolean {
        return this._freeServices > 0;
    }
    
    public buyService(): void {
        if (this._freeServices > 0) {
            this._freeServices--;
        }
    }
    
    public setToMaximumImprovement(): void {
        this._qualityImprovement = this._type?.maxImprovement ?? 0;
    }

    public static getItems(items: NPCServiceItems): Loot {
        let result: Loot = {};
        if (items.weaponInstance) {
            result.weapon = items.weaponInstance.copy();
        }
        if (items.potion) {
            result.potions = [{
                type: items.potion,
                amount: 1
            }];
        }
        return result;
    }
}