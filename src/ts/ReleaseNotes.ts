interface ReleaseNote {
    version: string;
    notes: string;
}

const RELEASE_NOTES: ReleaseNote[] = [{
    version: "0.1:21-09-19-1",
    notes: "Start of season 4"
}, {
    version: "0.1:21-01-13-1",
    notes: "Start of season 3"
}, {
    version: "0.1:20-12-14-1",
    notes: "Added leaderboard season archive, start of season 2"
}, {
    version: "0.1:20-12-02-1",
    notes: "Added leaderboard seasons; minor layout adjustments"
}, {
    version: "0.1:20-12-01-1",
    notes: "Added the ability to carry a backup weapon"
}, {
    version: "0.1:20-11-29-1",
    notes: "Improvement items are no longer collected after the services are improved to the maximum; added service filters to the shops; minor layout tweaks; view settings are saved; smaller save size"
}, {
    version: "0.1:20-11-27-1",
    notes: "Added gold pile visual effects"
}, {
    version: "0.1:20-11-24-1",
    notes: "Weapons looted from enemies have varying quality; style updates"
}, {
    version: "0.1:20-11-21-3",
    notes: "Updated credits screen"
}, {
    version: "0.1:20-11-21-2",
    notes: "Updated popups; disenchanting is now a level 2 mage service"
}, {
    version: "0.1:20-11-21-1",
    notes: "Minor formatting improvements"
}, {
    version: "0.1:20-11-20-1",
    notes: "Can join leaderboard with existing non-leaderboard hero, updated leaderboard layout"
}, {
    version: "0.1:20-11-19-1",
    notes: "Added boss feature, new quest, style changes, new mage services"
}, {
    version: "0.1:20-11-15-1",
    notes: "Added 2 quests, tweaked some styling"
}, {
    version: "0.1:20-11-13-1",
    notes: "Fixed a bug causing failure to load games with corrupted stats data"
}, {
    version: "0.1:20-11-12-5",
    notes: "Fixed a bug causing leaderboard statistics updates to fail"
}, {
    version: "0.1:20-11-12-4",
    notes: "Small tweaks"
}, {
    version: "0.1:20-11-12-3",
    notes: "Added quest indicator icons"
}, {
    version: "0.1:20-11-12-2",
    notes: "Fixed a bug related to keeping player statistics"
}, {
    version: "0.1:20-11-12-1",
    notes: "Fixed some quest bugs"
}, {
    version: "0.1:20-11-10-1",
    notes: "Added framework for quests"
}, {
    version: "0.1:20-11-07-1",
    notes: "Added intro texts when a new game is started"
}, {
    version: "0.1:20-11-06-2",
    notes: "Added version update notes"
}, {
    version: "0.1:20-11-06-1",
    notes: "Added loading bar to game startup"
}];

function getReleaseNotes(lastVersion: string): ReleaseNote[] {
    return RELEASE_NOTES.filter(releaseNotes => !lastVersion || (releaseNotes.version > lastVersion));
}