/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface PotionType extends NamedObject {
    image: string;
    effect: Effect;
}

type PotionTypeMap = NamedObjectMap<PotionType>;

const PotionTypes: PotionTypeMap = {
    SMALL_LIFE: {
        name: "Small healing potion",
        image: "life-small.png",
        effect: {
            type: EffectType.HEAL,
            value: 15
        }
    },
    MEDIUM_LIFE: {
        name: "Healing potion",
        image: "life-medium.png",
        effect: {
            type: EffectType.HEAL,
            value: 25
        }
    },
    LARGE_LIFE: {
        name: "Large healing potion",
        image: "life-large.png",
        effect: {
            type: EffectType.HEAL,
            value: 40
        }
    },
    SMALL_MAGIC: {
        name: "Small magic potion",
        image: "magic-small.png",
        effect: {
            name: "Magic potion",
            type: EffectType.INCREASE_DAMAGE,
            value: {
                magic: [1, 2]
            },
            rounds: 1
        }
    },
    MEDIUM_MAGIC: {
        name: "Magic potion",
        image: "magic-medium.png",
        effect: {
            name: "Magic potion",
            type: EffectType.INCREASE_DAMAGE,
            value: {
                magic: [2, 3]
            },
            rounds: 1
        }
    },
    LARGE_MAGIC: {
        name: "Large magic potion",
        image: "magic-large.png",
        effect: {
            name: "Magic potion",
            type: EffectType.INCREASE_DAMAGE,
            value: {
                magic: [3, 4]
            },
            rounds: 1
        }
    },
    BLESSINGS: {
        name: "Potion of blessings",
        image: "blessings.png",
        effect: {
            type: EffectType.INCREASE_BLESSING_DURATION,
            value: 1
        }
    }
};