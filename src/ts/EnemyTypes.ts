/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */

interface EnemyType extends NamedObject {
    image: string;
    level: number;
    hp: number;
    tough?: number;
    damage: FixedDamageRange;
    fast: boolean;
    loot?: LootChances;
}

type EnemyTypeMap = NamedObjectMap<EnemyType>;

const EnemyTypes: EnemyTypeMap = {
    SHAMAN: {
        name: "Shaman",
        image: "forestshaman.png",
        level: 0,
        hp: 10,
        damage: {
            physical: [3, 5],
            magic: [1, 2]
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.4,
                amount: [5, 20]
            },
            weapons: [{
                type: WeaponTypes.SHAMAN_STAFF,
                chance: 0.3
            }],
            potions: [{
                type: PotionTypes.SMALL_LIFE,
                chances: [0.8, 0.2]
            }, {
                type: PotionTypes.SMALL_MAGIC,
                chances: [0.8, 0.2]
            }]
        }
    },
    FOREST_MONSTER: {
        name: "Forest Guardian",
        image: "forestmonster.png",
        level: 0,
        hp: 25,
        damage: {
            physical: [6, 7]
        },
        fast: false
    },
    EARTH_GOLEM: {
        name: "Earth Golem",
        image: "earthgolem.png",
        level: 0,
        hp: 60,
        damage: {
            physical: [2, 6]
        },
        fast: false
    },
    ELVEN_RANGER: {
        name: "Elven Ranger",
        image: "elvenranger.png",
        level: 1,
        hp: 50,
        damage: {
            physical: [2, 12]
        },
        fast: true,
        loot: {
            gold: {
                chance: 0.75,
                amount: [15, 40]
            },
            potions: [{
                type: PotionTypes.SMALL_LIFE,
                chances: [0.7, 0.2, 0.1]
            }]
        }
    },
    ELVEN_MAGE: {
        name: "Elven Mage",
        image: "elvenmage.png",
        level: 2,
        hp: 40,
        damage: {
            physical: [1, 2],
            magic: [7, 9]
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.85,
                amount: [25, 50]
            },
            weapons: [{
                type: WeaponTypes.MAGE_STAFF,
                chance: 0.25,
                quality: [1, 1.2]
            }],
            potions: [{
                type: PotionTypes.SMALL_LIFE,
                chances: [0.6, 0.2, 0.2]
            }, {
                type: PotionTypes.MEDIUM_MAGIC,
                chances: [0.5, 0.3, 0.1, 0.1]
            }]
        }
    },
    GOLEM: {
        name: "Golem",
        image: "golem.png",
        level: 0,
        hp: 25,
        damage: {
            physical: [3, 4]
        },
        fast: false,
        tough: 1,
        loot: {
            gold: {
                chance: 0.1,
                amount: [5, 30]
            }
        }
    },
    HELLBEAST: {
        name: "Hellbeast",
        image: "hellbeast.png",
        level: 0,
        hp: 40,
        damage: {
            physical: [3, 7]
        },
        fast: true
    },
    MOUNTAIN_TROLL: {
        name: "Mountain Troll",
        image: "mountaintroll.png",
        level: 0,
        hp: 70,
        damage: {
            physical: [1, 14]
        },
        fast: false,
        tough: 2,
        loot: {
            gold: {
                chance: 0.25,
                amount: [5, 50]
            }
        }
    },
    INSECT_WARRIOR: {
        name: "Insect Warrior",
        image: "insectwarrior.png",
        level: 0,
        hp: 15,
        damage: {
            physical: [5, 9]
        },
        fast: true
    },
    RIVER_TROLL: {
        name: "River Troll",
        image: "rivertroll.png",
        level: 0,
        hp: 60,
        damage: {
            physical: [6, 12]
        },
        fast: false,
        tough: 1,
        loot: {
            gold: {
                chance: 0.5,
                amount: [25, 50]
            }
        }
    },
    DRAGON: {
        name: "Dragon",
        image: "dragon.png",
        level: 1,
        hp: 100,
        damage: {
            physical: [6, 10],
            magic: [2, 6]
        },
        fast: false,
        tough: 1
    },
    KNIGHT: {
        name: "Knight",
        image: "knight.png",
        level: 0,
        hp: 50,
        damage: {
            physical: [5, 10]
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.7,
                amount: [25, 60]
            },
            weapons: [{
                type: WeaponTypes.ARMING_SWORD,
                chance: 0.5,
                quality: [1, 1.2]
            }, {
                type: WeaponTypes.FINE_SWORD,
                chance: 0.1,
                quality: [1, 1.1]
            }],
            potions: [{
                type: PotionTypes.SMALL_LIFE,
                chances: [0.8, 0.1, 0.1]
            }]
        }
    },
    ORC_WARRIOR: {
        name: "Orc Warrior",
        image: "orc.png",
        level: 0,
        hp: 55,
        damage: {
            physical: [7, 11]
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.6,
                amount: [5, 40]
            },
            weapons: [{
                type: WeaponTypes.BATTLE_AXE,
                chance: 0.5,
                quality: [1, 1.2]
            }],
            potions: [{
                type: PotionTypes.SMALL_LIFE,
                chances: [0.8, 0.2]
            }, {
                type: PotionTypes.MEDIUM_LIFE,
                chances: [0.65, 0.25, 0.1]
            }]
        }
    },
    MINOTAUR: {
        name: "Minotaur",
        image: "minotaur.png",
        level: 0,
        hp: 65,
        damage: {
            physical: [4, 10],
            magic: [2, 4]
        },
        fast: false,
        tough: 1,
        loot: {
            gold: {
                chance: 0.7,
                amount: [10, 50]
            },
            weapons: [{
                type: WeaponTypes.LARGE_AXE,
                chance: 0.35,
                quality: [1, 1.2]
            }]
        }
    },
    FLAMELING: {
        name: "Flameling",
        image: "flameling.png",
        level: 0,
        hp: 50,
        damage: {
            physical: [2, 8],
            magic: [4, 12]
        },
        fast: true,
        loot: {
            weapons: [{
                type: WeaponTypes.FLAMING_SWORD,
                chance: 0.3,
                quality: [1, 1.2]
            }]
        }
    },
    GHOUL: {
        name: "Ghoul",
        image: "ghoul.png",
        level: 1,
        hp: 85,
        damage: {
            physical: [2, 16],
            magic: [2, 8]
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.2,
                amount: [5, 50]
            }
        }
    },
    FOSSIL: {
        name: "Fossil",
        image: "fossil.png",
        level: 0,
        hp: 25,
        damage: {
            physical: [1, 15]
        },
        fast: false
    },
    DRAGONSNAKE: {
        name: "Dragonsnake",
        image: "dragonsnake.png",
        level: 0,
        hp: 50,
        damage: {
            physical: [5, 25]
        },
        fast: true
    },
    DESERT_TROLL: {
        name: "Desert Troll",
        image: "deserttroll.png",
        level: 1,
        hp: 80,
        damage: {
            physical: [10, 20]
        },
        fast: false,
        tough: 2,
        loot: {
            gold: {
                chance: 0.2,
                amount: [5, 50]
            }
        }
    },
    SWAMPLING: {
        name: "Swampling",
        image: "swampling.png",
        level: 0,
        hp: 25,
        damage: {
            physical: [9, 13]
        },
        fast: true,
        loot: {
            gold: {
                chance: 0.2,
                amount: [5, 15]
            },
            potions: [{
                type: PotionTypes.SMALL_LIFE,
                chances: [0.9, 0.1]
            }]
        }
    },
    PENTAGORE: {
        name: "Pentagore",
        image: "pentagore.png",
        level: 0,
        hp: 75,
        damage: {
            physical: [5, 11],
            magic: [1, 2]
        },
        fast: false
    },
    EXILED_KNIGHT: {
        name: "Exiled Knight",
        image: "exiledknight.png",
        level: 0,
        hp: 70,
        tough: 1,
        damage: {
            physical: [8, 13]
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.4,
                amount: [5, 40]
            },
            weapons: [{
                type: WeaponTypes.BATTLE_AXE,
                chance: 0.4,
                quality: [1, 1.25]
            }],
            potions: [{
                type: PotionTypes.SMALL_LIFE,
                chances: [0.9, 0.05, 0.05]
            }, {
                type: PotionTypes.MEDIUM_LIFE,
                chances: [0.7, 0.15, 0.1, 0.05]
            }, {
                type: PotionTypes.LARGE_LIFE,
                chances: [0.95, 0.05]
            }]
        }
    },
    SWAMP_BEAST: {
        name: "Swamp Beast",
        image: "swampbeast.png",
        level: 1,
        hp: 90,
        tough: 1,
        damage: {
            physical: [11, 13]
        },
        fast: true
    },
    SWAMP_DRAGON: {
        name: "Swamp Dragon",
        image: "swampdragon.png",
        level: 2,
        hp: 100,
        tough: 1,
        damage: {
            physical: [8, 10],
            magic: [4, 8]
        },
        fast: false
    },
    SPIDER: {
        name: "Spider",
        image: "spider.png",
        level: 0,
        hp: 30,
        damage: {
            physical: [7, 12]
        },
        fast: true
    },
    SKELETON: {
        name: "Skeleton Warrior",
        image: "skeleton.png",
        level: 0,
        hp: 45,
        damage: {
            physical: [3, 16]
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.5,
                amount: [2, 40]
            },
            weapons: [{
                type: WeaponTypes.ARMING_SWORD,
                chance: 0.3,
                quality: [0.9, 1.1]
            }]
        }
    },
    CAVE_TROLL: {
        name: "Cave Troll",
        image: "troll.png",
        level: 0,
        hp: 75,
        damage: {
            physical: [10, 20]
        },
        fast: false,
        tough: 3,
        loot: {
            gold: {
                chance: 0.3,
                amount: [10, 40]
            }
        }
    },
    WEREWOLF: {
        name: "Werewolf",
        image: "werewolf.png",
        level: 0,
        hp: 60,
        damage: {
            physical: [15, 18],
            magic: [3, 6]
        },
        fast: true
    },
    ARMORED_CRAB: {
        name: "Armored Crab",
        image: "crab.png",
        level: 0,
        hp: 40,
        tough: 4,
        damage: {
            physical: [7, 9],
        },
        fast: false
    },
    VIKING: {
        name: "Viking",
        image: "viking.png",
        level: 0,
        hp: 70,
        damage: {
            physical: [4, 16],
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.5,
                amount: [10, 100]
            },
            weapons: [{
                type: WeaponTypes.VIKING_AXE,
                chance: 0.3,
                quality: [1, 1.2]
            }],
            potions: [{
                type: PotionTypes.SMALL_LIFE,
                chances: [0.9, 0.05, 0.05]
            }, {
                type: PotionTypes.MEDIUM_LIFE,
                chances: [0.5, 0.25, 0.15, 0.1]
            }, {
                type: PotionTypes.LARGE_LIFE,
                chances: [0.9, 0.1]
            }]
        }
    },
    SEA_DRAGON: {
        name: "Sea Dragon",
        image: "seadragon.png",
        level: 0,
        hp: 85,
        damage: {
            physical: [5, 8],
            magic: [5, 10]
        },
        fast: true
    },
    VIKING_BERSERKER: {
        name: "Viking Berserker",
        image: "vikingberserker.png",
        level: 1,
        hp: 80,
        tough: 1,
        damage: {
            physical: [5, 30],
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.8,
                amount: [25, 175]
            },
            weapons: [{
                type: WeaponTypes.BERSERKER_AXE,
                chance: 0.5,
                quality: [1, 1.2]
            }]
        }
    },
    MUTANT_SQUID: {
        name: "Mutant Squid",
        image: "squid.png",
        level: 2,
        hp: 100,
        damage: {
            physical: [2, 4],
            magic: [10, 20]
        },
        fast: false,
        loot: {
            weapons: [{
                type: WeaponTypes.RUBY_STAFF,
                chance: 0.5,
                quality: [1, 1.2]
            }]
        }
    },
    DARK_KNIGHT: {
        name: "Dark Knight",
        image: "darkknight.png",
        level: 0,
        hp: 60,
        damage: {
            physical: [8, 12],
            magic: [2, 3]
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.8,
                amount: [30, 100]
            },
            weapons: [{
                type: WeaponTypes.FINE_SWORD,
                chance: 0.25,
                quality: [1, 1.2]
            }],
            potions: [{
                type: PotionTypes.MEDIUM_LIFE,
                chances: [0.5, 0.3, 0.1, 0.1]
            }, {
                type: PotionTypes.LARGE_LIFE,
                chances: [0.5, 0.3, 0.1, 0.1]
            }]
        }
    },
    DARK_SORCERER: {
        name: "Dark Sorcerer",
        image: "darksorcerer.png",
        level: 0,
        hp: 120,
        damage: {
            physical: [4, 7],
            magic: [8, 18]
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.8,
                amount: [50, 150]
            },
            weapons: [{
                type: WeaponTypes.ARCHMAGE_STAFF,
                chance: 0.8,
                quality: [1, 1.2]
            }],
            potions: [{
                type: PotionTypes.LARGE_LIFE,
                chances: [0.5, 0.3, 0.1, 0.1]
            }, {
                type: PotionTypes.MEDIUM_MAGIC,
                chances: [0.5, 0.3, 0.1, 0.1]
            }, {
                type: PotionTypes.LARGE_MAGIC,
                chances: [0.2, 0.3, 0.3, 0.2]
            }]
        }
    },
    TEMPLAR_KNIGHT: {
        name: "Templar Knight",
        image: "templar.png",
        level: 1,
        hp: 150,
        damage: {
            physical: [15, 30]
        },
        fast: false,
        loot: {
            gold: {
                chance: 0.8,
                amount: [150, 300]
            },
            weapons: [{
                type: WeaponTypes.TEMPLAR_SWORD,
                chance: 0.9,
                quality: [1, 1.25]
            }],
            potions: [{
                type: PotionTypes.LARGE_LIFE,
                chances: [0.2, 0.3, 0.3, 0.2]
            }]
        }
    }
} as const;

type EnemyTypeName = keyof typeof EnemyTypes;