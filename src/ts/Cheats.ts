function maxNPCImprovement() {
    game.town.npcs.forEach((npc) => {
        npc.setToMaximumImprovement();
    });
}