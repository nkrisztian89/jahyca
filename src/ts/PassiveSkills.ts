/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface PassiveSkillType extends NamedObject {
    image: string;
}

type PassiveSkillTypeMap = NamedObjectMap<PassiveSkillType>;

const PassiveSkills: PassiveSkillTypeMap = {
    SWORD_EXPERTISE: {
        name: "Sword expertise",
        image: "sword-expertise.png"
    },
    AXE_EXPERTISE: {
        name: "Axe expertise",
        image: "axe-expertise.png"
    },
    STAFF_MAGIC_FOCUS: {
        name: "Staff magic focus",
        image: "staff-magic-focus.png"
    },
    FORTITUDE: {
        name: "Fortitude",
        image: "fortitude.png"
    },
    ENDURANCE: {
        name: "Endurance",
        image: "endurance.png"
    },
    EFFICIENT_POTION_STORAGE: {
        name: "Efficient potion storage",
        image: "efficient-potion-storage.png"
    },
    DIVINE_LIGHT: {
        name: "Divine light",
        image: "divine-light.png"
    },
    NATURAL_HEALING: {
        name: "Natural healing",
        image: "natural-healing.png"
    },
    TRACKING: {
        name: "Tracking",
        image: "tracking.png"
    }
};