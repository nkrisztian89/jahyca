/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface EnemyModifiers {
    hpFactor?: number;
    nameReplacements?: [string, string][];
    glowColor?: GlowColor;
    loot?: LootChances;
}

interface EnemyState {
    type: string;
    name?: string;
    boss?: boolean;
    hp: number;
    maxHP: number;
    toughness: number;
    glowColor?: GlowColor;
    loot: LootState | null;
    _levelFactor: number;
}

class Enemy {
    private _type: EnemyType | null;
    private _name: string | null;
    private _boss: boolean;
    private _area: Area;
    private _hp: number;
    private _maxHP: number;
    private _toughness: number;
    private _regenRate: number;
    private _damage: FullDamageRange;
    private _glowColor: GlowColor;
    private _loot: Loot | null;
    private _levelFactor: number;
    private _damageFactor: FullDamageFactor;
    private _stunned: number;

    constructor(type: EnemyType | null, areaLevel: number, area: Area, modifiers?: EnemyModifiers, boss: boolean = false) {
        console.log("created Enemy instance");

        this._type = type;
        
        if (type && modifiers?.nameReplacements) {
            this._name = type.name;
            modifiers.nameReplacements.forEach(replacement => {
                this._name = this._name!.replace(replacement[0], replacement[1]);
            });
        } else {
            this._name = null;
        }
        
        this._boss = boss;

        this._area = area;

        let levelFactor = 1;
        if (type && (areaLevel > type.level)) {
            levelFactor = Utils.getLevelFactor(areaLevel - type.level);
        }

        this._hp = type ? Math.round(type.hp * levelFactor * (modifiers?.hpFactor ?? 1)) : 0;
        this._maxHP = this._hp;

        this._toughness = Math.round((type ? (type.tough || 0) : 0) * levelFactor);

        this._regenRate = 0.1;

        this._damage = {
            physical: (type && type.damage.physical) ? [type.damage.physical[0], type.damage.physical[1]] : [0, 0],
            magic: (type && type.damage.magic) ? [type.damage.magic[0], type.damage.magic[1]] : [0, 0],
        };
        
        this._glowColor = modifiers?.glowColor ?? "none";
        
        this._levelFactor = levelFactor;
        
        this._loot = this._generateLoot(modifiers);

        this._damageFactor = {
            physical: 1,
            magic: 1
        };

        this._stunned = 0;
    }
    
    private _generateLoot(modifiers?: EnemyModifiers): Loot | null {
        return this._type ? Utils.generateLoot(modifiers?.loot ?? this._type.loot ?? null, this._levelFactor) : null;
    }

    public save(): EnemyState {
        if (!this._type) {
            throw new Error("Cannot save enemy without a type!");
        }
        const result: EnemyState = {
            type: this._type.name,
            hp: this._hp,
            maxHP: this._maxHP,
            toughness: this._toughness,
            loot: this._loot ? Utils.saveLoot(this._loot) : null,
            _levelFactor: this._levelFactor,
        };
        if (this._name) {
            result.name = this._name;
        }
        if (this._boss) {
            result.boss = this._boss;
        }
        if (this._glowColor !== "none") {
            result.glowColor = this._glowColor;
        }
        return result;
    }

    public load(state: EnemyState): Enemy {
        this._type = Utils.findByName(state.type, EnemyTypes);
        this._name = state.name ?? null;
        this._boss = state.boss ?? false;
        this._levelFactor = state._levelFactor;
        this._maxHP = state.maxHP ?? Math.round(this._type ? this._type.hp * this._levelFactor : 0);
        this._hp = state.hp;
        
        this._toughness = state.toughness;
        this._glowColor = state.glowColor ?? "none";
        
        this._loot = (state.loot !== undefined) ? (state.loot ? Utils.loadLoot(state.loot) : null) : this._generateLoot();

        return this;
    }
    
    public get name(): string {
        return this._name ?? this._type?.name ?? "unknown enemy";
    }
    
    public get boss(): boolean {
        return this._boss;
    }
    
    public get typeImage(): string {
        return this._type?.image ?? "";
    }

    public get damage(): FullDamageRange {
        if (this._type && this._type.damage.physical) {
            this._damage.physical[0] = Math.round(this._type.damage.physical[0] * this._damageFactor.physical * this._levelFactor);
            this._damage.physical[1] = Math.round(this._type.damage.physical[1] * this._damageFactor.physical * this._levelFactor);
        } else {
            this._damage.physical[0] = 0;
            this._damage.physical[1] = 0;
        }
        if (this._type && this._type.damage.magic) {
            this._damage.magic[0] = Math.round(this._type.damage.magic[0] * this._damageFactor.magic * this._levelFactor);
            this._damage.magic[1] = Math.round(this._type.damage.magic[1] * this._damageFactor.magic * this._levelFactor);
        } else {
            this._damage.magic[0] = 0;
            this._damage.magic[1] = 0;
        }
        return this._damage;
    }

    public getDamage(): FullDamage {
        return Utils.getDamage(this.damage);
    }
    
    public get hp(): number {
        return this._hp;
    }
    
    public get maxHP(): number {
        return this._maxHP;
    }

    public get dead(): boolean {
        return this._hp <= 0;
    }

    public get fast(): boolean {
        return !!this._type && this._type.fast;
    }
    
    public get toughness(): number {
        return this._toughness;
    }
    
    public get glowColor(): GlowColor {
        return this._glowColor;
    }

    public isStunned(): boolean {
        return this._stunned > 0;
    }

    public get stunned(): number {
        return this._stunned;
    }

    public hit(damage: FullDamage): void {
        console.log("Enemy got hit for " + damage.physical + " physical and " + damage.magic + " magic damage");
        if (this._toughness) {
            console.log("Physical damage reduced by toughness: " + this._toughness);
        }
        this._hp -= Math.max(0, damage.physical - this._toughness);
        this._hp -= damage.magic;
        if (this.dead) {
            this.die();
        }
    }

    public die(): void {
        console.log("Enemy got killed");
        this._hp = 0;
        this._area.handleEnemyKilled();
    }

    public attack(character: Character): void {
        if (this._stunned > 0) {
            console.log("Enemy is stunned");
            this._stunned--;
            return;
        }
        console.log("Enemy attacks Character");
        character.hit(this.getDamage());
    }

    public applyEffect(effect: Effect): void {
        switch (effect.type) {
            case EffectType.REDUCE_DAMAGE:
                Utils.subRelDamage(this._damageFactor, effect.value);
                console.log("Enemy got crippled");
                break;
            case EffectType.STUN:
                this._stunned = Utils.randomRange(effect.rounds[0], effect.rounds[1]);
                console.log("Enemy got stunned for " + this._stunned + " rounds");
                break;
            default:
                console.warn("Unrecognized effect type: " + effect.type);
        }
    }

    public finishFight(): void {
        if (!this.dead) {
            this._damageFactor.physical = 1;
            this._damageFactor.magic = 1;
            this._stunned = 0;
        }
    }

    public regenerate(): void {
        this._hp = Math.min(this._hp + Math.round(this._regenRate * this._maxHP), this._maxHP);
    }

    public getLoot(): Loot | null {
        return this._loot;
    }
}