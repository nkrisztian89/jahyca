/**
 * Copyright 2018-2020 Krisztián Nagy
 * @file 
 * @author Krisztián Nagy [nkrisztian89@gmail.com]
 * @licence GNU GPLv3 <http://www.gnu.org/licenses/>
 */
 
interface TownState {
    npcs: NPCState[];
}

class Town {
    private _npcs: NPC[];
    
    constructor() {
        console.log("created Town instance");

        this._npcs = [];
        this.reset();
    }
    
    public reset(): void {
        this._npcs = [
            new NPC(NPCTypes.HEALER),
            new NPC(NPCTypes.SWORDSMITH),
            new NPC(NPCTypes.MAGE),
            new NPC(NPCTypes.ALCHEMIST),
            new NPC(NPCTypes.SCOUT),
            new NPC(NPCTypes.TRAINER)
        ];
    }

    public save(): TownState {
        return {
            npcs: this._npcs.map((npc) => npc.save())
        }
    }

    public load(state: TownState) {
        this._npcs = [];
        state.npcs.forEach((npc) => {
            this._npcs.push(new NPC().load(npc));
        });
    }
    
    public get npcs(): NPC[] {
        return this._npcs;
    }
    
    public getNPC(name: string): NPC | null {
        return this._npcs.find(npc => npc.name === name) ?? null;
    }
}