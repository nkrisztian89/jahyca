interface StartupMessage {
    version: string;
    message: string;
    className?: string;
    onlyForExistingPlayers?: boolean;
}

const STARTUP_MESSAGES: StartupMessage[] = [{
    version: "0.1:21-09-19-1",
    message: 'Season 3 of the leaderboard has concluded! Congratulations to the following mighty heroes who have finished on top:<br>1st place: <span class="hero-name">Balazs</span><br>2nd place: <span class="hero-name">Flóó</span><br>3rd place: <span class="hero-name">Wagasy Pherye</span><br>Start a new game to participate in season 4!',
    className: "leaderboard-info",
    onlyForExistingPlayers: true
}, {
    version: "0.1:21-01-13-1",
    message: 'Season 2 of the leaderboard has concluded! Congratulations to the following mighty heroes who have finished on top:<br>1st place: <span class="hero-name">Wagasy Pherye</span><br>2nd place: <span class="hero-name">Flóó</span><br>3rd place: <span class="hero-name">Roller</span><br>Start a new game to participate in season 3!',
    className: "leaderboard-info",
    onlyForExistingPlayers: true
}, {
    version: "0.1:20-12-14-1",
    message: 'Season 1 of the leaderboard has concluded! Congratulations to the following mighty heroes who have finished on top:<br>1st place: <span class="hero-name">Train-er</span><br>2nd place: <span class="hero-name">Flóó</span><br>3rd place: <span class="hero-name">Wagasy Pherye</span><br>Start a new game to participate in season 2!',
    className: "leaderboard-info",
    onlyForExistingPlayers: true
}];

function getStartupMessages(lastVersion: string): StartupMessage[] {
    return STARTUP_MESSAGES.filter(message => !lastVersion || (message.version > lastVersion));
}